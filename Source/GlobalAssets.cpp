#include "GlobalAssets.hpp"

#include "Graphics.hpp"
#include "D2D\Factory.hpp"
#include "DW\FactoryDW.hpp"
#include "WIC\WicFactory.hpp"

namespace Kodo
{
	namespace Graphics
	{
		GlobalAssets::GlobalAssets()
		{
			Create();
		}

		void GlobalAssets::Create()
		{
			if ( Factory2D == nullptr || FactoryDw == nullptr || FactoryWic == nullptr )
			{
				Factory2D = gcnew FactoryD2D();
				FactoryDw = gcnew FactoryDW();
				FactoryWic = gcnew Imaging::WicFactory();
			}
		}
	}
}