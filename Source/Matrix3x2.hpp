#pragma once

#include "Common.hpp"

using namespace System::Runtime::InteropServices;

namespace Kodo
{
	namespace Graphics
	{
		/// <summary> Represents a 3-by-2 matrix and provides convenience methods for creating matrices. </summary>
		[StructLayout( LayoutKind::Sequential, Pack = 4 )]
		public value class Matrix3x2 : System::IEquatable < Matrix3x2 >
		{
		public:

			Single M11;
			Single M12;
			Single M21;
			Single M22;
			Single M31;
			Single M32;

			/// <summary> Creates an identity matrix. </summary>
			static property Matrix3x2 Identity
			{
				Matrix3x2 get() {
					Matrix3x2 result;
					result.M11 = 1.0f;
					result.M22 = 1.0f;
					return result;
				}
			}

			/// <summary> Indicates whether this matrix is the identity matrix. </summary>
			property Boolean IsIdentity
			{
				Boolean get() {
					return	( M11 == 1.0f && M12 == 0.0f && M21 == 0.0f && M22 == 1.0f && M31 == 0.0f && M32 == 0.0f );
				}
			}
			/// <summary> Indicates whether the matrix is invertible. </summary>
			property Boolean IsInvertible
			{
				Boolean get() {
					pin_ptr<Matrix3x2> pinnedThis = this;
					return ( D2D1::Matrix3x2F::ReinterpretBaseType( (D2D1_MATRIX_3X2_F*)pinnedThis ) )->IsInvertible();
				}
			}
			/// <summary> Inverts the matrix, if it is invertible. </summary>
			/// <param name="mat">The matrix to invert.</param>
			static Matrix3x2 Invert( Matrix3x2 mat ) {
				Matrix3x2 result = mat;
				( D2D1::Matrix3x2F::ReinterpretBaseType( (D2D1_MATRIX_3X2_F*)&result ) )->Invert();
				return result;
			}
			/// <summary> Multiplies this matrix with the specified matrix and returns the result. </summary>
			/// <param name="left">The matrix to multiply.</param>
			/// <param name="right">The matrix to multiply with.</param>
			static Matrix3x2 Multiply( Matrix3x2 left, Matrix3x2 right ) {
				Matrix3x2 r;
				r.M11 = ( left.M11 * right.M11 ) + ( left.M12 * right.M21 );
				r.M12 = ( left.M11 * right.M12 ) + ( left.M12 * right.M22 );
				r.M21 = ( left.M21 * right.M11 ) + ( left.M22 * right.M21 );
				r.M22 = ( left.M21 * right.M12 ) + ( left.M22 * right.M22 );
				r.M31 = ( left.M31 * right.M11 ) + ( left.M32 * right.M21 ) + right.M31;
				r.M32 = ( left.M31 * right.M12 ) + ( left.M32 * right.M22 ) + right.M32;
				return r;
			}
			/// <summary> Creates a rotation transformation that has the specified angle and center point. </summary>
			/// <param name="angle">The rotation angle in degrees. A positive angle creates a clockwise rotation, and a negative angle creates a counterclockwise rotation.</param>
			/// <param name="centerPoint">The point about which the rotation is performed.</param>
			static Matrix3x2 Rotation( Single angle, Point centerPoint ) {
				return Translation( -centerPoint.X, -centerPoint.Y ) * Rotation( angle ) * Translation( centerPoint.X, centerPoint.Y );
			}
			/// <summary> Creates a rotation transformation that has the specified angle. </summary>
			/// <param name="angle">The rotation angle in degrees. A positive angle creates a clockwise rotation, and a negative angle creates a counterclockwise rotation.</param>
			static Matrix3x2 Rotation( Single angle ) {
				Matrix3x2 result;

				double radians = ( Math::PI * angle ) / 180.0;

				Single cos = static_cast<Single>( Math::Cos( radians ) );
				Single sin = static_cast<Single>( Math::Sin( radians ) );

				result.M11 = cos;
				result.M12 = sin;
				result.M21 = -sin;
				result.M22 = cos;
				result.M31 = 0.0f;
				result.M32 = 0.0f;

				return result;
			}
			/// <summary> Creates a scale transformation that has the specified scale factors. </summary>
			/// <param name="size">The x-axis and y-axis scale factors of the scale transformation.</param>
			static Matrix3x2 Scale( Size size )	{
				Matrix3x2 r;
				r.M11 = size.Width;	r.M12 = 0.0f;
				r.M21 = 0.0f;		r.M22 = size.Height;
				r.M31 = 0.0f;		r.M32 = 0.0f;
				return r;
			}
			/// <summary> Creates a scale transformation that has the specified scale factors. </summary>
			/// <param name="x">The x-axis scale factor of the scale transformation.</param>
			/// <param name="y">The y-axis scale factor of the scale transformation.</param>
			static Matrix3x2 Scale( Single x, Single y ) {
				Matrix3x2 r;
				r.M11 = x;			r.M12 = 0.0f;
				r.M21 = 0.0f;		r.M22 = y;
				r.M31 = 0.0f;		r.M32 = 0.0f;
				return r;
			}
			/// <summary> Creates a scale transformation that has the specified scale factors and center point. </summary>
			/// <param name="size">The x-axis and y-axis scale factors of the scale transformation.</param>
			/// <param name="centerPoint">The point about which the scale is performed.</param>
			static Matrix3x2 Scale( Size size, Point centerPoint ) {
				Matrix3x2 r;
				r.M11 = size.Width;	r.M12 = 0.0f;
				r.M21 = 0.0f;		r.M22 = size.Height;
				r.M31 = centerPoint.X - ( size.Width * centerPoint.X );
				r.M32 = centerPoint.Y - ( size.Height * centerPoint.Y );
				return r;
			}
			/// <summary> Creates a scale transformation that has the specified scale factors and center point. </summary>
			/// <param name="x">The x-axis scale factor of the scale transformation.</param>
			/// <param name="y">The y-axis scale factor of the scale transformation.</param>
			/// <param name="centerPoint">The point about which the scale is performed.</param>
			static Matrix3x2 Scale( Single x, Single y, Point centerPoint )	{
				Matrix3x2 r;
				r.M11 = x;		r.M12 = 0.0f;
				r.M21 = 0.0f;	r.M22 = y;
				r.M31 = centerPoint.X - ( x * centerPoint.X );
				r.M32 = centerPoint.Y - ( y * centerPoint.Y );
				return r;
			}
			/// <summary> Creates a skew transformation that has the specified x-axis and y-axis values and center point. </summary>
			/// <param name="angleX">The x-axis skew angle, which is measured in degrees counterclockwise from the y-axis.</param>
			/// <param name="angleY">The y-axis skew angle, which is measured in degrees clockwise from the x-axis.</param>
			/// <param name="centerPoint">The point about which the skew is performed.</param>
			static Matrix3x2 Skew( Single angleX, Single angleY, Point centerPoint ) {
				D2D1_POINT_2F centerPoint_n = D2D1::Point2F( centerPoint.X, centerPoint.Y );

				Matrix3x2 r;

				D2D1::Matrix3x2F mat = D2D1::Matrix3x2F::Skew( angleX, angleY, centerPoint_n );

				r.M11 = mat._11; r.M12 = mat._12;
				r.M21 = mat._21; r.M22 = mat._22;
				r.M31 = mat._31; r.M32 = mat._32;

				return r;
			}
			/// <summary> Creates a translation transformation that has the specified x and y displacements. </summary>
			/// <param name="point">The distance to translate along the x-axis and the y-axis.</param>
			static Matrix3x2 Translation( Point point ) {
				Matrix3x2 r;
				r.M11 = 1.0f;		r.M12 = 0.0f;
				r.M21 = 0.0f;		r.M22 = 1.0f;
				r.M31 = point.X;	r.M32 = point.Y;
				return r;
			}
			/// <summary> Creates a translation transformation that has the specified x and y displacements. </summary>
			/// <param name="x">The distance to translate along the x-axis.</param>
			/// <param name="y">The distance to translate along the y-axis.</param>
			static Matrix3x2 Translation( Single x, Single y ) {
				Matrix3x2 r;
				r.M11 = 1.0f;	r.M12 = 0.0f;
				r.M21 = 0.0f;	r.M22 = 1.0f;
				r.M31 = x;		r.M32 = y;
				return r;
			}
			/// <summary> Transform the specified point. </summary>
			/// <param name="mat">The matrix to use.</param>
			/// <param name="point">The point to transform.</param>
			static Point TransformPoint( Matrix3x2 mat, Point point ) {
				Point r;
				r.X = ( point.X * mat.M11 ) + ( point.Y * mat.M21 ) + mat.M31;
				r.Y = ( point.X * mat.M12 ) + ( point.Y * mat.M22 ) + mat.M32;
				return r;
			}

			/// <summary> Calculates the determinant of the matrix. </summary>
			Single Determinant() {
				return ( M11 * M22 ) - ( M12 * M21 );
			}
			/// <summary> Inverts the matrix, if it is invertible. </summary>
			Boolean Invert() {
				pin_ptr<Matrix3x2> pinnedThis = this;
				return ( D2D1::Matrix3x2F::ReinterpretBaseType( (D2D1_MATRIX_3X2_F*)pinnedThis ) )->Invert();
			}

			static Matrix3x2 operator * ( Matrix3x2 left, Matrix3x2 right ) {
				Matrix3x2 r;
				r.M11 = ( left.M11 * right.M11 ) + ( left.M12 * right.M21 );
				r.M12 = ( left.M11 * right.M12 ) + ( left.M12 * right.M22 );
				r.M21 = ( left.M21 * right.M11 ) + ( left.M22 * right.M21 );
				r.M22 = ( left.M21 * right.M12 ) + ( left.M22 * right.M22 );
				r.M31 = ( left.M31 * right.M11 ) + ( left.M32 * right.M21 ) + right.M31;
				r.M32 = ( left.M31 * right.M12 ) + ( left.M32 * right.M22 ) + right.M32;
				return r;
			}
			static Boolean operator == ( Matrix3x2 left, Matrix3x2 right ) {
				return Matrix3x2::Equals( left, right );
			}
			static Boolean operator != ( Matrix3x2 left, Matrix3x2 right ) {
				return !Matrix3x2::Equals( left, right );
			}

			virtual System::String^ ToString() override	{
				return String::Format( System::Globalization::CultureInfo::CurrentCulture, "[[M11:{0} M12:{1}] [M21:{2} M22:{3}] [M31:{4} M32:{5}]]",
									   M11.ToString( System::Globalization::CultureInfo::CurrentCulture ), M12.ToString( System::Globalization::CultureInfo::CurrentCulture ),
									   M21.ToString( System::Globalization::CultureInfo::CurrentCulture ), M22.ToString( System::Globalization::CultureInfo::CurrentCulture ),
									   M31.ToString( System::Globalization::CultureInfo::CurrentCulture ), M32.ToString( System::Globalization::CultureInfo::CurrentCulture ) );
			}

			virtual Int32 GetHashCode() override {
				return M11.GetHashCode() + M12.GetHashCode() + M21.GetHashCode() + M22.GetHashCode() + M31.GetHashCode() + M32.GetHashCode();
			}

			virtual Boolean Equals( System::Object^ obj ) override{
				if ( obj == nullptr )
					return false;
				if ( obj->GetType() != GetType() )
					return false;
				return Equals( safe_cast<Matrix3x2>( obj ) );
			}
			virtual Boolean Equals( Matrix3x2 other ) {
				return ( M11 == other.M11 && M12 == other.M12 &&
						 M21 == other.M21 && M22 == other.M22 &&
						 M31 == other.M31 && M32 == other.M32 );
			}
			static Boolean Equals( Matrix3x2% value1, Matrix3x2% value2 ) {
				return ( value1.M11 == value2.M11 && value1.M12 == value2.M12 &&
						 value1.M21 == value2.M21 && value1.M22 == value2.M22 &&
						 value1.M31 == value2.M31 && value1.M32 == value2.M32 );
			}
		};
	}
}
