#pragma once

#include "..\Graphics.hpp"

#include "WicBitmapSource.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			/// <summary> Represents a resized version of the input bitmap using a resampling or filtering algorithm. </summary>
			public ref class WicScaler : WicBitmapSource
			{
				GRAPHICS_OBJECT( WicScaler, IWICBitmapScaler );

			public:

				/// <summary> Create a new instance of WicScaler. </summary>
				WicScaler();

				/// <summary> Perform the scaling. </summary>
				/// <param name="source">The input bitmap source.</param>
				/// <param name="width">The destination width.</param>
				/// <param name="height">The destination height.</param>
				/// <param name="mode">The BitmapInterpolationMode to use when scaling.</param>
				void Scale( WicBitmapSource^ source, Int32 width, Int32 height, WicInterpolationMode mode );
			};
		}
	}
}

