#pragma once

#include "..\Graphics.hpp"
#include "..\Common.hpp"

#include "WicBitmapSource.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			ref class WicPalette;

			[Flags]
			public enum class WICBitmapLockFlags
			{
				Read = WICBitmapLockRead,
				Write = WICBitmapLockWrite
			};

			public ref class WicBitmapLock : GraphicsObject
			{
				GRAPHICS_OBJECT( WicBitmapLock, IWICBitmapLock );
			};

			public ref class WicBitmap : WicBitmapSource
			{
				GRAPHICS_OBJECT( WicBitmap, IWICBitmap );

			public:

				void SetPalette( WicPalette^ palette );
				void SetResolution( Double dpiX, Double dpiY );

				WicBitmapLock^ Lock( Kodo::Graphics::Rectangle lockRect, WICBitmapLockFlags flags );
			};
		}
	}
}

