#include "WicStream.hpp"
#include "WicFactory.hpp"

#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			ref class StreamWrapper : public System::Runtime::InteropServices::ComTypes::IStream
			{
				System::IO::Stream^ _stream;

			public:

				StreamWrapper( System::IO::Stream^ stream )
				{
					if ( stream == nullptr )
						throw gcnew ArgumentNullException( "stream" );
					_stream = stream;
				}

			protected:

				virtual void Clone( System::Runtime::InteropServices::ComTypes::IStream^% ppstm ) = IStream::Clone
				{
					ppstm = nullptr;
				}

					virtual void Commit( Int32 grfCommitFlags ) = IStream::Commit
				{
				}

				virtual void CopyTo( System::Runtime::InteropServices::ComTypes::IStream^ pstm, __int64 cb, IntPtr pcbRead, IntPtr pcbWritten ) = IStream::CopyTo
				{
				}

				virtual void LockRegion( __int64 libOffset, __int64 cb, Int32 dwLockType ) = IStream::LockRegion
				{
				}

				virtual void Read( cli::array<unsigned char>^ pv, Int32 cb, System::IntPtr pcbRead ) = IStream::Read
				{
					if ( pv == nullptr )
						throw gcnew ArgumentNullException();
					if ( pcbRead == IntPtr::Zero )
						throw gcnew ArgumentNullException();

					try
					{
						auto value = ( System::Int32 )_stream->Read( pv, 0, cb );
						//if ( value > 0 )
						{

							Marshal::WriteInt32( pcbRead, value );
						}
					}
					catch ( AccessViolationException^ e )
					{
						UNREFERENCED_PARAMETER( e );
						throw;
					}
				}

					virtual void Revert() = IStream::Revert
				{
				}

				virtual void Seek( __int64 dlibMove, Int32 dwOrigin, IntPtr plibNewPosition ) = IStream::Seek
				{
					__int64* pNewPosition = (__int64*)plibNewPosition.ToPointer();
					__int64 newPosition = _stream->Seek( dlibMove, ( System::IO::SeekOrigin )dwOrigin );

					if ( pNewPosition != nullptr )
						*( pNewPosition ) = newPosition;
				}

					virtual void SetSize( __int64 libNewSize ) = IStream::SetSize
				{
				}

				virtual void Stat( System::Runtime::InteropServices::ComTypes::STATSTG% pstatstg, Int32 grfStatFlag ) = IStream::Stat
				{
					if ( ( grfStatFlag & STATFLAG_NONAME ) == 0 )
					{
						pstatstg.pwcsName = gcnew String( L"no name" );
					}

					pstatstg.type = STGTY_STREAM;
					pstatstg.cbSize = _stream->Length;
					pstatstg.mtime.dwHighDateTime = 0;
					pstatstg.mtime.dwLowDateTime = 0;
					pstatstg.atime.dwHighDateTime = 0;
					pstatstg.atime.dwLowDateTime = 0;
					pstatstg.grfMode = 0;
					pstatstg.grfLocksSupported = 0;
					pstatstg.clsid = Guid( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
					pstatstg.grfStateBits = 0;
				}

					virtual void UnlockRegion( __int64 libOffset, __int64 cb, Int32 dwLockType ) = IStream::UnlockRegion
				{
				}

				virtual void Write( cli::array<unsigned char>^ pv, Int32 cb, IntPtr pcbWritten ) = IStream::Write
				{
				}
			};

			void WicStream::Create()
			{
				IWICStream* stream;

				ThrowIfFailed<GraphicsException>( GlobalAssets::FactoryWic->Native->CreateStream( &stream ) );

				Assing( stream );
			}

			void WicStream::Initialize( Type^ type, String^ resourceName )
			{
				System::IO::Stream^ stream = type->Assembly->GetManifestResourceStream( type, resourceName );
				Initialize( stream );
			}

			void WicStream::Initialize( String^ fileName, WicDesiredAccess desiredAccess )
			{
				pin_ptr<const System::Char> pFileName = PtrToStringChars( fileName );
				ThrowIfFailed<GraphicsException>( Native->InitializeFromFilename( pFileName, (DWORD)desiredAccess ) );
			}

			void WicStream::Initialize( System::IO::Stream^ stream )
			{
				auto wrapper = gcnew StreamWrapper( stream );
				auto pStream = Marshal::GetComInterfaceForObject( wrapper, System::Runtime::InteropServices::ComTypes::IStream::typeid );

				try
				{
					ThrowIfFailed<GraphicsException>( Native->InitializeFromIStream( (IStream*)pStream.ToPointer() ) );
				}
				finally
				{
					Marshal::Release( pStream );
				}
			}

			void WicStream::Initialize( System::IO::Stream^ stream, UInt64 offset, UInt64 maxSize )
			{
				StreamWrapper ^wrapper = gcnew StreamWrapper( stream );
				IntPtr pStream = Marshal::GetComInterfaceForObject( wrapper, System::Runtime::InteropServices::ComTypes::IStream::typeid );

				try
				{
					ThrowIfFailed<GraphicsException>( Native->InitializeFromIStreamRegion(
						(IStream*)pStream.ToPointer(),
						*(ULARGE_INTEGER *)&offset,
						*(ULARGE_INTEGER *)&maxSize ) );
				}
				finally
				{
					Marshal::Release( pStream );
				}
			}

			void WicStream::Initialize( array<Byte>^ buffer )
			{
				if ( buffer == nullptr )
					throw gcnew ArgumentNullException( "buffer" );

				pin_ptr<Byte> pBuffer = &buffer[ 0 ];

				ThrowIfFailed<GraphicsException>( Native->InitializeFromMemory( (BYTE*)pBuffer, buffer->Length ) );
			}
		}
	}
}