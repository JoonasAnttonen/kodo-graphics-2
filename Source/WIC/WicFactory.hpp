#pragma once

#include "..\Graphics.hpp"

#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			using namespace System;
			using namespace System::IO;
			using namespace System::Runtime::InteropServices;

			ref class WicBitmap;

			public ref class WicFactory : GraphicsObject
			{
				GRAPHICS_OBJECT( WicFactory, IWICImagingFactory );

			public:

				WicFactory();

				WicBitmap^ CreateBitmap( Int32 width, Int32 height, Guid pixelFormat, WicBitmapCreateCacheOption option );
			};
		}
	}
}