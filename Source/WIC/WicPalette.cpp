#include "WicPalette.hpp"

#include "WicBitmapSource.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			array<WicColor>^ WicPalette::GetColors()
			{
				IWICPalette* palette = Native;
				UINT count;
				ThrowIfFailed<GraphicsException>( palette->GetColorCount( &count ) );

				array<WicColor>^ colors = gcnew array<WicColor>( count );
				pin_ptr<WicColor> pColors = &colors[ 0 ];
				UINT actualColors;
				ThrowIfFailed<GraphicsException>( palette->GetColors( count, (WICColor*)pColors, &actualColors ) );
				return colors;
			}

			void WicPalette::Initialize( array<WicColor>^ colors )
			{
				if ( colors == nullptr )
					throw gcnew ArgumentNullException( "colors" );

				pin_ptr<WicColor> pColors = &colors[ 0 ];
				ThrowIfFailed<GraphicsException>( Native->InitializeCustom( (WICColor*)pColors, colors->Length ) );
			}

			void WicPalette::Initialize( WicBitmapSource^ surface, Int32 colorCount, Boolean addTransparentColor )
			{
				ThrowIfFailed<GraphicsException>( Native->InitializeFromBitmap( surface->Native, (UINT)colorCount, (BOOL)addTransparentColor ) );
			}

			void WicPalette::Initialize( WicPalette^ source )
			{
				ThrowIfFailed<GraphicsException>( Native->InitializeFromPalette( source->Native ) );
			}

			void WicPalette::Initialize( WICBitmapPaletteType paletteType, Boolean addTransparentColor )
			{
				ThrowIfFailed<GraphicsException>( Native->InitializePredefined( (WICBitmapPaletteType)paletteType, (BOOL)addTransparentColor ) );
			}
		}
	}
}
