#pragma once

using namespace System;
using namespace System::Runtime::InteropServices;

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			[StructLayout( LayoutKind::Sequential )]
			public value struct WicColor
			{
			public:
				Byte R;
				Byte G;
				Byte B;
				Byte A;
			};
			
			public enum class BitmapCreateCacheOption
			{
				NoCache = 0x00000000,
				CacheOnDemand = 0x00000001,
				CacheOnLoad = 0x00000002
			};

			public enum class WicDecodeOptions
			{
				MetadataCacheOnDemand = 0x00000000,
				MetadataCacheOnLoad = 0x00000001
			};

			/// <summary>
			/// Specifies the sampling or filtering mode to use when scaling an image.
			/// </summary>
			public enum class WicInterpolationMode
			{
				/// <summary>
				/// A nearest neighbor interpolation algorithm. Also known as nearest pixel or point interpolation.
				/// The output pixel is assigned the value of the pixel that the point falls within. No other pixels are considered.
				/// </summary>
				NearestNeighbor = 0x00000000,
				/// <summary>
				/// A bilinear interpolation algorithm. 
				/// The output pixel values are computed as a weighted average of the nearest four pixels in a 2x2 grid.
				/// </summary>
				Linear = 0x00000001,
				/// <summary>
				/// A bicubic interpolation algorithm. 
				/// Destination pixel values are computed as a weighted average of the nearest sixteen pixels in a 4x4 grid.
				/// </summary>
				Cubic = 0x00000002,
				/// <summary>
				/// A Fant resampling algorithm. 
				/// Destination pixel values are computed as a weighted average of the all the pixels that map to the new pixel.
				/// </summary>
				Fant = 0x00000003
			};

			public enum class WicPaletteType
			{
				Custom = 0x00000000,
				MedianCut = 0x00000001,
				FixedBW = 0x00000002,
				FixedHalftone8 = 0x00000003,
				FixedHalftone27 = 0x00000004,
				FixedHalftone64 = 0x00000005,
				FixedHalftone125 = 0x00000006,
				FixedHalftone216 = 0x00000007,
				FixedWebPalette = FixedHalftone216,
				FixedHalftone252 = 0x00000008,
				FixedHalftone256 = 0x00000009,
				FixedGray4 = 0x0000000A,
				FixedGray16 = 0x0000000B,
				FixedGray256 = 0x0000000C
			};

			public enum class WicDitherType
			{
				None = 0x00000000,
				Solid = 0x00000000,
				Ordered4x4 = 0x00000001,
				Ordered8x8 = 0x00000002,
				Ordered16x16 = 0x00000003,
				Spiral4x4 = 0x00000004,
				Spiral8x8 = 0x00000005,
				DualSpiral4x4 = 0x00000006,
				DualSpiral8x8 = 0x00000007,
				ErrorDiffusion = 0x00000008
			};

			public enum class WicDesiredAccess
			{
				Read = (Int32)GENERIC_READ,
				Write = (Int32)GENERIC_WRITE
			};

			[StructLayout( LayoutKind::Sequential )]
			public value struct WicRect
			{
			public:
				Int32 X;
				Int32 Y;
				Int32 Width;
				Int32 Height;
			};
		}
	}
}