#include "WicBitmapSource.hpp"

#include "WicPalette.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			void WicBitmapSource::GetResolution( [ OutAttribute() ]Double% dpiX, [ OutAttribute() ]Double% dpiY )
			{
				double x, y;
				ThrowIfFailed<GraphicsException>( Native->GetResolution( (double*)&x, (double*)&y ) );
				dpiX = x;
				dpiY = y;
			}

			void WicBitmapSource::CopyPalette( WicPalette^ palette )
			{
				ThrowIfFailed<GraphicsException>( Native->CopyPalette( palette->Native ) );
			}

			void WicBitmapSource::CopyPixels( WicRect rect, Int32 stride, array<Byte>^ buffer )
			{
				pin_ptr<Byte> pBuffer = &buffer[ 0 ];
				ThrowIfFailed<GraphicsException>( Native->CopyPixels( (WICRect*)&rect, (UINT)stride, buffer->Length, (BYTE*)pBuffer ) );
			}
		}
	}
}
