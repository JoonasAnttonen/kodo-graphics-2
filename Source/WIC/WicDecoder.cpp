#include "WicDecoder.hpp"

#include "WicFactory.hpp"
#include "WicStream.hpp"

#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			WicDecoder::WicDecoder( WicStream^ stream, Guid vendor, WicDecodeOptions options )
			{
				IWICBitmapDecoder* decoder;

				ThrowIfFailed<GraphicsException>( GlobalAssets::FactoryWic->Native->CreateDecoderFromStream(
					(IStream*)stream->Native,
					( vendor == Guid::Empty ? nullptr : (GUID*)&vendor ),
					(WICDecodeOptions)options,
					&decoder )
				);

				Assing( decoder );
			}

			WicDecoder::WicDecoder( String^ fileName, Guid vendor, WicDesiredAccess desiredAccess, WicDecodeOptions options )
			{
				pin_ptr<const System::Char> pFileName = PtrToStringChars( fileName );

				IWICBitmapDecoder* decoder;

				ThrowIfFailed<GraphicsException>( GlobalAssets::FactoryWic->Native->CreateDecoderFromFilename(
					pFileName,
					( vendor == Guid::Empty ? nullptr : (GUID*)&vendor ),
					(DWORD)desiredAccess,
					(WICDecodeOptions)options,
					&decoder )
				);

				Assing( decoder );
			}
		}
	}
}
