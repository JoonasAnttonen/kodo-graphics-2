#include "WicFactory.hpp"

#include "WicBitmap.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			WicFactory::WicFactory()
			{
				IWICImagingFactory* factory;

				ThrowIfFailed<GraphicsException>( CoCreateInstance(
					CLSID_WICImagingFactory,
					nullptr,
					CLSCTX_INPROC_SERVER,
					IID_IWICImagingFactory,
					(LPVOID*)&factory ) );

				Assing( factory );
			}

			WicBitmap^ WicFactory::CreateBitmap( Int32 width, Int32 height, Guid pixelFormat, WicBitmapCreateCacheOption option )
			{
				IWICBitmap *bitmap;
				ThrowIfFailed<GraphicsException>( Native->CreateBitmap( width, height, *(GUID*)&pixelFormat, (WICBitmapCreateCacheOption)option, &bitmap ) );
				return gcnew WicBitmap( bitmap );
			}
		}
	}
}