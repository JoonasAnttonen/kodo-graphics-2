#pragma once

#include "..\Graphics.hpp"

#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			/// <summary> Represents a Windows Imaging Component (WIC) stream for referencing imaging and metadata content. </summary>
			public ref class WicStream : GraphicsObject
			{
				GRAPHICS_OBJECT( WicStream, IWICStream );

			public:

				/// <summary>
				/// Initializes a stream from a resource.
				/// </summary>
				/// <param name="type">The type of the resource.</param>
				/// <param name="resourceName">The name of the resource.</param>
				WicStream( Type^ type, String^ resourceName ) { Create(); Initialize( type, resourceName ); }
				/// <summary>
				/// Initializes a stream from a file.
				/// </summary>
				/// <param name="fileName">The name of the file.</param>
				WicStream( String^ fileName ) { Create(); Initialize( fileName, WicDesiredAccess::Read ); }
				/// <summary>
				/// Initializes a stream from a file.
				/// </summary>
				/// <param name="fileName">The name of the file.</param>
				/// <param name="desiredAccess">The desired file access.</param>
				WicStream( String^ fileName, WicDesiredAccess desiredAccess ) { Create(); Initialize( fileName, desiredAccess ); }
				/// <summary>
				/// Initializes a stream from an existing stream.
				/// </summary>
				/// <param name="stream">The stream.</param>
				WicStream( System::IO::Stream^ stream ) 
				{ 
					Create(); 
					Initialize( stream ); 
				}
				/// <summary>
				/// Initializes a stream from an existing stream.
				/// </summary>
				/// <param name="stream">The stream.</param>
				/// <param name="offset">The stream in to the stream.</param>
				/// <param name="maxSize">The maximum size of the stream.</param>
				WicStream( System::IO::Stream^ stream, UInt64 offset, UInt64 maxSize ) { Create(); Initialize( stream, offset, maxSize ); }
				/// <summary>
				/// Initializes a stream from an array of bytes.
				/// </summary>
				/// <param name="buffer">The array to initialize from.</param>
				WicStream( array<Byte>^ buffer ) { Create(); Initialize( buffer ); }


			private:

				void Create();
				void Initialize( Type^ type, String^ resourceName );
				void Initialize( String^ fileName, WicDesiredAccess desiredAccess );
				void Initialize( System::IO::Stream^ stream );
				void Initialize( System::IO::Stream^ stream, UInt64 offset, UInt64 maxSize );
				void Initialize( array<Byte>^ buffer );
			};
		}
	}
}
