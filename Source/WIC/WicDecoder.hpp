#pragma once

#include "..\Graphics.hpp"

#include "WicBitmapSource.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			ref class WicFactory;
			ref class WicStream;

			/// <summary> This class provides methods for decoding individual image frames of an encoded file. </summary>
			public ref class WicDecoderFrame : WicBitmapSource
			{
				GRAPHICS_OBJECT( WicDecoderFrame, IWICBitmapFrameDecode );
			};

			/// <summary> This class provides access to the decoder's properties such as global thumbnails (if supported), frames, and palette. </summary>
			public ref class WicDecoder : GraphicsObject
			{
				GRAPHICS_OBJECT( WicDecoder, IWICBitmapDecoder );

			public:

				/// <summary> Creates a new instance of <see cref="WicDecoder"/> from a stream. </summary>
				/// <param name="stream">The stream.</param>
				/// <param name="options">The decode options.</param>
				WicDecoder( WicStream^ stream, WicDecodeOptions options ) : WicDecoder( stream, Guid::Empty, options ) { }
				/// <summary> Creates a new instance of <see cref="WicDecoder"/> from a stream. </summary>
				/// <param name="stream">The stream.</param>
				/// <param name="vendor">The vendor guid.</param>
				/// <param name="options">The decode options.</param>
				WicDecoder( WicStream^ stream, Guid vendor, WicDecodeOptions options );

				/// <summary> Creates a new instance of <see cref="WicDecoder"/> from a file. </summary>
				/// <param name="filename">The filename.</param>
				/// <param name="options">The decode options.</param>
				WicDecoder( String^ filename, WicDecodeOptions options ) : WicDecoder( filename, Guid::Empty, WicDesiredAccess::Read, options ) { }
				/// <summary> Creates a new instance of <see cref="WicDecoder"/> from a file. </summary>
				/// <param name="filename">The filename.</param>
				/// <param name="vendor">The vendor guid.</param>
				/// <param name="desiredAccess">The desired file access.</param>
				/// <param name="options">The decode options.</param>
				WicDecoder( String^ filename, Guid vendor, WicDesiredAccess desiredAccess, WicDecodeOptions options );

				/// <summary> Get the total number of frames in the image. </summary>
				Int32 GetFrameCount()
				{
					UINT count;
					ThrowIfFailed<GraphicsException>( Native->GetFrameCount( &count ) );
					return (Int32)count;
				}

				/// <summary>
				/// Get the specified frame of the image.
				/// </summary>
				WicDecoderFrame^ GetFrame( Int32 index )
				{
					IWICBitmapFrameDecode* frame;
					ThrowIfFailed<GraphicsException>( Native->GetFrame( (UINT)index, &frame ) );
					return gcnew WicDecoderFrame( frame );
				}
			};
		}
	}
}
