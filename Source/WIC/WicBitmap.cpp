#include "WicBitmap.hpp"

#include "WicPalette.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			void WicBitmap::SetPalette( WicPalette^ palette )
			{
				if ( palette == nullptr )
					throw gcnew ArgumentNullException( "palette" );
				ThrowIfFailed<GraphicsException>( Native->SetPalette( palette->Native ) );
			}

			void WicBitmap::SetResolution( Double dpiX, Double dpiY )
			{
				ThrowIfFailed<GraphicsException>( Native->SetResolution( dpiX, dpiY ) );
			}

			WicBitmapLock^ WicBitmap::Lock( Kodo::Graphics::Rectangle lockRect, WICBitmapLockFlags flags )
			{
				WICRect rect = { static_cast<Int32>( lockRect.Left ), static_cast<Int32>( lockRect.Top ), static_cast<Int32>( lockRect.Width ), static_cast<Int32>( lockRect.Height ) };
				IWICBitmapLock* pLock;
				ThrowIfFailed<GraphicsException>( Native->Lock( &rect, (DWORD)flags, &pLock ) );
				return gcnew WicBitmapLock( pLock );
			}
		}
	}
}
