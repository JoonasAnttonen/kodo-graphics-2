#include "WicScaler.hpp"

#include "WicFactory.hpp"

#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			WicScaler::WicScaler()
			{
				IWICBitmapScaler* scaler;
				ThrowIfFailed<GraphicsException>( GlobalAssets::FactoryWic->Native->CreateBitmapScaler( &scaler ) );
				Assing( scaler );
			}

			void WicScaler::Scale( WicBitmapSource^ source, Int32 width, Int32 height, WicInterpolationMode mode )
			{
				ThrowIfFailed<GraphicsException>( Native->Initialize( source->Native, width, height, (WICBitmapInterpolationMode)mode ) );
			}
		}
	}
}
