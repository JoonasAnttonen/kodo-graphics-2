#pragma once

#include "..\Graphics.hpp"

#include "Common.hpp"
#include "WicBitmapSource.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			ref class WicPalette;

			/// <summary> Represents an <see cref="WicBitmapSource"/> that converts the image data from one pixel format to another, handling dithering and halftoning to indexed formats, palette translation and alpha thresholding. </summary>
			public ref class WicFormatConverter : WicBitmapSource
			{
				GRAPHICS_OBJECT( WicFormatConverter, IWICFormatConverter );

			public:

				/// <summary> Create a new instance of <see cref="WicFormatConverter"/>. </summary>
				WicFormatConverter();

				/// <summary> Determines if the source pixel format can be converted to the destination pixel format. </summary>
				/// <param name="srcPixelFormat">The source pixel format.</param>
				/// <param name="dstPixelFormat">The destination pixel format.</param>
				Boolean CanConvert( Guid srcPixelFormat, Guid dstPixelFormat )
				{
					BOOL value;
					ThrowIfFailed<GraphicsException>( Native->CanConvert( *(GUID*)&srcPixelFormat, *(GUID*)&dstPixelFormat, &value ) );
					return ( value != 0 );
				}

				/// <summary> Performs the conversion with no color loss mitigation. </summary>
				/// <param name="source">The input bitmap to convert.</param>
				/// <param name="dstPixelFormat">The destination pixel format GUID.</param>
				void Convert( WicBitmapSource^ source, Guid dstPixelFormat ) { Convert( source, dstPixelFormat, WicDitherType::None, nullptr, 0.0, WicPaletteType::Custom ); }

				/// <summary> Performs the conversion. </summary>
				/// <param name="source">The input bitmap to convert.</param>
				/// <param name="dstPixelFormat">The destination pixel format GUID.</param>
				/// <param name="dither">The <see cref="WicDitherType"/> used for conversion.</param>
				/// <param name="palette">The palette to use for conversion.</param>
				/// <param name="alphaThresholdPercent">The alpha threshold to use for conversion.</param>
				/// <param name="paletteType">The palette translation type to use for conversion.</param>
				void Convert( WicBitmapSource^ source, Guid dstPixelFormat, WicDitherType dither, WicPalette^ palette, Double alphaThresholdPercent, WicPaletteType paletteType );
			};
		}
	}
}
