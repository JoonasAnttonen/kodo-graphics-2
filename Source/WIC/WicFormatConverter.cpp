#include "WicFormatConverter.hpp"

#include "WicPalette.hpp"

#include "WicFactory.hpp"
#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			WicFormatConverter::WicFormatConverter()
			{
				IWICFormatConverter* converter;
				ThrowIfFailed<GraphicsException>( GlobalAssets::FactoryWic->Native->CreateFormatConverter( &converter ) );
				Assing( converter );
			}

			void WicFormatConverter::Convert( WicBitmapSource^ source, Guid dstPixelFormat, WicDitherType dither, WicPalette^ palette, Double alphaThresholdPercent, WicPaletteType paletteType )
			{
				IWICPalette *p = ( palette == nullptr ? nullptr : palette->Native );

				ThrowIfFailed<GraphicsException>( Native->Initialize(
					source->Native,
					*(GUID*)&dstPixelFormat,
					(WICBitmapDitherType)dither,
					p,
					alphaThresholdPercent,
					(WICBitmapPaletteType)paletteType ) );
			}
		}
	}
}
