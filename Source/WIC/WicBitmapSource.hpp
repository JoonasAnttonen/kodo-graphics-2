#pragma once

#include "..\Graphics.hpp"
#include "..\Common.hpp"

#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			using namespace System;
			using namespace System::Runtime::InteropServices;

			ref class WicPalette;

			public ref class WicBitmapSource : GraphicsObject
			{
				GRAPHICS_OBJECT( WicBitmapSource, IWICBitmapSource );

			public:

				property Guid PixelFormat
				{
					Guid get() {
						Guid guid;
						ThrowIfFailed<GraphicsException>( Native->GetPixelFormat( (WICPixelFormatGUID*)&guid ) );
						return guid;
					}
				}

				property Kodo::Graphics::Size Size
				{
					Kodo::Graphics::Size get() {
						UINT32 width, height;
						ThrowIfFailed<GraphicsException>( Native->GetSize( &width, &height ) );
						return Kodo::Graphics::Size( Single( width ), Single( height ) );
					}
				}

				WicBitmapSource() { }

				void CopyPalette( WicPalette^ palette );
				void CopyPixels( WicRect rect, Int32 stride, array<Byte>^ buffer );
				void GetResolution( [ OutAttribute() ]Double% dpiX, [ OutAttribute() ]Double% dpiY );
			};
		}
	}
}