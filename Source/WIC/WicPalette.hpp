#pragma once

#include "..\Graphics.hpp"

#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		namespace Imaging
		{
			ref class WicBitmapSource;

			public ref class WicPalette : GraphicsObject
			{
				GRAPHICS_OBJECT( WicPalette, IWICPalette );

			public:

				property WicPaletteType PaletteType
				{
					WicPaletteType get() {
						WICBitmapPaletteType value;
						ThrowIfFailed<GraphicsException>( Native->GetType( &value ) );
						return (WicPaletteType)value;
					}
				}
				property Boolean HasAlpha
				{
					Boolean get() {
						BOOL value;
						ThrowIfFailed<GraphicsException>( Native->HasAlpha( &value ) );
						return value != 0;
					}
				}

				property Boolean IsBlackWhite
				{
					Boolean get() {
						BOOL value;
						ThrowIfFailed<GraphicsException>( Native->IsBlackWhite( &value ) );
						return value != 0;
					}
				}
				property Boolean IsGrayscale
				{
					Boolean get() {
						BOOL value;
						ThrowIfFailed<GraphicsException>( Native->IsGrayscale( &value ) );
						return value != 0;
					}
				}

				array<WicColor>^ GetColors();
				void Initialize( array<WicColor>^ colors );
				void Initialize( WicBitmapSource^ surface, Int32 colorCount, Boolean addTransparentColor );
				void Initialize( WicPalette^ source );
				void Initialize( WICBitmapPaletteType paletteType, Boolean addTransparentColor );
			};
		}
	}
}

