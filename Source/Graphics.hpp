#pragma once

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "shell32.lib")
#pragma comment(lib, "Windowscodecs.lib")
#pragma comment(lib, "Ole32.lib")
#pragma comment(lib, "dwmapi.lib") 

#include <SDKDDKVer.h>

#include <stdint.h>
#include <memory>
#include <string>
#include <vector>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#include <vcclr.h>
#include <dwmapi.h>
#include <unknwn.h>
#include <shellapi.h>
#include <Wincodec.h>
#include <comdef.h>

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d2d1.lib")

#include <d3d11.h>
#include <dxgi1_3.h>
#include <d2d1_2.h>
#include <d2d1_1helper.h>
#include <d2d1_2helper.h>

#pragma comment(lib, "dwrite.lib")
#include <dwrite_2.h>

using namespace System;

template<class T, class U>
static System::Boolean Is( U u )
{
	return dynamic_cast<T>( u ) != nullptr;
}

template<typename E>
static void ThrowIfFailed( HRESULT result, String^ message )
{
	if ( FAILED( result ) )
	{
		throw gcnew E( message );
	}
}
template<typename E>
static void ThrowIfFailed( HRESULT result )
{
	if ( FAILED( result ) )
	{
		throw gcnew E( gcnew String( _com_error( result ).ErrorMessage() ) );
	}
}

template<typename E, typename T>
static void ThrowIfNull( T thing, String^ message )
{
	if ( thing == nullptr )
	{
		throw gcnew E( message );
	}
}
template<typename E, typename T>
static void ThrowIfNull( T thing ) { ThrowIfNull<E>( thing, "This was null!" ); }

template<typename E, typename T>
static void ThrowIfZero( T thing, String^ message ) {
	if ( thing == 0 )
		throw gcnew E( message );
}
template<typename E, typename T>
static void ThrowIfZero( T thing ) { ThrowIfZero<E>( thing, "This was 0!" ); }

template<typename E, typename T>
static void ThrowIf( T thing, String^ message )
{
	if ( thing == TRUE )
	{
		throw gcnew E( message );
	}
}
template<typename E, typename T>
static void ThrowIf( T thing ) { ThrowIf<E>( thing, "Thing was true!" ); }

#ifndef GRAPHICS_OBJECT
#define GRAPHICS_OBJECT(managed_type, native_type) \
	internal: \
		property native_type* Native { native_type* get() { return static_cast<native_type*>( _native ); } } \
		managed_type( native_type* n ) { _native = n; }
#endif

namespace Kodo
{
	namespace Graphics
	{
		/// <summary>
		/// Represents errors that occur within the graphics system.
		/// </summary>
		public ref class GraphicsException : Exception
		{
		public:
			GraphicsException( Int32 hr ) : Exception( String::Format( System::Globalization::CultureInfo::CurrentCulture, "Graphics operation failed with HRESULT: {0}", hr ) ) { }
			GraphicsException( String^ message ) : Exception( message ) { }
			GraphicsException( Int32 hr, String^ message ) : Exception( String::Format( System::Globalization::CultureInfo::CurrentCulture, "{0} HRESULT: {1}", message, hr ) ) { }
		};

		public ref class GraphicsObject abstract
		{
		internal:

			IUnknown* _native;

			GraphicsObject() : _native( nullptr ) { }
			GraphicsObject( IUnknown* n ) : _native( n ) { }

			void Assing( IUnknown* native ) {
				System::Diagnostics::Debug::Assert( _native == nullptr, "This object has already been assigned!" );
				_native = native;
			}

		public:

			virtual ~GraphicsObject() {
				this->!GraphicsObject();
				GC::SuppressFinalize( this );
			}

			!GraphicsObject() {
				if ( _native ) {
					_native->Release();
					_native = nullptr;
				}
			}
		};
	}
}