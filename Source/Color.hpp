#pragma once

namespace Kodo
{
	namespace Graphics
	{
		using namespace System;
		using namespace System::Runtime::InteropServices;

		/// <summary> A four-component (RGBA) color value; each component is a Single in the range [0,1]. </summary>
		[StructLayout( LayoutKind::Sequential )]
		public value class Color
		{
		public:

			/// <summary> Gets or sets the color's red component. </summary>
			Single Red;
			/// <summary> Gets or sets the color's green component. </summary>
			Single Green;
			/// <summary> Gets or sets the color's blue component. </summary>
			Single Blue;
			/// <summary> Gets or sets the color's alpha component. </summary>
			Single Alpha;

		public:

			Color( Single alpha, Color color )
			{
				Alpha = alpha;
				Red = color.Red;
				Green = color.Green;
				Blue = color.Blue;
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="Color"/> structure.
			/// </summary>
			/// <param name="alpha">The alpha component of the color.</param>
			/// <param name="red">The red component of the color.</param>
			/// <param name="green">The green component of the color.</param>
			/// <param name="blue">The blue component of the color.</param>
			Color( Single alpha, Single red, Single green, Single blue )
			{
				Alpha = alpha;
				Red = red;
				Green = green;
				Blue = blue;
			}
			/// <summary>
			/// Initializes a new instance of the <see cref="Color"/> structure.
			/// </summary>
			/// <param name="red">The red component of the color.</param>
			/// <param name="green">The green component of the color.</param>
			/// <param name="blue">The blue component of the color.</param>
			Color( Single red, Single green, Single blue )
			{
				Alpha = 1.0f;
				Red = red;
				Green = green;
				Blue = blue;
			}
			/// <summary>
			/// Initializes a new instance of the <see cref="Color"/> structure.
			/// </summary>
			/// <param name="argb">A packed integer containing all four color components.</param>
			Color( UInt32 argb )
			{
				Alpha = ( ( argb >> 24 ) & 255 ) / 255.0f;
				Red = ( ( argb >> 16 ) & 255 ) / 255.0f;
				Green = ( ( argb >> 8 ) & 255 ) / 255.0f;
				Blue = ( argb & 255 ) / 255.0f;
			}
			/// <summary>
			/// Converts the color into a packed integer.
			/// </summary>
			/// <returns>A packed integer containing all four color components.</returns>
			Int32 ToArgb()
			{
				UInt32 a, r, g, b;

				a = static_cast<UInt32>( Alpha * 255.0f );
				r = static_cast<UInt32>( Red * 255.0f );
				g = static_cast<UInt32>( Green * 255.0f );
				b = static_cast<UInt32>( Blue * 255.0f );

				UInt32 value = b;
				value += g << 8;
				value += r << 16;
				value += a << 24;

				return static_cast<Int32>( value );
			}

			static Color FromAColor( Single alpha, Color color )
			{
				return Color( alpha, color );
			}

			/// <summary>
			/// Adds two colors.
			/// </summary>
			/// <param name="color1">The first color to add.</param>
			/// <param name="color2">The second color to add.</param>
			/// <returns>The sum of the two colors.</returns>
			static Color Add( Color color1, Color color2 )
			{
				return Color( color1.Alpha + color2.Alpha, color1.Red + color2.Red, color1.Green + color2.Green, color1.Blue + color2.Blue );
			}
			/// <summary>
			/// Subtracts two colors.
			/// </summary>
			/// <param name="color1">The first color to subtract.</param>
			/// <param name="color2">The second color to subtract.</param>
			/// <returns>The difference between the two colors.</returns>
			static Color Subtract( Color color1, Color color2 )
			{
				return Color( color1.Alpha - color2.Alpha, color1.Red - color2.Red, color1.Green - color2.Green, color1.Blue - color2.Blue );
			}
			/// <summary>
			/// Modulates two colors.
			/// </summary>
			/// <param name="color1">The first color to modulate.</param>
			/// <param name="color2">The second color to modulate.</param>
			/// <returns>The modulation of the two colors.</returns>
			static Color Modulate( Color color1, Color color2 )
			{
				return Color( color1.Alpha * color2.Alpha, color1.Red * color2.Red, color1.Green * color2.Green, color1.Blue * color2.Blue );
			}
			/// <summary>
			/// Performs a linear interpolation between two colors.
			/// </summary>
			/// <param name="color1">Start color.</param>
			/// <param name="color2">End color.</param>
			/// <param name="amount">Value between 0 and 1 indicating the weight of <paramref name="color2"/>.</param>
			/// <returns>The linear interpolation of the two colors.</returns>
			/// <remarks>
			/// This method performs the linear interpolation based on the following formula.
			/// <code>color1 + (color2 - color1) * amount</code>
			/// Passing <paramref name="amount"/> a value of 0 will cause <paramref name="color1"/> to be returned; a value of 1 will cause <paramref name="color2"/> to be returned. 
			/// </remarks>
			static Color Lerp( Color color1, Color color2, Single amount )
			{
				Single a = color1.Alpha + amount * ( color2.Alpha - color1.Alpha );
				Single r = color1.Red + amount * ( color2.Red - color1.Red );
				Single g = color1.Green + amount * ( color2.Green - color1.Green );
				Single b = color1.Blue + amount * ( color2.Blue - color1.Blue );

				return Color( a, r, g, b );
			}
			/// <summary>
			/// Negates a color.
			/// </summary>
			/// <param name="color">The color to negate.</param>
			/// <returns>The negated color.</returns>
			static Color Negate( Color color )
			{
				return Color( 1.0f - color.Alpha, 1.0f - color.Red, 1.0f - color.Green, 1.0f - color.Blue );
			}
			/// <summary>
			/// Scales a color by the specified amount.
			/// </summary>
			/// <param name="color">The color to scale.</param>
			/// <param name="scale">The amount by which to scale.</param>
			/// <returns>The scaled color.</returns>
			static Color Scale( Color color, Single scale )
			{
				return Color( color.Alpha, color.Red * scale, color.Green * scale, color.Blue * scale );
			}
			/// <summary>
			/// Adjusts the contrast of a color.
			/// </summary>
			/// <param name="color">The color whose contrast is to be adjusted.</param>
			/// <param name="contrast">The amount by which to adjust the contrast.</param>
			/// <returns>The adjusted color.</returns>
			static Color AdjustContrast( Color color, Single contrast )
			{
				Single r = 0.5f + contrast * ( color.Red - 0.5f );
				Single g = 0.5f + contrast * ( color.Green - 0.5f );
				Single b = 0.5f + contrast * ( color.Blue - 0.5f );

				return Color( color.Alpha, r, g, b );
			}
			/// <summary>
			/// Adjusts the saturation of a color.
			/// </summary>
			/// <param name="color">The color whose saturation is to be adjusted.</param>
			/// <param name="saturation">The amount by which to adjust the saturation.</param>
			/// <returns>The adjusted color.</returns>
			static Color AdjustSaturation( Color color, Single saturation )
			{
				Single grey = color.Red * 0.2125f + color.Green * 0.7154f + color.Blue * 0.0721f;
				Single r = grey + saturation * ( color.Red - grey );
				Single g = grey + saturation * ( color.Green - grey );
				Single b = grey + saturation * ( color.Blue - grey );

				return Color( color.Alpha, r, g, b );
			}

			/// <summary>
			/// Returns the hash code for this instance.
			/// </summary>
			/// <returns>A 32-bit signed integer hash code.</returns>
			virtual Int32 GetHashCode() override
			{
				return ( Alpha.GetHashCode() + Red.GetHashCode() + Green.GetHashCode() + Blue.GetHashCode() );
			}

			/// <summary> Tests for equality between two objects. </summary>
			static Boolean operator == ( Color left, Color right ) { return Color::Equals( left, right ); }
			/// <summary> Tests for inequality between two objects. </summary>
			static Boolean operator != ( Color left, Color right ) { return !Color::Equals( left, right ); }

			/// <summary>
			/// Returns a value that indicates whether the current instance is equal to a specified object. 
			/// </summary>
			/// <param name="obj">Object to make the comparison with.</param>
			/// <returns><c>true</c> if the current instance is equal to the specified object; <c>false</c> otherwise.</returns>
			virtual Boolean Equals( System::Object^ obj ) override
			{
				if ( obj == nullptr )
					return false;

				if ( obj->GetType() != GetType() )
					return false;

				return Equals( safe_cast<Color>( obj ) );
			}

			/// <summary>
			/// Returns a value that indicates whether the current instance is equal to the specified object. 
			/// </summary>
			/// <param name="other">Object to make the comparison with.</param>
			/// <returns><c>true</c> if the current instance is equal to the specified object; <c>false</c> otherwise.</returns>
			virtual Boolean Equals( Color other )
			{
				return ( Alpha == other.Alpha && Red == other.Red && Green == other.Green && Blue == other.Blue );
			}

			/// <summary>
			/// Determines whether the specified object instances are considered equal. 
			/// </summary>
			/// <param name="value1"></param>
			/// <param name="value2"></param>
			/// <returns><c>true</c> if <paramref name="value1"/> is the same instance as <paramref name="value2"/> or 
			/// if both are <c>null</c> references or if <c>value1.Equals(value2)</c> returns <c>true</c>; otherwise, <c>false</c>.</returns>
			static Boolean Equals( Color% value1, Color% value2 )
			{
				return ( value1.Alpha == value2.Alpha && value1.Red == value2.Red && value1.Green == value2.Green && value1.Blue == value2.Blue );
			}

			/// <summary>
			/// The undefined color.
			/// </summary>
			static initonly Color Undefined = Color( 0x00000000 );

			/// <summary>
			/// The alice blue.
			/// </summary>
			static initonly Color AliceBlue = Color( 0xFFF0F8FF );

			/// <summary>
			/// The antique white.
			/// </summary>
			static initonly Color AntiqueWhite = Color( 0xFFFAEBD7 );

			/// <summary>
			/// The aqua.
			/// </summary>
			static initonly Color Aqua = Color( 0xFF00FFFF );

			/// <summary>
			/// The aquamarine.
			/// </summary>
			static initonly Color Aquamarine = Color( 0xFF7FFFD4 );

			/// <summary>
			/// The azure.
			/// </summary>
			static initonly Color Azure = Color( 0xFFF0FFFF );

			/// <summary>
			/// The beige.
			/// </summary>
			static initonly Color Beige = Color( 0xFFF5F5DC );

			/// <summary>
			/// The bisque.
			/// </summary>
			static initonly Color Bisque = Color( 0xFFFFE4C4 );

			/// <summary>
			/// The black.
			/// </summary>
			static initonly Color Black = Color( 0xFF000000 );

			/// <summary>
			/// The blanched almond.
			/// </summary>
			static initonly Color BlanchedAlmond = Color( 0xFFFFEBCD );

			/*/// <summary>
			/// The blue.
			/// </summary>
			static initonly Color Blue = Color( 0xFF0000FF );*/

			/// <summary>
			/// The blue violet.
			/// </summary>
			static initonly Color BlueViolet = Color( 0xFF8A2BE2 );

			/// <summary>
			/// The brown.
			/// </summary>
			static initonly Color Brown = Color( 0xFFA52A2A );

			/// <summary>
			/// The burly wood.
			/// </summary>
			static initonly Color BurlyWood = Color( 0xFFDEB887 );

			/// <summary>
			/// The cadet blue.
			/// </summary>
			static initonly Color CadetBlue = Color( 0xFF5F9EA0 );

			/// <summary>
			/// The chartreuse.
			/// </summary>
			static initonly Color Chartreuse = Color( 0xFF7FFF00 );

			/// <summary>
			/// The chocolate.
			/// </summary>
			static initonly Color Chocolate = Color( 0xFFD2691E );

			/// <summary>
			/// The coral.
			/// </summary>
			static initonly Color Coral = Color( 0xFFFF7F50 );

			/// <summary>
			/// The cornflower blue.
			/// </summary>
			static initonly Color CornflowerBlue = Color( 0xFF6495ED );

			/// <summary>
			/// The cornsilk.
			/// </summary>
			static initonly Color Cornsilk = Color( 0xFFFFF8DC );

			/// <summary>
			/// The crimson.
			/// </summary>
			static initonly Color Crimson = Color( 0xFFDC143C );

			/// <summary>
			/// The cyan.
			/// </summary>
			static initonly Color Cyan = Color( 0xFF00FFFF );

			/// <summary>
			/// The dark blue.
			/// </summary>
			static initonly Color DarkBlue = Color( 0xFF00008B );

			/// <summary>
			/// The dark cyan.
			/// </summary>
			static initonly Color DarkCyan = Color( 0xFF008B8B );

			/// <summary>
			/// The dark goldenrod.
			/// </summary>
			static initonly Color DarkGoldenrod = Color( 0xFFB8860B );

			/// <summary>
			/// The dark gray.
			/// </summary>
			static initonly Color DarkGray = Color( 0xFFA9A9A9 );

			/// <summary>
			/// The dark green.
			/// </summary>
			static initonly Color DarkGreen = Color( 0xFF006400 );

			/// <summary>
			/// The dark khaki.
			/// </summary>
			static initonly Color DarkKhaki = Color( 0xFFBDB76B );

			/// <summary>
			/// The dark magenta.
			/// </summary>
			static initonly Color DarkMagenta = Color( 0xFF8B008B );

			/// <summary>
			/// The dark olive green.
			/// </summary>
			static initonly Color DarkOliveGreen = Color( 0xFF556B2F );

			/// <summary>
			/// The dark orange.
			/// </summary>
			static initonly Color DarkOrange = Color( 0xFFFF8C00 );

			/// <summary>
			/// The dark orchid.
			/// </summary>
			static initonly Color DarkOrchid = Color( 0xFF9932CC );

			/// <summary>
			/// The dark red.
			/// </summary>
			static initonly Color DarkRed = Color( 0xFF8B0000 );

			/// <summary>
			/// The dark salmon.
			/// </summary>
			static initonly Color DarkSalmon = Color( 0xFFE9967A );

			/// <summary>
			/// The dark sea green.
			/// </summary>
			static initonly Color DarkSeaGreen = Color( 0xFF8FBC8F );

			/// <summary>
			/// The dark slate blue.
			/// </summary>
			static initonly Color DarkSlateBlue = Color( 0xFF483D8B );

			/// <summary>
			/// The dark slate gray.
			/// </summary>
			static initonly Color DarkSlateGray = Color( 0xFF2F4F4F );

			/// <summary>
			/// The dark turquoise.
			/// </summary>
			static initonly Color DarkTurquoise = Color( 0xFF00CED1 );

			/// <summary>
			/// The dark violet.
			/// </summary>
			static initonly Color DarkViolet = Color( 0xFF9400D3 );

			/// <summary>
			/// The deep pink.
			/// </summary>
			static initonly Color DeepPink = Color( 0xFFFF1493 );

			/// <summary>
			/// The deep sky blue.
			/// </summary>
			static initonly Color DeepSkyBlue = Color( 0xFF00BFFF );

			/// <summary>
			/// The dim gray.
			/// </summary>
			static initonly Color DimGray = Color( 0xFF696969 );

			/// <summary>
			/// The dodger blue.
			/// </summary>
			static initonly Color DodgerBlue = Color( 0xFF1E90FF );

			/// <summary>
			/// The firebrick.
			/// </summary>
			static initonly Color Firebrick = Color( 0xFFB22222 );

			/// <summary>
			/// The floral white.
			/// </summary>
			static initonly Color FloralWhite = Color( 0xFFFFFAF0 );

			/// <summary>
			/// The forest green.
			/// </summary>
			static initonly Color ForestGreen = Color( 0xFF228B22 );

			/// <summary>
			/// The fuchsia.
			/// </summary>
			static initonly Color Fuchsia = Color( 0xFFFF00FF );

			/// <summary>
			/// The gainsboro.
			/// </summary>
			static initonly Color Gainsboro = Color( 0xFFDCDCDC );

			/// <summary>
			/// The ghost white.
			/// </summary>
			static initonly Color GhostWhite = Color( 0xFFF8F8FF );

			/// <summary>
			/// The gold.
			/// </summary>
			static initonly Color Gold = Color( 0xFFFFD700 );

			/// <summary>
			/// The goldenrod.
			/// </summary>
			static initonly Color Goldenrod = Color( 0xFFDAA520 );

			/// <summary>
			/// The gray.
			/// </summary>
			static initonly Color Gray = Color( 0xFF808080 );

			/*/// <summary>
			/// The green.
			/// </summary>
			static initonly Color Green = Color( 0xFF008000 );*/

			/// <summary>
			/// The green yellow.
			/// </summary>
			static initonly Color GreenYellow = Color( 0xFFADFF2F );

			/// <summary>
			/// The honeydew.
			/// </summary>
			static initonly Color Honeydew = Color( 0xFFF0FFF0 );

			/// <summary>
			/// The hot pink.
			/// </summary>
			static initonly Color HotPink = Color( 0xFFFF69B4 );

			/// <summary>
			/// The indian red.
			/// </summary>
			static initonly Color IndianRed = Color( 0xFFCD5C5C );

			/// <summary>
			/// The indigo.
			/// </summary>
			static initonly Color Indigo = Color( 0xFF4B0082 );

			/// <summary>
			/// The ivory.
			/// </summary>
			static initonly Color Ivory = Color( 0xFFFFFFF0 );

			/// <summary>
			/// The khaki.
			/// </summary>
			static initonly Color Khaki = Color( 0xFFF0E68C );

			/// <summary>
			/// The lavender.
			/// </summary>
			static initonly Color Lavender = Color( 0xFFE6E6FA );

			/// <summary>
			/// The lavender blush.
			/// </summary>
			static initonly Color LavenderBlush = Color( 0xFFFFF0F5 );

			/// <summary>
			/// The lawn green.
			/// </summary>
			static initonly Color LawnGreen = Color( 0xFF7CFC00 );

			/// <summary>
			/// The lemon chiffon.
			/// </summary>
			static initonly Color LemonChiffon = Color( 0xFFFFFACD );

			/// <summary>
			/// The light blue.
			/// </summary>
			static initonly Color LightBlue = Color( 0xFFADD8E6 );

			/// <summary>
			/// The light coral.
			/// </summary>
			static initonly Color LightCoral = Color( 0xFFF08080 );

			/// <summary>
			/// The light cyan.
			/// </summary>
			static initonly Color LightCyan = Color( 0xFFE0FFFF );

			/// <summary>
			/// The light goldenrod yellow.
			/// </summary>
			static initonly Color LightGoldenrodYellow = Color( 0xFFFAFAD2 );

			/// <summary>
			/// The light gray.
			/// </summary>
			static initonly Color LightGray = Color( 0xFFD3D3D3 );

			/// <summary>
			/// The light green.
			/// </summary>
			static initonly Color LightGreen = Color( 0xFF90EE90 );

			/// <summary>
			/// The light pink.
			/// </summary>
			static initonly Color LightPink = Color( 0xFFFFB6C1 );

			/// <summary>
			/// The light salmon.
			/// </summary>
			static initonly Color LightSalmon = Color( 0xFFFFA07A );

			/// <summary>
			/// The light sea green.
			/// </summary>
			static initonly Color LightSeaGreen = Color( 0xFF20B2AA );

			/// <summary>
			/// The light sky blue.
			/// </summary>
			static initonly Color LightSkyBlue = Color( 0xFF87CEFA );

			/// <summary>
			/// The light slate gray.
			/// </summary>
			static initonly Color LightSlateGray = Color( 0xFF778899 );

			/// <summary>
			/// The light steel blue.
			/// </summary>
			static initonly Color LightSteelBlue = Color( 0xFFB0C4DE );

			/// <summary>
			/// The light yellow.
			/// </summary>
			static initonly Color LightYellow = Color( 0xFFFFFFE0 );

			/// <summary>
			/// The lime.
			/// </summary>
			static initonly Color Lime = Color( 0xFF00FF00 );

			/// <summary>
			/// The lime green.
			/// </summary>
			static initonly Color LimeGreen = Color( 0xFF32CD32 );

			/// <summary>
			/// The linen.
			/// </summary>
			static initonly Color Linen = Color( 0xFFFAF0E6 );

			/// <summary>
			/// The magenta.
			/// </summary>
			static initonly Color Magenta = Color( 0xFFFF00FF );

			/// <summary>
			/// The maroon.
			/// </summary>
			static initonly Color Maroon = Color( 0xFF800000 );

			/// <summary>
			/// The medium aquamarine.
			/// </summary>
			static initonly Color MediumAquamarine = Color( 0xFF66CDAA );

			/// <summary>
			/// The medium blue.
			/// </summary>
			static initonly Color MediumBlue = Color( 0xFF0000CD );

			/// <summary>
			/// The medium orchid.
			/// </summary>
			static initonly Color MediumOrchid = Color( 0xFFBA55D3 );

			/// <summary>
			/// The medium purple.
			/// </summary>
			static initonly Color MediumPurple = Color( 0xFF9370DB );

			/// <summary>
			/// The medium sea green.
			/// </summary>
			static initonly Color MediumSeaGreen = Color( 0xFF3CB371 );

			/// <summary>
			/// The medium slate blue.
			/// </summary>
			static initonly Color MediumSlateBlue = Color( 0xFF7B68EE );

			/// <summary>
			/// The medium spring green.
			/// </summary>
			static initonly Color MediumSpringGreen = Color( 0xFF00FA9A );

			/// <summary>
			/// The medium turquoise.
			/// </summary>
			static initonly Color MediumTurquoise = Color( 0xFF48D1CC );

			/// <summary>
			/// The medium violet red.
			/// </summary>
			static initonly Color MediumVioletRed = Color( 0xFFC71585 );

			/// <summary>
			/// The midnight blue.
			/// </summary>
			static initonly Color MidnightBlue = Color( 0xFF191970 );

			/// <summary>
			/// The mint cream.
			/// </summary>
			static initonly Color MintCream = Color( 0xFFF5FFFA );

			/// <summary>
			/// The misty rose.
			/// </summary>
			static initonly Color MistyRose = Color( 0xFFFFE4E1 );

			/// <summary>
			/// The moccasin.
			/// </summary>
			static initonly Color Moccasin = Color( 0xFFFFE4B5 );

			/// <summary>
			/// The navajo white.
			/// </summary>
			static initonly Color NavajoWhite = Color( 0xFFFFDEAD );

			/// <summary>
			/// The navy.
			/// </summary>
			static initonly Color Navy = Color( 0xFF000080 );

			/// <summary>
			/// The old lace.
			/// </summary>
			static initonly Color OldLace = Color( 0xFFFDF5E6 );

			/// <summary>
			/// The olive.
			/// </summary>
			static initonly Color Olive = Color( 0xFF808000 );

			/// <summary>
			/// The olive drab.
			/// </summary>
			static initonly Color OliveDrab = Color( 0xFF6B8E23 );

			/// <summary>
			/// The orange.
			/// </summary>
			static initonly Color Orange = Color( 0xFFFFA500 );

			/// <summary>
			/// The orange red.
			/// </summary>
			static initonly Color OrangeRed = Color( 0xFFFF4500 );

			/// <summary>
			/// The orchid.
			/// </summary>
			static initonly Color Orchid = Color( 0xFFDA70D6 );

			/// <summary>
			/// The pale goldenrod.
			/// </summary>
			static initonly Color PaleGoldenrod = Color( 0xFFEEE8AA );

			/// <summary>
			/// The pale green.
			/// </summary>
			static initonly Color PaleGreen = Color( 0xFF98FB98 );

			/// <summary>
			/// The pale turquoise.
			/// </summary>
			static initonly Color PaleTurquoise = Color( 0xFFAFEEEE );

			/// <summary>
			/// The pale violet red.
			/// </summary>
			static initonly Color PaleVioletRed = Color( 0xFFDB7093 );

			/// <summary>
			/// The papaya whip.
			/// </summary>
			static initonly Color PapayaWhip = Color( 0xFFFFEFD5 );

			/// <summary>
			/// The peach puff.
			/// </summary>
			static initonly Color PeachPuff = Color( 0xFFFFDAB9 );

			/// <summary>
			/// The peru.
			/// </summary>
			static initonly Color Peru = Color( 0xFFCD853F );

			/// <summary>
			/// The pink.
			/// </summary>
			static initonly Color Pink = Color( 0xFFFFC0CB );

			/// <summary>
			/// The plum.
			/// </summary>
			static initonly Color Plum = Color( 0xFFDDA0DD );

			/// <summary>
			/// The powder blue.
			/// </summary>
			static initonly Color PowderBlue = Color( 0xFFB0E0E6 );

			/// <summary>
			/// The purple.
			/// </summary>
			static initonly Color Purple = Color( 0xFF800080 );

			/*/// <summary>
			/// The red.
			/// </summary>
			static initonly Color Red = Color( 0xFFFF0000 );*/

			/// <summary>
			/// The rosy brown.
			/// </summary>
			static initonly Color RosyBrown = Color( 0xFFBC8F8F );

			/// <summary>
			/// The royal blue.
			/// </summary>
			static initonly Color RoyalBlue = Color( 0xFF4169E1 );

			/// <summary>
			/// The saddle brown.
			/// </summary>
			static initonly Color SaddleBrown = Color( 0xFF8B4513 );

			/// <summary>
			/// The salmon.
			/// </summary>
			static initonly Color Salmon = Color( 0xFFFA8072 );

			/// <summary>
			/// The sandy brown.
			/// </summary>
			static initonly Color SandyBrown = Color( 0xFFF4A460 );

			/// <summary>
			/// The sea green.
			/// </summary>
			static initonly Color SeaGreen = Color( 0xFF2E8B57 );

			/// <summary>
			/// The sea shell.
			/// </summary>
			static initonly Color SeaShell = Color( 0xFFFFF5EE );

			/// <summary>
			/// The sienna.
			/// </summary>
			static initonly Color Sienna = Color( 0xFFA0522D );

			/// <summary>
			/// The silver.
			/// </summary>
			static initonly Color Silver = Color( 0xFFC0C0C0 );

			/// <summary>
			/// The sky blue.
			/// </summary>
			static initonly Color SkyBlue = Color( 0xFF87CEEB );

			/// <summary>
			/// The slate blue.
			/// </summary>
			static initonly Color SlateBlue = Color( 0xFF6A5ACD );

			/// <summary>
			/// The slate gray.
			/// </summary>
			static initonly Color SlateGray = Color( 0xFF708090 );

			/// <summary>
			/// The snow.
			/// </summary>
			static initonly Color Snow = Color( 0xFFFFFAFA );

			/// <summary>
			/// The spring green.
			/// </summary>
			static initonly Color SpringGreen = Color( 0xFF00FF7F );

			/// <summary>
			/// The steel blue.
			/// </summary>
			static initonly Color SteelBlue = Color( 0xFF4682B4 );

			/// <summary>
			/// The tan.
			/// </summary>
			static initonly Color Tan = Color( 0xFFD2B48C );

			/// <summary>
			/// The teal.
			/// </summary>
			static initonly Color Teal = Color( 0xFF008080 );

			/// <summary>
			/// The thistle.
			/// </summary>
			static initonly Color Thistle = Color( 0xFFD8BFD8 );

			/// <summary>
			/// The tomato.
			/// </summary>
			static initonly Color Tomato = Color( 0xFFFF6347 );

			/// <summary>
			/// The transparent.
			/// </summary>
			static initonly Color Transparent = Color( 0x00FFFFFF );

			/// <summary>
			/// The turquoise.
			/// </summary>
			static initonly Color Turquoise = Color( 0xFF40E0D0 );

			/// <summary>
			/// The violet.
			/// </summary>
			static initonly Color Violet = Color( 0xFFEE82EE );

			/// <summary>
			/// The wheat.
			/// </summary>
			static initonly Color Wheat = Color( 0xFFF5DEB3 );

			/// <summary>
			/// The white.
			/// </summary>
			static initonly Color White = Color( 0xFFFFFFFF );

			/// <summary>
			/// The white smoke.
			/// </summary>
			static initonly Color WhiteSmoke = Color( 0xFFF5F5F5 );

			/// <summary>
			/// The yellow.
			/// </summary>
			static initonly Color Yellow = Color( 0xFFFFFF00 );

			/// <summary>
			/// The yellow green.
			/// </summary>
			static initonly Color YellowGreen = Color( 0xFF9ACD32 );
		};
	}
}

