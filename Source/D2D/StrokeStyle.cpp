#include "StrokeStyle.hpp"

#include "Factory.hpp"
#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		StrokeStyle::StrokeStyle()
		{
			ID2D1StrokeStyle *strokeStyle = nullptr;
			ThrowIfFailed<GraphicsException>(
				GlobalAssets::Factory2D->_d2dFactory->CreateStrokeStyle( D2D1::StrokeStyleProperties(), nullptr, 0, &strokeStyle )
				);

			Assing( strokeStyle );
		}

		StrokeStyle::StrokeStyle(  StrokeStyleProperties properties )
		{
			ID2D1StrokeStyle *strokeStyle = nullptr;

			ThrowIfFailed<GraphicsException>(
				GlobalAssets::Factory2D->_d2dFactory->CreateStrokeStyle( reinterpret_cast<D2D1_STROKE_STYLE_PROPERTIES*>( &properties ), nullptr, 0, &strokeStyle )
				);

			Assing( strokeStyle );
		}

		StrokeStyle::StrokeStyle(  StrokeStyleProperties properties, array<float>^ dashes )
		{
			ID2D1StrokeStyle *strokeStyle = nullptr;
			pin_ptr<float> pinnedDashes = &dashes[ 0 ];

			ThrowIfFailed<GraphicsException>(
				GlobalAssets::Factory2D->_d2dFactory->CreateStrokeStyle( reinterpret_cast<D2D1_STROKE_STYLE_PROPERTIES*>( &properties ), pinnedDashes, dashes->Length, &strokeStyle )
				);

			Assing( strokeStyle );
		}

		array<float>^ StrokeStyle::GetDashes()
		{
			array<float>^ results = gcnew array<float>( Native->GetDashesCount() );
			pin_ptr<float> pinnedResults = &results[ 0 ];

			Native->GetDashes( pinnedResults, Native->GetDashesCount() );

			return results;
		}

		CapStyle StrokeStyle::StartCap::get()
		{
			return static_cast<CapStyle>( Native->GetStartCap() );
		}

		CapStyle StrokeStyle::EndCap::get()
		{
			return static_cast<CapStyle>( Native->GetEndCap() );
		}

		CapStyle StrokeStyle::DashCap::get()
		{
			return static_cast<CapStyle>( Native->GetDashCap() );
		}

		Kodo::Graphics::LineJoin StrokeStyle::LineJoin::get()
		{
			return static_cast<Kodo::Graphics::LineJoin>( Native->GetLineJoin() );
		}

		float StrokeStyle::MiterLimit::get()
		{
			return Native->GetMiterLimit();
		}

		Kodo::Graphics::DashStyle StrokeStyle::DashStyle::get()
		{
			return static_cast<Kodo::Graphics::DashStyle>( Native->GetDashStyle() );
		}

		float StrokeStyle::DashOffset::get()
		{
			return Native->GetDashOffset();
		}
	}
}