#include "SolidColorBrush.hpp"

#include "Context.hpp"

namespace Kodo
{
	namespace Graphics
	{
		SolidColorBrush::SolidColorBrush( Context^ context, Kodo::Graphics::Color color )
		{
			ID2D1SolidColorBrush *brush = nullptr;

			ThrowIfFailed<GraphicsException>(
				context->_d2dContext->CreateSolidColorBrush( reinterpret_cast<D2D1_COLOR_F*>( &color ), nullptr, &brush )
				);

			Assing( brush );
		}

		SolidColorBrush::SolidColorBrush( Context^ context, Kodo::Graphics::Color color, BrushProperties properties )
		{
			ID2D1SolidColorBrush *brush = nullptr;

			ThrowIfFailed<GraphicsException>(
				context->_d2dContext->CreateSolidColorBrush( reinterpret_cast<D2D1_COLOR_F*>( &color ),
				reinterpret_cast<D2D1_BRUSH_PROPERTIES*>( &properties ), &brush )
				);

			Assing( brush );
		}
	}
}