#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		ref class Context;

		namespace Imaging
		{
			ref class WicBitmapSource;
		}

		/// <summary> Extended bitmap properties. </summary>
		public enum class BitmapOptions
		{
			/// <summary> The bitmap is created with default properties. </summary>
			None = 0x00000000,
			/// <summary> The bitmap can be specified as a target in ID2D1DeviceContext::SetTarget </summary>
			Target = 0x00000001,
			/// <summary> The bitmap cannot be used as an input to DrawBitmap, DrawImage, in a bitmap brush or as an input to an effect. </summary>
			CannotDraw = 0x00000002,
			/// <summary> The bitmap can be read from the CPU. </summary>
			CPURead = 0x00000004,
			/// <summary> The bitmap works with the ID2D1GdiInteropRenderTarget::GetDC API. </summary>
			GDICompatible = 0x00000008
		};

		public value struct BitmapProperties
		{
			property Kodo::Graphics::PixelFormat PixelFormat;
			property Single DPIX;
			property Single DPIY;
			property Kodo::Graphics::BitmapOptions BitmapOptions;

			static BitmapProperties Target( Context^ context );
		};

		public ref class Bitmap : Image
		{
			GRAPHICS_OBJECT( Bitmap, ID2D1Bitmap1 );

		public:

			Bitmap( Context^ context, Size size );
			Bitmap( Context^ context, Size size, BitmapProperties properties );
			Bitmap( Context^ context, Imaging::WicBitmapSource^ source );
			Bitmap( Context^ context, Imaging::WicBitmapSource^ source, BitmapProperties properties );
		};
	}
}
