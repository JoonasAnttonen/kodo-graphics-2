#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		/// <summary> Represents a geometry resource and defines a set of helper methods for manipulating and measuring geometric shapes. Classes that inherit from Geometry define specific shapes. </summary>
		public ref class Geometry : GraphicsObject
		{
			GRAPHICS_OBJECT( Geometry, ID2D1Geometry );

		protected:

			Geometry() { };

		public:
		};
	}
}