#include "PathGeometry.hpp"

#include "GeometrySink.hpp"

#include "Factory.hpp"
#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		PathGeometry::PathGeometry()
		{
			ID2D1PathGeometry *geometry = nullptr;
			ThrowIfFailed<GraphicsException>( GlobalAssets::Factory2D->_d2dFactory->CreatePathGeometry( &geometry ) );
			Assing( geometry );
		}

		GeometrySink^ PathGeometry::Open()
		{
			ID2D1GeometrySink *sink = nullptr;
			ThrowIfFailed<GraphicsException>( Native->Open( &sink ) );
			return gcnew GeometrySink( sink );
		}

		void PathGeometry::Stream( GeometrySink^ geometrySink )
		{
			ThrowIfFailed<GraphicsException>( Native->Stream( geometrySink->Native ) );
		}
	}
}