#pragma once

#include "..\Graphics.hpp"
#include "..\Matrix3x2.hpp"

namespace Kodo
{
	namespace Graphics
	{
		value class Matrix3x2;

		/// <summary> Describes the opacity and transformation of a brush. </summary>
		public value struct BrushProperties
		{
		public:

			/// <summary> A value between 0.0f and 1.0f, inclusive, that specifies the degree of opacity of the brush. </summary>
			property Single Opacity;
			/// <summary> The transformation that is applied to the brush. </summary>
			property Matrix3x2 Transform;
		};

		/// <summary> Defines an object that paints an area. </summary>
		public ref class Brush abstract : GraphicsObject
		{
			GRAPHICS_OBJECT( Brush, ID2D1Brush );

		protected:

			Brush() { }

		public:

			/// <summary> Get or set the degree of opacity of this brush. </summary>
			property Single Opacity
			{
				Single get() { return Native->GetOpacity(); }
				void set( Single value ) { Native->SetOpacity( value ); }
			}

			/// <summary> Get or set the transform applied to this brush. </summary>
			property Matrix3x2 Transform
			{
				Matrix3x2 get() {
					Matrix3x2 result;
					Native->GetTransform( reinterpret_cast<D2D1_MATRIX_3X2_F*>( &result ) );
					return result;
				}
				void set( Matrix3x2 value ) {
					Native->SetTransform( reinterpret_cast<D2D1_MATRIX_3X2_F*>( &value ) );
				}
			}
		};
	}
}

