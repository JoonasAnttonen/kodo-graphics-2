#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		/// <summary> Describes the stroke that outlines a shape. </summary>
		public value struct StrokeStyleProperties
		{
		public:
			/// <summary> The cap applied to the start of all the open figures in a stroked geometry. </summary>
			property CapStyle StartCap;
			/// <summary> The cap applied to the end of all the open figures in a stroked geometry. </summary>
			property CapStyle EndCap;
			/// <summary> The shape at either end of each dash segment. </summary>
			property CapStyle DashCap;
			/// <summary> A value that describes how segments are joined. This value is ignored for a vertex if the segment flags specify that the segment should have a smooth join. </summary>
			property LineJoin LineJoin;
			/// <summary> The limit of the thickness of the join on a mitered corner. This value is always treated as though it is greater than or equal to 1.0f. </summary>
			property float MiterLimit;
			/// <summary> A value that specifies whether the stroke has a dash pattern and, if so, the dash style. </summary>
			property DashStyle DashStyle;
			/// <summary> A value that specifies an offset in the dash sequence. A positive dash offset value shifts the dash pattern, in units of stroke width, toward the start of the stroked geometry. A negative dash offset value shifts the dash pattern, in units of stroke width, toward the end of the stroked geometry. </summary>
			property float DashOffset;

			/// <summary> Creates a new instance of StrokeStyleProperties. </summary>
			/// <param name="startCap">The cap applied to the start of all the open figures in a stroked geometry.</param>
			/// <param name="endCap">The cap applied to the end of all the open figures in a stroked geometry.</param>
			/// <param name="dashCap">The shape at either end of each dash segment.</param>
			/// <param name="lineJoin">A value that describes how segments are joined. This value is ignored for a vertex if the segment flags specify that the segment should have a smooth join.</param>
			/// <param name="miterLimit">The limit of the thickness of the join on a mitered corner. This value is always treated as though it is greater than or equal to 1.0f.</param>
			/// <param name="dashStyle">A value that specifies whether the stroke has a dash pattern and, if so, the dash style.</param>
			/// <param name="dashOffset">A value that specifies an offset in the dash sequence. A positive dash offset value shifts the dash pattern, in units of stroke width, toward the start of the stroked geometry. A negative dash offset value shifts the dash pattern, in units of stroke width, toward the end of the stroked geometry.</param>
			StrokeStyleProperties( CapStyle startCap, CapStyle endCap, CapStyle dashCap, Kodo::Graphics::LineJoin lineJoin, float miterLimit, Kodo::Graphics::DashStyle dashStyle, float dashOffset )
			{
				StartCap = startCap;
				EndCap = endCap;
				DashCap = dashCap;
				LineJoin = lineJoin;
				MiterLimit = miterLimit;
				DashStyle = dashStyle;
				DashOffset = dashOffset;
			}
		};

		/// <summary> Describes the caps, miter limit, line join, and dash information for a stroke. </summary>
		public ref class StrokeStyle : GraphicsObject
		{
			GRAPHICS_OBJECT( StrokeStyle, ID2D1StrokeStyle );

		public:

			/// <summary> Specifies the type of shape used at the beginning of a stroke. </summary>
			property CapStyle StartCap { CapStyle get(); }
			/// <summary> Specifies the shape used at the end of a stroke. </summary>
			property CapStyle EndCap { CapStyle get(); }
			/// <summary> Specifies how the ends of each dash are drawn. </summary>
			property CapStyle DashCap { CapStyle get(); }
			/// <summary> Specifies the type of joint used at the vertices of a shape's outline. </summary>
			property LineJoin LineJoin { Kodo::Graphics::LineJoin get(); }
			/// <summary> Specifies the limit on the ratio of the miter length to half the stroke's thickness. </summary>
			property float MiterLimit { float get(); }
			/// <summary> Specifies the value that describes the stroke's dash pattern. </summary>
			property DashStyle DashStyle { Kodo::Graphics::DashStyle get(); }
			/// <summary> Specifies the value that specifies how far in the dash sequence the stroke will start. </summary>
			property float DashOffset { float get(); }

			/// <summary> Create a new instance of StrokeStyle. </summary>
			StrokeStyle();
			/// <summary> Create a new instance of StrokeStyle. </summary>
			/// <param name="properties">A structure that describes the stroke's line cap, dash offset, and other details of a stroke.</param>
			StrokeStyle( StrokeStyleProperties properties );
			/// <summary> Create a new instance of StrokeStyle. </summary>
			/// <param name="properties">A structure that describes the stroke's line cap, dash offset, and other details of a stroke.</param>
			/// <param name="dashes">
			/// An array whose elements are set to the length of each dash and space in the dash pattern. 
			/// The first element sets the length of a dash, the second element sets the length of a space, the third element sets the length of a dash, and so on. 
			/// The length of each dash and space in the dash pattern is the product of the element value in the array and the stroke width.
			/// </param>
			StrokeStyle( StrokeStyleProperties properties, array<float>^ dashes );
			/// <summary> Returns the dash pattern.</summary>
			array<float>^ GetDashes();
		};
	}
}