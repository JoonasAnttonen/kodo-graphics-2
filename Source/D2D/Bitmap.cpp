#include "Bitmap.hpp"

#include "Context.hpp"

#include "..\WIC\WicBitmapSource.hpp"

namespace Kodo
{
	namespace Graphics
	{
		using namespace Imaging;

		BitmapProperties BitmapProperties::Target( Context^ context )
		{
			BitmapProperties result;
			result.BitmapOptions = Kodo::Graphics::BitmapOptions::Target;
			result.DPIX = 96;
			result.DPIY = 96;
			result.PixelFormat = context->PixelFormat;
			return result;
		}

		Bitmap::Bitmap( Context^ context, Size size, BitmapProperties properties )
		{
			ID2D1Bitmap1* b = nullptr;
			D2D_SIZE_U s;
			s.width = static_cast<UINT32>( size.Width );
			s.height = static_cast<UINT32>( size.Height );
			D2D1_BITMAP_PROPERTIES1 p;
			p.pixelFormat.format = static_cast<DXGI_FORMAT>( properties.PixelFormat.Format );
			p.pixelFormat.alphaMode = static_cast<D2D1_ALPHA_MODE>( properties.PixelFormat.AlphaMode );
			p.dpiX = properties.DPIX;
			p.dpiY = properties.DPIY;
			p.bitmapOptions = static_cast<D2D1_BITMAP_OPTIONS>( properties.BitmapOptions );
			p.colorContext = nullptr;

			ThrowIfFailed<GraphicsException>( context->_d2dContext->CreateBitmap( s, nullptr, 0, p, &b ) );

			Assing( b );
		}

		Bitmap::Bitmap( Context^ context, Size size )
		{
			ID2D1Bitmap1* b = nullptr;
			D2D_SIZE_U s;
			s.width = static_cast<UINT32>( size.Width );
			s.height = static_cast<UINT32>( size.Height );

			D2D1_BITMAP_PROPERTIES1 p = D2D1::BitmapProperties1();
			p.pixelFormat = context->_d2dContext->GetPixelFormat();

			ThrowIfFailed<GraphicsException>( context->_d2dContext->CreateBitmap( s, nullptr, 0, p, &b ) );

			Assing( b );
		}

		Bitmap::Bitmap( Context^ context, WicBitmapSource^ source )
		{
			ID2D1Bitmap1* b = nullptr;

			ThrowIfFailed<GraphicsException>( context->_d2dContext->CreateBitmapFromWicBitmap( source->Native, D2D1::BitmapProperties1(), &b ) );

			Assing( b );
		}

		Bitmap::Bitmap( Context^ context, WicBitmapSource^ source, BitmapProperties properties )
		{
			ID2D1Bitmap1* b = nullptr;
			D2D1_BITMAP_PROPERTIES1 p;
			p.pixelFormat.format = static_cast<DXGI_FORMAT>( properties.PixelFormat.Format );
			p.pixelFormat.alphaMode = static_cast<D2D1_ALPHA_MODE>( properties.PixelFormat.AlphaMode );
			p.dpiX = properties.DPIX;
			p.dpiY = properties.DPIY;
			p.bitmapOptions = static_cast<D2D1_BITMAP_OPTIONS>( properties.BitmapOptions );
			p.colorContext = nullptr;

			ThrowIfFailed<GraphicsException>( context->_d2dContext->CreateBitmapFromWicBitmap( source->Native, p, &b ) );

			Assing( b );
		}
	}
}
