#pragma once

#include "..\Graphics.hpp"
#include "..\Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		using namespace System::Runtime::InteropServices;

		public ref class Image abstract : GraphicsObject
		{
			GRAPHICS_OBJECT( Image, ID2D1Image );

		protected:

			Image() { }
		};

		public enum class BitmapInterpolationMode
		{
			/// <summary>
			/// Nearest Neighbor filtering.
			/// Also known as nearest pixel or nearest point sampling.
			/// </summary>
			NearestNeighbor = D2D1_INTERPOLATION_MODE_DEFINITION_NEAREST_NEIGHBOR,
			/// <summary>
			/// Linear filtering.
			/// </summary>
			Linear
		};

		/// <summary>
		/// Specifies whether text snapping is suppressed or clipping to the layout rectangle is enabled. This enumeration allows a bitwise combination of its member values.
		/// </summary>
		public enum class DrawTextOptions : Int32
		{
			/// <summary>
			/// Text is not vertically snapped to pixel boundaries. This setting is recommended for text that is being animated.
			/// </summary>
			None = D2D1_DRAW_TEXT_OPTIONS_NONE,
			/// <summary>
			/// Text is clipped to the layout rectangle.
			/// </summary>
			NoSnap = D2D1_DRAW_TEXT_OPTIONS_NO_SNAP,
			/// <summary>
			/// Text is vertically snapped to pixel boundaries and is not clipped to the layout rectangle.
			/// </summary>
			Clip = D2D1_DRAW_TEXT_OPTIONS_CLIP
		};

		/// <summary>
		/// Specifies how a brush paints areas outside of its normal content area.
		/// </summary>
		public enum class ExtendMode : Int32
		{
			/// <summary>
			/// Repeat the edge pixels of the brush's content for all regions outside the normal content area.
			/// </summary>
			Clamp = D2D1_EXTEND_MODE_CLAMP,
			/// <summary>
			/// Repeat the brush's content.
			/// </summary>
			Wrap = D2D1_EXTEND_MODE_WRAP,
			/// <summary>
			/// The same as ExtendMode.Wrap, except that alternate tiles of the brush's content are flipped. (The brush's normal content is drawn untransformed.)
			/// </summary>
			Mirror = D2D1_EXTEND_MODE_MIRROR
		};

		/// <summary>
		/// Specifies which gamma is used for interpolation.
		/// </summary>
		public enum class Gamma : Int32
		{
			/// <summary>
			/// Interpolation is performed in the standard RGB (sRGB) gamma.
			/// </summary>
			StandardRgb = D2D1_GAMMA_2_2,
			/// <summary>
			/// Interpolation is performed in the linear-gamma color space.
			/// </summary>
			Linear = D2D1_GAMMA_1_0
		};

		/// <summary>
		/// Specifies options that can be applied when a layer resource is applied to create a layer.
		/// </summary>
		public enum class LayerOptions : Int32
		{
			/// <summary>
			/// The text in this layer does not use ClearType antialiasing.
			/// </summary>
			None = D2D1_LAYER_OPTIONS_NONE,
			/// <summary>
			/// The layer renders correctly for ClearType text. 
			/// If the render target is set to ClearType, the layer continues to render ClearType. 
			/// If the render target is set to ClearType and this option is not specified, the render target will be set to render gray-scale until the layer is popped. 
			/// The caller can override this default by calling SetTextAntialiasMode while within the layer. 
			/// This flag is slightly slower than the default.
			/// </summary>
			InitializeForClearType = D2D1_LAYER_OPTIONS_INITIALIZE_FOR_CLEARTYPE
		};

		/// <summary>
		/// Describes whether an opacity mask contains graphics or text. 
		/// Direct2D uses this information to determine which gamma space to use when blending the opacity mask.
		/// </summary>
		public enum class OpacityMaskContent : Int32
		{
			/// <summary>
			/// The opacity mask contains graphics. The opacity mask is blended in the gamma 2.2 color space.
			/// </summary>
			Graphics = D2D1_OPACITY_MASK_CONTENT_GRAPHICS,
			/// <summary>
			/// The opacity mask contains non-GDI text. The gamma space used for blending is obtained from the render target's text rendering parameters. 
			/// </summary>
			NaturalText = D2D1_OPACITY_MASK_CONTENT_TEXT_NATURAL,
			/// <summary>
			/// The opacity mask contains text rendered using the GDI-compatible rendering mode. The opacity mask is blended using the gamma for GDI rendering.
			/// </summary>
			GdiCompatibleText = D2D1_OPACITY_MASK_CONTENT_TEXT_GDI_COMPATIBLE
		};

		/// <summary>
		/// Specifies the algorithm that is used when images are scaled or rotated.
		/// </summary>
		public enum class InterpolationMode : Int32
		{
			/// <summary>
			/// Use the exact color of the nearest bitmap pixel to the current rendering pixel.
			/// </summary>
			NearestNeighbor = D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			/// <summary>
			/// Interpolate a color from the four bitmap pixels that are the nearest to the rendering pixel.
			/// </summary>
			Linear = D2D1_BITMAP_INTERPOLATION_MODE_LINEAR
		};

		/// <summary>
		/// Specifies how the edges of nontext primitives are rendered.
		/// </summary>
		public enum class AntialiasMode : Int32
		{
			/// <summary>
			/// Edges are antialiased using the Direct2D per-primitive method of high-quality antialiasing.
			/// </summary>
			PerPrimitive = D2D1_ANTIALIAS_MODE_PER_PRIMITIVE,
			/// <summary>
			/// Objects are aliased in most cases. 
			/// </summary>
			Aliased = D2D1_ANTIALIAS_MODE_ALIASED
		};

		/// <summary>
		/// Describes the antialiasing mode used for drawing text.
		/// </summary>
		public enum class TextAntialiasMode : Int32
		{
			/// <summary>
			/// Use the system default.
			/// </summary>
			Default = D2D1_TEXT_ANTIALIAS_MODE_DEFAULT,
			/// <summary>
			/// Use ClearType antialiasing.
			/// </summary>
			ClearType = D2D1_TEXT_ANTIALIAS_MODE_CLEARTYPE,
			/// <summary>
			/// Use grayscale antialiasing.
			/// </summary>
			Grayscale = D2D1_TEXT_ANTIALIAS_MODE_GRAYSCALE,
			/// <summary>
			/// Do not use antialiasing.
			/// </summary>
			Aliased = D2D1_TEXT_ANTIALIAS_MODE_ALIASED
		};

		/// <summary>
		/// Describes the minimum DirectX support required for hardware rendering by a render target.
		/// </summary>
		public enum class FeatureLevel : Int32
		{
			/// <summary>
			/// Direct2D determines whether the video card provides adequate hardware rendering support.
			/// </summary>
			Default = D2D1_FEATURE_LEVEL_DEFAULT,
			/// <summary>
			/// The video card must support DirectX 9.
			/// </summary>
			Direct3D9 = D2D1_FEATURE_LEVEL_9,
			/// <summary>
			/// The video card must support DirectX 10.
			/// </summary>
			Direct3D10 = D2D1_FEATURE_LEVEL_10
		};

		/// <summary>
		/// Specifies how the alpha value of a bitmap or render target should be treated.
		/// </summary>
		public enum class AlphaMode : Int32
		{
			/// <summary>
			/// The alpha value might not be meaningful.
			/// </summary>
			Unknown = D2D1_ALPHA_MODE_UNKNOWN,
			/// <summary>
			/// The alpha value has been premultiplied. 
			/// Each color is first scaled by the alpha value. 
			/// The alpha value itself is the same in both straight and premultiplied alpha. 
			/// Typically, no color channel value is greater than the alpha channel value.
			// If a color channel value in a premultiplied format is greater than the alpha channel, the standard source-over blending math results in an additive blend.
			/// </summary>
			Premultiplied = D2D1_ALPHA_MODE_PREMULTIPLIED,
			/// <summary>
			/// The alpha value has not been premultiplied. The alpha channel indicates the transparency of the color.
			/// </summary>
			Straight = D2D1_ALPHA_MODE_STRAIGHT,
			/// <summary>
			/// The alpha value is ignored.
			/// </summary>
			Ignore = D2D1_ALPHA_MODE_IGNORE
		};

		/// <summary>
		/// Describes how a render target behaves when it presents its content. This enumeration allows a bitwise combination of its member values.
		/// </summary>
		public enum class PresentOptions : Int32
		{
			/// <summary>
			/// The render target waits until the display refreshes to present and discards the frame upon presenting.
			/// </summary>
			None = D2D1_PRESENT_OPTIONS_NONE,
			/// <summary>
			/// The render target does not discard the frame upon presenting.
			/// </summary>
			RetainContents = D2D1_PRESENT_OPTIONS_RETAIN_CONTENTS,
			/// <summary>
			/// The render target does not wait until the display refreshes to present.
			/// </summary>
			Immediately = D2D1_PRESENT_OPTIONS_IMMEDIATELY
		};

		/// <summary>
		/// Describes whether a render target uses hardware or software rendering, or if Direct2D should select the rendering mode.
		/// </summary>
		public enum class RenderTargetType : Int32
		{
			/// <summary>
			/// The render target uses hardware rendering, if available; otherwise, it uses software rendering.
			/// </summary>
			Default = D2D1_RENDER_TARGET_TYPE_DEFAULT,
			/// <summary>
			/// The render target uses software rendering only.
			/// </summary>
			Software = D2D1_RENDER_TARGET_TYPE_SOFTWARE,
			/// <summary>
			/// The render target uses hardware rendering only.
			/// </summary>
			Hardware = D2D1_RENDER_TARGET_TYPE_HARDWARE
		};

		/// <summary>
		/// Describes how a render target is remoted and whether it should be GDI-compatible. This enumeration allows a bitwise combination of its member values.
		/// </summary>
		public enum class RenderTargetUsage : Int32
		{
			/// <summary>
			/// The render target attempts to use Direct3D command-stream remoting and uses bitmap remoting if stream remoting fails. The render target is not GDI-compatible.
			/// </summary>
			None = D2D1_RENDER_TARGET_USAGE_NONE,
			/// <summary>
			/// The render target renders content locally and sends it to the terminal services client as a bitmap.
			/// </summary>
			ForceBitmapRemoting = D2D1_RENDER_TARGET_USAGE_FORCE_BITMAP_REMOTING,
			/// <summary>
			/// The render target can be used efficiently with GDI.
			/// </summary>
			GdiCompatible = D2D1_RENDER_TARGET_USAGE_GDI_COMPATIBLE
		};

		/// <summary>
		/// Describes the sequence of dashes and gaps in a stroke.
		/// </summary>
		public enum class DashStyle : Int32
		{
			/// <summary>
			/// A solid line with no breaks.
			/// </summary>
			Solid = D2D1_DASH_STYLE_SOLID,
			/// <summary>
			/// A dash followed by a gap of equal length. The dash and the gap are each twice as long as the stroke thickness.
			/// </summary>
			Dash = D2D1_DASH_STYLE_DASH,
			/// <summary>
			/// A dot followed by a longer gap.
			/// </summary>
			Dot = D2D1_DASH_STYLE_DOT,
			/// <summary>
			/// A dash, followed by a gap, followed by a dot, followed by another gap.
			/// </summary>
			DashDot = D2D1_DASH_STYLE_DASH_DOT,
			/// <summary>
			/// A dash, followed by a gap, followed by a dot, followed by another gap, followed by another dot, followed by another gap.
			/// </summary>
			DashDotDot = D2D1_DASH_STYLE_DASH_DOT_DOT,
			/// <summary>
			/// The dash pattern is specified by an array of floating-point values.
			/// </summary>
			Custom = D2D1_DASH_STYLE_CUSTOM
		};

		/// <summary>
		/// Describes the shape that joins two lines or segments.
		/// </summary>
		public enum class LineJoin : Int32
		{
			/// <summary>
			/// Regular angular vertices.
			/// </summary>
			Miter = D2D1_LINE_JOIN_MITER,
			/// <summary>
			/// Beveled vertices.
			/// </summary>
			Bevel = D2D1_LINE_JOIN_BEVEL,
			/// <summary>
			/// Rounded vertices.
			/// </summary>
			Round = D2D1_LINE_JOIN_ROUND,
			/// <summary>
			/// Regular angular vertices unless the join would extend beyond the miter limit; otherwise, beveled vertices.
			/// </summary>
			MiterOrBevel = D2D1_LINE_JOIN_MITER_OR_BEVEL
		};

		/// <summary>
		/// Describes the shape at the end of a line or segment.
		/// </summary>
		public enum class CapStyle : Int32
		{
			/// <summary>
			/// A cap that does not extend past the last point of the line. Comparable to cap used for objects other than lines.
			/// </summary>
			Flat = D2D1_CAP_STYLE_FLAT,
			/// <summary>
			/// Half of a square that has a length equal to the line thickness.
			/// </summary>
			Square = D2D1_CAP_STYLE_SQUARE,
			/// <summary>
			/// A semicircle that has a diameter equal to the line thickness.
			/// </summary>
			Round = D2D1_CAP_STYLE_ROUND,
			/// <summary>
			/// An isosceles right triangle whose hypotenuse is equal in length to the thickness of the line.
			/// </summary>
			Triangle = D2D1_CAP_STYLE_TRIANGLE
		};

		/// <summary>
		/// Specifies the different methods by which two geometries can be combined.
		/// </summary>
		public enum class CombineMode : Int32
		{
			/// <summary>
			/// The two regions are combined by taking the union of both. Given two geometries, A and B, the resulting geometry is geometry A + geometry B.
			/// </summary>
			Union = D2D1_COMBINE_MODE_UNION,
			/// <summary>
			/// The two regions are combined by taking their intersection. The new area consists of the overlapping region between the two geometries.
			/// </summary>
			Intersect = D2D1_COMBINE_MODE_INTERSECT,
			/// <summary>
			/// The two regions are combined by taking the area that exists in the first region but not the second and the area that exists in the second region but not the first. Given two geometries, A and B, the new region consists of (A-B) + (B-A).
			/// </summary>
			Xor = D2D1_COMBINE_MODE_XOR,
			/// <summary>
			/// The second region is excluded from the first. Given two geometries, A and B, the area of geometry B is removed from the area of geometry A, producing a region that is A-B.
			/// </summary>
			Exclude = D2D1_COMBINE_MODE_EXCLUDE
		};

		/// <summary>
		/// Describes how one geometry object is spatially related to another geometry object.
		/// </summary>
		public enum class GeometryRelation : Int32
		{
			/// <summary>
			/// The relationship between the two geometries cannot be determined. This value is never returned by any D2D method.
			/// </summary>
			Unknown = D2D1_GEOMETRY_RELATION_UNKNOWN,
			/// <summary>
			/// The two geometries do not intersect at all.
			/// </summary>
			Disjoint = D2D1_GEOMETRY_RELATION_DISJOINT,
			/// <summary>
			/// The instance geometry is entirely contained by the passed-in geometry.
			/// </summary>
			IsContained = D2D1_GEOMETRY_RELATION_IS_CONTAINED,
			/// <summary>
			/// The instance geometry entirely contains the passed-in geometry.
			/// </summary>
			Contains = D2D1_GEOMETRY_RELATION_CONTAINS,
			/// <summary>
			/// The two geometries overlap but neither completely contains the other.
			/// </summary>
			Overlap = D2D1_GEOMETRY_RELATION_OVERLAP
		};

		/// <summary>
		///Specifies how a geometry is simplified to an SimplifiedGeometrySink.
		/// </summary>
		public enum class GeometrySimplificationOption : Int32
		{
			/// <summary>
			/// The output can contain cubic Bezier curves and line segments.
			/// </summary>
			CubicsAndLines = D2D1_GEOMETRY_SIMPLIFICATION_OPTION_CUBICS_AND_LINES,
			/// <summary>
			/// The output is flattened so that it contains only line segments.
			/// </summary>
			Lines = D2D1_GEOMETRY_SIMPLIFICATION_OPTION_LINES
		};

		/// <summary>
		/// Defines the direction that an elliptical arc is drawn.
		/// </summary>
		public enum class SweepDirection : Int32
		{
			/// <summary>
			/// Arcs are drawn in a counterclockwise (negative-angle) direction.
			/// </summary>
			CounterClockwise = 0, //D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE,
			/// <summary>
			/// Arcs are drawn in a clockwise (positive-angle) direction.
			/// </summary>
			Clockwise = 1 //D2D1_SWEEP_DIRECTION_CLOCKWISE
		};

		/// <summary>
		/// Specifies whether an arc should be greater than 180 degrees.
		/// </summary>
		public enum class ArcSize : Int32
		{
			/// <summary>
			/// An arc's sweep should be 180 degrees or less.
			/// </summary>
			Small = D2D1_ARC_SIZE_SMALL,
			/// <summary>
			/// An arc's sweep should be 180 degrees or greater.
			/// </summary>
			Large = D2D1_ARC_SIZE_LARGE
		};

		/// <summary>
		/// Specifies how the intersecting areas of geometries are combined to form the area of the composite geometry. 
		/// </summary>
		public enum class FillMode : Int32
		{
			/// <summary>
			/// Determines whether a point is in the fill region by drawing a ray from that point to infinity 
			/// in any direction, and then counting the number of path segments within the given shape 
			/// that the ray crosses. If this number is odd, the point is in the fill region; 
			/// if even, the point is outside the fill region.
			/// </summary>
			Alternate = D2D1_FILL_MODE_ALTERNATE,
			/// <summary>
			/// Determines whether a point is in the fill region of the path by drawing a ray from that point to infinity in any direction, and then examining the places where a segment of the shape crosses the ray. 
			/// Starting with a count of zero, add one each time a segment crosses the ray from left to right and subtract one each time a path segment crosses the ray from right to left, as long as left and right are seen from the perspective of the ray. 
			/// After counting the crossings, if the result is zero, then the point is outside the path. 
			/// Otherwise, it is inside the path.
			/// </summary>   
			Winding = D2D1_FILL_MODE_WINDING
		};

		/// <summary>
		/// Indicates whether a segment should be stroked and whether the join between this segment and the previous one should be smooth. 
		/// This enumeration allows a bitwise combination of its member values.
		/// </summary>
		public enum class PathSegment : Int32
		{
			/// <summary>
			/// The segment is joined as specified by the StrokeStyle, and it is stroked.
			/// </summary>
			None = D2D1_PATH_SEGMENT_NONE,
			/// <summary>
			/// The segment is not stroked.
			/// </summary>
			ForceUnstroked = D2D1_PATH_SEGMENT_FORCE_UNSTROKED,
			/// <summary>
			/// The segment is always joined with the one preceding it using a round line join, regardless of which LineJoin is specified by the StrokeStyle. 
			/// If this segment is the first segment and the figure is closed, a round line join is used to connect the closing segment with the first segment. 
			/// If the figure is not closed, this setting has no effect on the first segment of the figure. 
			/// If SimplifiedGeometrySink::SetSegmentFlags is called just before SimplifiedGeometrySink::EndFigure, the join between the closing segment and the last explicitly specified segment is affected.
			/// </summary>
			ForceRoundLineJoin = D2D1_PATH_SEGMENT_FORCE_ROUND_LINE_JOIN
		};

		/// <summary>
		/// Indicates whether a specific GeometrySink figure is filled or hollow.
		/// </summary>
		public enum class FigureBegin : Int32
		{
			/// <summary>
			/// The figure is filled.
			/// </summary>
			Filled = D2D1_FIGURE_BEGIN_FILLED,
			/// <summary>
			/// The figure is hollow.
			/// </summary>
			Hollow = D2D1_FIGURE_BEGIN_HOLLOW
		};

		/// <summary>
		/// Indicates whether a specific GeometrySink figure is open or closed.
		/// </summary>
		public enum class FigureEnd : Int32
		{
			/// <summary>
			/// The figure is open.
			/// </summary>
			Open = D2D1_FIGURE_END_OPEN,
			/// <summary>
			/// The figure is closed.
			/// </summary>
			Closed = D2D1_FIGURE_END_CLOSED
		};

		/// <summary>
		/// Specifies whether Direct2D provides synchronization for an D2D1Factory and the resources it creates, so that they may be safely accessed from multiple threads.
		/// </summary>
		public enum class FactoryType : Int32
		{
			/// <summary>
			/// No synchronization is provided for accessing or writing to the factory or the objects it creates. 
			/// If the factory or the objects are called from multiple threads, it is up to the application to provide access locking.
			/// </summary>
			SingleThreaded = D2D1_FACTORY_TYPE_SINGLE_THREADED,
			/// <summary>
			/// Direct2D provides synchronization for accessing and writing to the factory and the objects it creates, enabling safe access from multiple threads.
			/// </summary>
			MultiThreaded = D2D1_FACTORY_TYPE_MULTI_THREADED
		};

		/// <summary>
		/// Indicates the type of information provided by the Direct2D Debug Layer.
		/// </summary>
		public enum class DebugLevel : Int32
		{
			/// <summary>
			/// Direct2D does not produce any debugging output.
			/// </summary>
			None = D2D1_DEBUG_LEVEL_NONE,
			/// <summary>
			/// Direct2D sends error messages to the debug layer.
			/// </summary>
			Error = D2D1_DEBUG_LEVEL_ERROR,
			/// <summary>
			/// Direct2D sends error messages and warnings to the debug layer.
			/// </summary>
			Warning = D2D1_DEBUG_LEVEL_WARNING,
			/// <summary>
			/// Direct2D sends error messages, warnings, and additional diagnostic information that can help improve performance to the debug layer.
			/// </summary>
			Information = D2D1_DEBUG_LEVEL_INFORMATION
		};

		public enum class Format : System::Int32
		{
			Unknown = DXGI_FORMAT_UNKNOWN,
			R32G32B32A32_Typeless = DXGI_FORMAT_R32G32B32A32_TYPELESS,
			R32G32B32A32_Float = DXGI_FORMAT_R32G32B32A32_FLOAT,
			R32G32B32A32_UInt = DXGI_FORMAT_R32G32B32A32_UINT,
			R32G32B32A32_SInt = DXGI_FORMAT_R32G32B32A32_SINT,
			R32G32B32_Typeless = DXGI_FORMAT_R32G32B32_TYPELESS,
			R32G32B32_Float = DXGI_FORMAT_R32G32B32_FLOAT,
			R32G32B32_UInt = DXGI_FORMAT_R32G32B32_UINT,
			R32G32B32_SInt = DXGI_FORMAT_R32G32B32_SINT,
			R16G16B16A16_Typeless = DXGI_FORMAT_R16G16B16A16_TYPELESS,
			R16G16B16A16_Float = DXGI_FORMAT_R16G16B16A16_FLOAT,
			R16G16B16A16_UNorm = DXGI_FORMAT_R16G16B16A16_UNORM,
			R16G16B16A16_UInt = DXGI_FORMAT_R16G16B16A16_UINT,
			R16G16B16A16_SNorm = DXGI_FORMAT_R16G16B16A16_SNORM,
			R16G16B16A16_SInt = DXGI_FORMAT_R16G16B16A16_SINT,
			R32G32_Typeless = DXGI_FORMAT_R32G32_TYPELESS,
			R32G32_Float = DXGI_FORMAT_R32G32_FLOAT,
			R32G32_UInt = DXGI_FORMAT_R32G32_UINT,
			R32G32_SInt = DXGI_FORMAT_R32G32_SINT,
			R32G8X24_Typeless = DXGI_FORMAT_R32G8X24_TYPELESS,
			D32_Float_S8X24_UInt = DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
			R32_Float_X8X24_Typeless = DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,
			X32_Typeless_G8X24_UInt = DXGI_FORMAT_X32_TYPELESS_G8X24_UINT,
			R10G10B10A2_Typeless = DXGI_FORMAT_R10G10B10A2_TYPELESS,
			R10G10B10A2_UNorm = DXGI_FORMAT_R10G10B10A2_UNORM,
			R10G10B10A2_UInt = DXGI_FORMAT_R10G10B10A2_UINT,
			R11G11B10_Float = DXGI_FORMAT_R11G11B10_FLOAT,
			R8G8B8A8_Typeless = DXGI_FORMAT_R8G8B8A8_TYPELESS,
			R8G8B8A8_UNorm = DXGI_FORMAT_R8G8B8A8_UNORM,
			R8G8B8A8_UNorm_SRGB = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,
			R8G8B8A8_UInt = DXGI_FORMAT_R8G8B8A8_UINT,
			R8G8B8A8_SNorm = DXGI_FORMAT_R8G8B8A8_SNORM,
			R8G8B8A8_SInt = DXGI_FORMAT_R8G8B8A8_SINT,
			R16G16_Typeless = DXGI_FORMAT_R16G16_TYPELESS,
			R16G16_Float = DXGI_FORMAT_R16G16_FLOAT,
			R16G16_UNorm = DXGI_FORMAT_R16G16_UNORM,
			R16G16_UInt = DXGI_FORMAT_R16G16_UINT,
			R16G16_SNorm = DXGI_FORMAT_R16G16_SNORM,
			R16G16_SInt = DXGI_FORMAT_R16G16_SINT,
			R32_Typeless = DXGI_FORMAT_R32_TYPELESS,
			D32_Float = DXGI_FORMAT_D32_FLOAT,
			R32_Float = DXGI_FORMAT_R32_FLOAT,
			R32_UInt = DXGI_FORMAT_R32_UINT,
			R32_SInt = DXGI_FORMAT_R32_SINT,
			R24G8_Typeless = DXGI_FORMAT_R24G8_TYPELESS,
			D24_UNorm_S8_UInt = DXGI_FORMAT_D24_UNORM_S8_UINT,
			R24_UNorm_X8_Typeless = DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
			X24_Typeless_G8_UInt = DXGI_FORMAT_X24_TYPELESS_G8_UINT,
			R8G8_Typeless = DXGI_FORMAT_R8G8_TYPELESS,
			R8G8_UNorm = DXGI_FORMAT_R8G8_UNORM,
			R8G8_UInt = DXGI_FORMAT_R8G8_UINT,
			R8G8_SNorm = DXGI_FORMAT_R8G8_SNORM,
			R8G8_SInt = DXGI_FORMAT_R8G8_SINT,
			R16_Typeless = DXGI_FORMAT_R16_TYPELESS,
			R16_Float = DXGI_FORMAT_R16_FLOAT,
			D16_UNorm = DXGI_FORMAT_D16_UNORM,
			R16_UNorm = DXGI_FORMAT_R16_UNORM,
			R16_UInt = DXGI_FORMAT_R16_UINT,
			R16_SNorm = DXGI_FORMAT_R16_SNORM,
			R16_SInt = DXGI_FORMAT_R16_SINT,
			R8_Typeless = DXGI_FORMAT_R8_TYPELESS,
			R8_UNorm = DXGI_FORMAT_R8_UNORM,
			R8_UInt = DXGI_FORMAT_R8_UINT,
			R8_SNorm = DXGI_FORMAT_R8_SNORM,
			R8_SInt = DXGI_FORMAT_R8_SINT,
			A8_UNorm = DXGI_FORMAT_A8_UNORM,
			R1_UNorm = DXGI_FORMAT_R1_UNORM,
			R9G9B9E5_SharedExp = DXGI_FORMAT_R9G9B9E5_SHAREDEXP,
			R8G8_B8G8_UNorm = DXGI_FORMAT_R8G8_B8G8_UNORM,
			G8R8_G8B8_UNorm = DXGI_FORMAT_G8R8_G8B8_UNORM,
			BC1_Typeless = DXGI_FORMAT_BC1_TYPELESS,
			BC1_UNorm = DXGI_FORMAT_BC1_UNORM,
			BC1_UNorm_SRGB = DXGI_FORMAT_BC1_UNORM_SRGB,
			BC2_Typeless = DXGI_FORMAT_BC2_TYPELESS,
			BC2_UNorm = DXGI_FORMAT_BC2_UNORM,
			BC2_UNorm_SRGB = DXGI_FORMAT_BC2_UNORM_SRGB,
			BC3_Typeless = DXGI_FORMAT_BC3_TYPELESS,
			BC3_UNorm = DXGI_FORMAT_BC3_UNORM,
			BC3_UNorm_SRGB = DXGI_FORMAT_BC3_UNORM_SRGB,
			BC4_Typeless = DXGI_FORMAT_BC4_TYPELESS,
			BC4_UNorm = DXGI_FORMAT_BC4_UNORM,
			BC4_SNorm = DXGI_FORMAT_BC4_SNORM,
			BC5_Typeless = DXGI_FORMAT_BC5_TYPELESS,
			BC5_UNorm = DXGI_FORMAT_BC5_UNORM,
			BC5_SNorm = DXGI_FORMAT_BC5_SNORM,
			B5G6R5_UNorm = DXGI_FORMAT_B5G6R5_UNORM,
			B5G5R5A1_UNorm = DXGI_FORMAT_B5G5R5A1_UNORM,
			B8G8R8A8_UNorm = DXGI_FORMAT_B8G8R8A8_UNORM,
			B8G8R8X8_UNorm = DXGI_FORMAT_B8G8R8X8_UNORM,
			//////////////////////////////////////////////////////////////////////////
			// DirectX 11 Format entries
			//////////////////////////////////////////////////////////////////////////

			/// <summary>Not Supported below DirectX 11.</summary>
			R10G10B10_XR_Bias_A2_UNorm = DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM,
			/// <summary>Not Supported below DirectX 11.</summary>
			B8G8R8A8_Typeless = DXGI_FORMAT_B8G8R8A8_TYPELESS,
			/// <summary>Not Supported below DirectX 11.</summary>
			B8G8R8A8_UNorm_SRGB = DXGI_FORMAT_B8G8R8A8_UNORM_SRGB,
			/// <summary>Not Supported below DirectX 11.</summary>
			B8G8R8X8_Typeless = DXGI_FORMAT_B8G8R8X8_TYPELESS,
			/// <summary>Not Supported below DirectX 11.</summary>
			B8G8R8X8_UNorm_SRGB = DXGI_FORMAT_B8G8R8X8_UNORM_SRGB,
			/// <summary>Not Supported below DirectX 11.</summary>
			BC6_Typeless = DXGI_FORMAT_BC6H_TYPELESS,
			/// <summary>Not Supported below DirectX 11.</summary>
			BC6_UFloat16 = DXGI_FORMAT_BC6H_UF16,
			/// <summary>Not Supported below DirectX 11.</summary>
			BC6_SFloat16 = DXGI_FORMAT_BC6H_SF16,
			/// <summary>Not Supported below DirectX 11.</summary>
			BC7_Typeless = DXGI_FORMAT_BC7_TYPELESS,
			/// <summary>Not Supported below DirectX 11.</summary>
			BC7_UNorm = DXGI_FORMAT_BC7_UNORM,
			/// <summary>Not Supported below DirectX 11.</summary>
			BC7_UNorm_SRGB = DXGI_FORMAT_BC7_UNORM_SRGB
		};

		/// <summary>
		/// Contains the center point, x-radius, and y-radius of an ellipse.
		/// </summary>
		public value class Ellipse
		{
		public:
			/// <summary>
			/// The center point of the ellipse.
			/// </summary>
			property Point Center;
			/// <summary>
			/// The X-radius of the ellipse.
			/// </summary>
			property float RadiusX;
			/// <summary>
			/// The Y-radius of the ellipse.
			/// </summary>
			property float RadiusY;
		};

		/// <summary>
		/// Contains the dimensions and corner radii of a rounded rectangle.
		/// </summary>
		public value class RoundedRectangle
		{
		public:
			/// <summary>
			/// The coordinates of the rectangle.
			/// </summary>
			property Rectangle Rectangle;
			/// <summary>
			/// The x-radius for the quarter ellipse that is drawn to replace every corner of the rectangle.
			/// </summary>
			property float RadiusX;
			/// <summary>
			/// The y-radius for the quarter ellipse that is drawn to replace every corner of the rectangle.
			/// </summary>
			property float RadiusY;
		};

		/// <summary>
		/// Contains the three vertices that describe a triangle.
		/// </summary>
		public value struct Triangle
		{
		public:

			/// <summary>
			/// The first vertex of a triangle.
			/// </summary>
			property Point Point1;
			/// <summary>
			/// The second vertex of a triangle.
			/// </summary>
			property Point Point2;
			/// <summary>
			/// The third vertex of a triangle.
			/// </summary>
			property Point Point3;

			/// <summary>
			/// Create a new instance of Triangle.
			/// </summary>
			/// <param name="point1">The first vertex of a triangle.</param>
			/// <param name="point2">The second vertex of a triangle.</param>
			/// <param name="point3">The third vertex of a triangle.</param>
			Triangle( Point point1, Point point2, Point point3 )
			{
				Point1 = point1;
				Point2 = point2;
				Point3 = point3;
			}
		};

		/// <summary>
		/// Represents a cubic bezier segment drawn between two points.
		/// </summary>
		[ StructLayout( LayoutKind::Sequential ) ]
		public value struct BezierSegment
		{
		private:

			Point _point1;
			Point _point2;
			Point _point3;

		public:

			/// <summary>
			/// Create a new instance of BezierSegment.
			/// </summary>
			BezierSegment( Point point1, Point point2, Point point3 )
			{
				_point1 = point1;
				_point2 = point2;
				_point3 = point3;
			}

			/// <summary>
			/// The first control point for the Bezier segment.
			/// </summary>
			property Point Point1 { Point get() { return _point1; } }

			/// <summary>
			/// The second control point for the Bezier segment.
			/// </summary>
			property Point Point2 { Point get() { return _point2; } }

			/// <summary>
			/// The end point for the Bezier segment.
			/// </summary>
			property Point Point3 { Point get() { return _point3; } }
		};

		/// <summary>
		/// Describes an elliptical arc between two points.
		/// </summary>
		[ StructLayout( LayoutKind::Sequential ) ]
		public value struct ArcSegment
		{
		private:

			Point        _point;
			Size         _size;
			float         _rotationAngle;
			D2D1_SWEEP_DIRECTION _sweepDirection;
			D2D1_ARC_SIZE        _arcSize;

		public:

			/// <summary>
			/// Create a new instance of ArcSegment.
			/// </summary>
			ArcSegment( Point point, Size size, float rotationAngle, SweepDirection sweepDirection, ArcSize arcSize )
			{
				_point = point;
				_size = size;
				_rotationAngle = rotationAngle;
				_sweepDirection = (D2D1_SWEEP_DIRECTION)sweepDirection;
				_arcSize = (D2D1_ARC_SIZE)arcSize;
			}

			/// <summary>
			/// The end point of the arc.
			/// </summary>
			property Kodo::Graphics::Point Point { Kodo::Graphics::Point get() { return _point; } }

			/// <summary>
			/// The x-radius and y-radius of the arc.
			/// </summary>
			property Kodo::Graphics::Size Size { Kodo::Graphics::Size get() { return _size; } }

			/// <summary>
			/// A value that specifies how many degrees in the clockwise direction the ellipse is rotated relative to the current coordinate system.
			/// </summary>
			property Single RotationAngle { Single get() { return _rotationAngle; } }

			/// <summary>
			/// A value that specifies whether the arc sweep is clockwise or counterclockwise.
			/// </summary>
			property Kodo::Graphics::SweepDirection SweepDirection { Kodo::Graphics::SweepDirection get() { return ( Kodo::Graphics::SweepDirection )_sweepDirection; } }

			/// <summary>
			/// A value that specifies whether the given arc is larger than 180 degrees.
			/// </summary>
			property Kodo::Graphics::ArcSize ArcSize { Kodo::Graphics::ArcSize get() { return ( Kodo::Graphics::ArcSize )_arcSize; } }
		};

		/// <summary>
		/// Contains the control point and end point for a quadratic Bezier segment.
		/// </summary>
		[ StructLayout( LayoutKind::Sequential ) ]
		public value struct QuadraticBezierSegment
		{
		private:

			Point _point1;
			Point _point2;

		public:

			/// <summary>
			/// Create a new instance of QuadraticBezierSegment.
			/// </summary>
			QuadraticBezierSegment( Point point1, Point point2 )
			{
				_point1 = point1;
				_point2 = point2;
			}

			/// <summary>
			/// The control point of the quadratic Bezier segment.
			/// </summary>
			property Point Point1 { Point get() { return _point1; } }

			/// <summary>
			/// The end point of the quadratic Bezier segment.
			/// </summary>
			property Point Point2 { Point get() { return _point2; } }
		};

		/// <summary>
		/// Contains the data format and alpha mode for a bitmap or render target.
		/// </summary>
		[ StructLayout( LayoutKind::Sequential ) ]
		public value class PixelFormat
		{
		public:

			/// <summary>
			/// A value that specifies the size and arrangement of channels in each pixel.
			/// </summary>
			property Kodo::Graphics::Format Format;
			/// <summary>
			/// A value that specifies whether the alpha channel is using pre-multiplied alpha, straight alpha, whether it should be ignored and considered opaque, or whether it is unknown.
			/// </summary>
			property Kodo::Graphics::AlphaMode AlphaMode;

			/// <summary>
			/// Create new instance of PixelFormat.
			/// </summary>
			PixelFormat( Kodo::Graphics::Format format, Kodo::Graphics::AlphaMode alphaMode )
			{
				Format = format;
				AlphaMode = alphaMode;
			}
		};
	}
}