#pragma once

#include "..\Graphics.hpp"
#include "..\Color.hpp"

#include "Brush.hpp"

namespace Kodo
{
	namespace Graphics
	{
		ref class Context;

		/// <summary> 
		/// Paints an area with a solid color. 
		///	</summary>
		public ref class SolidColorBrush : Brush
		{
			GRAPHICS_OBJECT( SolidColorBrush, ID2D1SolidColorBrush );

		public:

			/// <summary> Get or set the color of the brush. </summary>
			property Kodo::Graphics::Color Color
			{
				Kodo::Graphics::Color get() {
					D2D1_COLOR_F color = Native->GetColor();
					return Kodo::Graphics::Color( color.a, color.r, color.g, color.b );
				}
				void set( Kodo::Graphics::Color value ) {
					Native->SetColor( reinterpret_cast<D2D1_COLOR_F*>( &value ) );
				}
			}

			/// <summary> 
			/// Create a new <see cref="SolidColorBrush"/>. 
			/// </summary>
			/// <param name="context"> The <see cref="Context"/> that will own the brush. </param>
			SolidColorBrush( Context^ context ) : SolidColorBrush( context, Kodo::Graphics::Color::Black ) {  }
			/// <summary> 
			/// Create a new <see cref="SolidColorBrush"/>. 
			/// </summary>
			/// <param name="context"> The Context that will own the brush. </param>
			/// <param name="color"> The color of the brush.</param>
			SolidColorBrush( Context^ context, Kodo::Graphics::Color color );
			/// <summary> 
			/// Create a new <see cref="SolidColorBrush"/>. 
			/// </summary>
			/// <param name="context"> The Context that will own the brush. </param>
			/// <param name="color"> The color of the brush. </param>
			/// <param name="properties"> The properties of the brush. </param>
			SolidColorBrush( Context^ context, Kodo::Graphics::Color color, BrushProperties properties );
		};
	}
}
