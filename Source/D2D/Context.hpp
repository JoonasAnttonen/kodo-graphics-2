#pragma once

#include "..\Graphics.hpp"
#include "..\Matrix3x2.hpp"
#include "..\Color.hpp"

#include "Common.hpp"
#include "..\DW\Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		ref class FactoryD2D;
		ref class Brush;
		ref class Bitmap;
		ref class TextFormat;
		ref class TextLayout;
		ref class Geometry;
		ref class StrokeStyle;

		public value struct DirtyRect
		{
			LONG x;
			LONG y;
			LONG cx;
			LONG cy;

			DirtyRect( LONG l, LONG t, LONG r, LONG b ) :
				x( l ), y( t ), cx( r ), cy( b )
			{
			}
		};

		public ref class Context
		{
		public:

			Context( IntPtr hWnd );

		internal:

			Image^ _target;

			ID2D1DeviceContext1* _d2dContext;
			IDXGIAdapter* _dxgiAdapter;
			IDXGISurface* _dxgiBackBuffer;
			IDXGISwapChain1* _dxgiSwapChain;
			IDXGIFactory2* _dxgiFactory;

			virtual ~Context()
			{
				this->!Context();
				GC::SuppressFinalize( this );
			}

			!Context()
			{
				if ( _d2dContext ) {
					_d2dContext->Release();
					_d2dContext = nullptr;
				}
				if ( _dxgiAdapter ) {
					_dxgiAdapter->Release();
					_dxgiAdapter = nullptr;
				}
				if ( _dxgiBackBuffer ) {
					_dxgiBackBuffer->Release();
					_dxgiBackBuffer = nullptr;
				}
				if ( _dxgiSwapChain ) {
					_dxgiSwapChain->Release();
					_dxgiSwapChain = nullptr;
				}
				if ( _dxgiFactory ) {
					_dxgiFactory->Release();
					_dxgiFactory = nullptr;
				}
			}

		public:

			/// <summary>
			/// The <see cref="Kodo::Graphics::PixelFormat"/> of this context.
			/// </summary>
			property Kodo::Graphics::PixelFormat PixelFormat
			{
				Kodo::Graphics::PixelFormat get()
				{
					auto format = _d2dContext->GetPixelFormat();
					return Kodo::Graphics::PixelFormat( static_cast<Format>( format.format ), static_cast<AlphaMode>( format.alphaMode ) );
				}
			}

			/// <summary>
			/// Get or set the <see cref="Kodo::Graphics::AntialiasMode"/> of this context.
			/// </summary>
			property Kodo::Graphics::AntialiasMode AntialiasMode
			{
				Kodo::Graphics::AntialiasMode get()
				{
					return static_cast<Kodo::Graphics::AntialiasMode>( _d2dContext->GetAntialiasMode() );
				}
				void set( Kodo::Graphics::AntialiasMode value )
				{
					_d2dContext->SetAntialiasMode( static_cast<D2D1_ANTIALIAS_MODE>( value ) );
				}
			}

			Image^ GetTarget() { return _target; }
			void SetTarget( Image^ target )
			{
				_target = target;
				_d2dContext->SetTarget( target->Native );
			}

			Matrix3x2 GetTransform()
			{
				Matrix3x2 mat;
				_d2dContext->GetTransform( (D2D1_MATRIX_3X2_F*)&mat );
				return mat;
			}

			void SetTransform( Matrix3x2 transform )
			{
				_d2dContext->SetTransform( (D2D1_MATRIX_3X2_F*)&transform );
			}

			void DrawLine( Point begin, Point end, Brush^ brush, Single strokeWidth );
			void DrawLine( Point begin, Point end, Brush^ brush, Single strokeWidth, StrokeStyle^ stroke );

			void DrawLines( array<Point>^ points, Int32 start, Int32 count, Brush^ brush, Single strokeWidth );
			void DrawLines( array<Point>^ points, Int32 start, Int32 count, Brush^ brush, Single strokeWidth, StrokeStyle^ stroke );

			void DrawRectangle( Rectangle rect, Brush^ brush, Single strokeWidth );

			void DrawGeometry( Geometry^ geometry, Brush^ brush, Single strokeWidth );
			void DrawGeometry( Geometry^ geometry, Brush^ brush, Single strokeWidth, StrokeStyle^ stroke );

			void FillRectangle( Rectangle rect, Brush^ brush );
			void FillGeometry( Geometry^ geometry, Brush^ brush );
			void FillOpacityMask( Bitmap^ bitmap, Brush^ brush, Rectangle destinationArea );
			void DrawBitmap( Bitmap^ bitmap, Rectangle destinationArea, Single opacity, InterpolationMode interpolation );

			void DrawText( String^ text, TextFormat^ format, Rectangle area, Brush^ brush ) { DrawText( text, format, area, brush, DrawTextOptions::Clip, MeasuringMode::Natural ); }
			void DrawText( String^ text, TextFormat^ format, Rectangle area, Brush^ brush, DrawTextOptions options, MeasuringMode mode );

			void DrawTextLayout( TextLayout^ layout, Point origin, Brush^ brush ) { DrawTextLayout( layout, origin, brush, Kodo::Graphics::DrawTextOptions::None ); }
			void DrawTextLayout( TextLayout^ layout, Point origin, Brush^ brush, DrawTextOptions options );

			void Clear()
			{
				_d2dContext->Clear();
			}

			void Clear( Color color )
			{
				_d2dContext->Clear( (D2D1_COLOR_F*)&color );
			}

			void Resize( Size size );

			void BeginDraw()
			{
				_d2dContext->BeginDraw();
			}

			void EndDrawNoPresent()
			{
				ThrowIfFailed<GraphicsException>( _d2dContext->EndDraw() );
			}

			void EndDraw()
			{
				DXGI_PRESENT_PARAMETERS params = { 0 };

				ThrowIfFailed<GraphicsException>( _d2dContext->EndDraw() );
				auto res = _dxgiSwapChain->Present1( 1, 0, &params );

				if ( FAILED( res ) )
				{
					if ( res != DXGI_STATUS_OCCLUDED )
						ThrowIfFailed<GraphicsException>( res );
				}
			}

			void EndDraw( array<DirtyRect>^ dirties, Int32 dirtiesCount )
			{
				pin_ptr<DirtyRect> pinnedDirties = &dirties[ 0 ];

				DXGI_PRESENT_PARAMETERS params = { (UINT)dirtiesCount, (RECT*)pinnedDirties };

				ThrowIfFailed<GraphicsException>( _d2dContext->EndDraw() );

				auto res = _dxgiSwapChain->Present1( 1, 0, &params );

				if ( FAILED( res ) )
				{
					if ( res != DXGI_STATUS_OCCLUDED )
						ThrowIfFailed<GraphicsException>( res );
				}
			}
		};
	}
}