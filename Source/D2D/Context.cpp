#include "Context.hpp"

#include "Factory.hpp"
#include "Brush.hpp"
#include "Bitmap.hpp"
#include "Geometry.hpp"
#include "StrokeStyle.hpp"

#include "..\DW\TextLayout.hpp"

#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		Context::Context( IntPtr hWnd )
		{
			ID2D1DeviceContext1* d2dContext;
			ID2D1Bitmap1* d2dTargetBitmap;
			IDXGIAdapter* dxgiAdapter;
			IDXGISurface* dxgiBackBuffer;
			IDXGISwapChain1* dxgiSwapChain;
			IDXGIFactory2* dxgiFactory;

			ThrowIfFailed<GraphicsException>( GlobalAssets::Factory2D->_d2dDevice->CreateDeviceContext( D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &d2dContext ) );

			// Allocate a descriptor.
			DXGI_SWAP_CHAIN_DESC1 swapChainDesc = { 0 };
			swapChainDesc.Width = 0;                           // use automatic sizing
			swapChainDesc.Height = 0;
			swapChainDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; // this is the most common swapchain format
			swapChainDesc.Stereo = false;
			swapChainDesc.SampleDesc.Count = 1;                // don't use multi-sampling
			swapChainDesc.SampleDesc.Quality = 0;
			swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swapChainDesc.BufferCount = 2;                     // use double buffering to enable flip
			swapChainDesc.Scaling = DXGI_SCALING_NONE;
			swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL; // all apps must use this SwapEffect
			swapChainDesc.Flags = 0;

			// Identify the physical adapter (GPU or card) this device is runs on.
			ThrowIfFailed<GraphicsException>( GlobalAssets::Factory2D->_dxgiDevice->GetAdapter( &dxgiAdapter ) );
			ThrowIfFailed<GraphicsException>( dxgiAdapter->GetParent( IID_PPV_ARGS( &dxgiFactory ) ) );
			ThrowIfFailed<GraphicsException>( dxgiFactory->CreateSwapChainForHwnd(
				GlobalAssets::Factory2D->_d3dDevice,
				(HWND)hWnd.ToPointer(),
				&swapChainDesc,
				nullptr,
				nullptr,
				&dxgiSwapChain ) );

			ThrowIfFailed<GraphicsException>( GlobalAssets::Factory2D->_dxgiDevice->SetMaximumFrameLatency( 1 ) );
			ThrowIfFailed<GraphicsException>( dxgiSwapChain->GetBuffer( 0, IID_PPV_ARGS( &dxgiBackBuffer ) ) );

			auto dpiX = 0.0f;
			auto dpiY = 0.0f;
			GlobalAssets::Factory2D->_d2dFactory->GetDesktopDpi( &dpiX, &dpiY );

			// Now we set up the Direct2D render target bitmap linked to the swapchain. 
			// Whenever we render to this bitmap, it is directly rendered to the swap chain associated with the window.
			auto bitmapProperties = D2D1::BitmapProperties1(
				D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
				D2D1::PixelFormat( DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE ),
				dpiX, dpiY );

			// Get a D2D surface from the DXGI back buffer to use as the D2D render target.
			ThrowIfFailed<GraphicsException>(
				d2dContext->CreateBitmapFromDxgiSurface(
					dxgiBackBuffer,
					&bitmapProperties,
					&d2dTargetBitmap ) );

			// Now we can set the Direct2D render target.
			d2dContext->SetTarget( d2dTargetBitmap );

			_d2dContext = d2dContext;
			_dxgiAdapter = dxgiAdapter;
			_dxgiBackBuffer = dxgiBackBuffer;
			_dxgiSwapChain = dxgiSwapChain;
			_dxgiFactory = dxgiFactory;
			_target = gcnew Bitmap( d2dTargetBitmap );
		}

		void Context::Resize( Size size )
		{
			ID2D1Bitmap1* local_d2dTargetBitmap;
			IDXGISurface* local_dxgiBackBuffer;

			_target->~Image();
			_d2dContext->SetTarget( 0 );
			_dxgiBackBuffer->Release();
			_dxgiBackBuffer = nullptr;

			ThrowIfFailed<GraphicsException>( _dxgiSwapChain->ResizeBuffers( 0, static_cast<UINT>( size.Width ), static_cast<UINT>( size.Height ), DXGI_FORMAT_UNKNOWN, 0 ), "ResizeBuffers" );
			ThrowIfFailed<GraphicsException>( _dxgiSwapChain->GetBuffer( 0, IID_PPV_ARGS( &local_dxgiBackBuffer ) ) );

			auto bitmapProperties = D2D1::BitmapProperties1(
				D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
				D2D1::PixelFormat( DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE ) );

			// Get a D2D surface from the DXGI back buffer to use as the D2D render target.
			ThrowIfFailed<GraphicsException>(
				_d2dContext->CreateBitmapFromDxgiSurface(
					local_dxgiBackBuffer,
					&bitmapProperties,
					&local_d2dTargetBitmap ) );

			_d2dContext->SetTarget( local_d2dTargetBitmap );
			_dxgiBackBuffer = local_dxgiBackBuffer;
			_target = gcnew Bitmap( local_d2dTargetBitmap );
		}

		void Context::DrawLine( Point begin, Point end, Brush^ brush, Single strokeWidth )
		{
			_d2dContext->DrawLine( D2D1_POINT_2F{ begin.X, begin.Y },
								   D2D1_POINT_2F{ end.X, end.Y },
								   brush->Native,
								   strokeWidth );
		}

		void Context::DrawLine( Point begin, Point end, Brush^ brush, Single strokeWidth, StrokeStyle^ stroke )
		{
			_d2dContext->DrawLine( D2D1_POINT_2F{ begin.X, begin.Y },
								   D2D1_POINT_2F{ end.X, end.Y },
								   brush->Native,
								   strokeWidth,
								   stroke->Native );
		}

		void Context::DrawLines( array<Point>^ points, Int32 start, Int32 count, Brush^ brush, Single strokeWidth )
		{
			pin_ptr<Point> pinnedPoints = &points[ 0 ];
			D2D1_POINT_2F* nativePoints = reinterpret_cast<D2D1_POINT_2F*>( pinnedPoints );
			ID2D1Brush* nativeBrush = brush->Native;
			ID2D1StrokeStyle* nativeStroke = nullptr;

			for ( auto i = start; i < count; i++ )
			{
				_d2dContext->DrawLine( nativePoints[ i ],
									   nativePoints[ i + 1 ],
									   nativeBrush,
									   strokeWidth,
									   nativeStroke );
			}
		}

		void Context::DrawLines( array<Point>^ points, Int32 start, Int32 count, Brush^ brush, Single strokeWidth, StrokeStyle^ stroke )
		{
			pin_ptr<Point> pinnedPoints = &points[ 0 ];
			D2D1_POINT_2F* nativePoints = reinterpret_cast<D2D1_POINT_2F*>( pinnedPoints );
			ID2D1Brush* nativeBrush = brush->Native;
			ID2D1StrokeStyle* nativeStroke = stroke->Native;

			for ( auto i = start; i < count; i++ )
			{
				_d2dContext->DrawLine( nativePoints[ i ],
									   nativePoints[ i + 1 ],
									   nativeBrush,
									   strokeWidth,
									   nativeStroke );
			}
		}

		void Context::FillGeometry( Geometry^ geometry, Brush^ brush )
		{
			_d2dContext->FillGeometry( geometry->Native,
									   brush->Native );
		}

		void Context::DrawGeometry( Geometry^ geometry, Brush^ brush, Single strokeWidth )
		{
			_d2dContext->DrawGeometry( geometry->Native,
									   brush->Native,
									   strokeWidth );
		}

		void Context::DrawGeometry( Geometry^ geometry, Brush^ brush, Single strokeWidth, StrokeStyle^ stroke )
		{
			_d2dContext->DrawGeometry( geometry->Native,
									   brush->Native,
									   strokeWidth,
									   stroke->Native );
		}

		void Context::DrawRectangle( Rectangle rect, Brush^ brush, Single strokeWidth )
		{
			_d2dContext->DrawRectangle( (D2D1_RECT_F*)&rect,
										brush->Native,
										strokeWidth );
		}

		void Context::DrawBitmap( Bitmap^ bitmap, Rectangle destinationArea, Single opacity, InterpolationMode interpolation )
		{
			_d2dContext->DrawBitmap( bitmap->Native,
									 (D2D1_RECT_F*)&destinationArea,
									 opacity,
									 static_cast<D2D1_INTERPOLATION_MODE>( interpolation ) );
		}

		void Context::DrawText( String^ text, TextFormat^ format, Rectangle area, Brush^ brush, DrawTextOptions options, MeasuringMode mode )
		{
			if ( !String::IsNullOrEmpty( text ) )
			{
				pin_ptr<const wchar_t> pinnedText = PtrToStringChars( text );
				_d2dContext->DrawText( pinnedText,
									   text->Length,
									   format->Native,
									   (D2D1_RECT_F*)&area,
									   brush->Native,
									   static_cast<D2D1_DRAW_TEXT_OPTIONS>( options ),
									   static_cast<DWRITE_MEASURING_MODE>( mode ) );
			}
		}

		void Context::DrawTextLayout( TextLayout^ layout, Point origin, Brush^ brush, DrawTextOptions options )
		{
			_d2dContext->DrawTextLayout( D2D1_POINT_2F{ origin.X, origin.Y },
										 layout->Native,
										 brush->Native,
										 static_cast<D2D1_DRAW_TEXT_OPTIONS>( options ) );
		}

		void Context::FillRectangle( Rectangle rect, Brush^ brush )
		{
			_d2dContext->FillRectangle( (D2D1_RECT_F*)&rect,
										brush->Native );
		}

		void Context::FillOpacityMask( Bitmap^ bitmap, Brush^ brush, Rectangle destinationArea )
		{
			_d2dContext->FillOpacityMask( bitmap->Native,
										  brush->Native,
										  (D2D1_RECT_F*)&destinationArea );
		}
	}
}