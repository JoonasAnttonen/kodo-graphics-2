#pragma once

#include "..\Graphics.hpp"

namespace Kodo
{
	namespace Graphics
	{
		public ref class FactoryD2D
		{
		internal:

			ID2D1Factory2* _d2dFactory;
			ID3D11Device* _d3dDevice;
			ID3D11DeviceContext* _d3dContext;
			ID3D11Texture2D* _backBuffer;
			IDXGIDevice1* _dxgiDevice;
			ID2D1Device1* _d2dDevice;

		public:

			FactoryD2D()
			{
				ID2D1Factory2* d2dFactory;
				ID2D1Device1* d2dDevice;
				ID3D11Device* d3dDevice;
				ID3D11DeviceContext* d3dContext;
				IDXGIDevice1* dxgiDevice;

				ThrowIfFailed<Exception>( D2D1CreateFactory<ID2D1Factory2>( D2D1_FACTORY_TYPE_SINGLE_THREADED, &d2dFactory ) );

				UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
/*#if defined(_DEBUG)
				// If the project is in a debug build, enable the debug layer.
				creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif*/

				D3D_FEATURE_LEVEL selectedFeatureLevel;
				D3D_FEATURE_LEVEL featureLevels[] =
				{
					D3D_FEATURE_LEVEL_11_1, D3D_FEATURE_LEVEL_11_0,
					D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_10_0,
					D3D_FEATURE_LEVEL_9_3, D3D_FEATURE_LEVEL_9_2, D3D_FEATURE_LEVEL_9_1
				};

				ThrowIfFailed<Exception>( D3D11CreateDevice(
					nullptr,                          // specify null to use the default adapter
					D3D_DRIVER_TYPE_HARDWARE,
					0,
					creationFlags, // optionally set debug and Direct2D compatibility flags
					featureLevels,                    // list of feature levels this app can support
					ARRAYSIZE( featureLevels ),       // number of possible feature levels
					D3D11_SDK_VERSION,
					&d3dDevice,                       // returns the Direct3D device created
					&selectedFeatureLevel,            // returns feature level of device created
					&d3dContext ) );                  // returns the device immediate context	

				// 1. Obtain the underlying DXGI device of the Direct3D11 device.
				// 2. Obtain the Direct2D device for 2-D rendering.
				ThrowIfFailed<Exception>( d3dDevice->QueryInterface( __uuidof( IDXGIDevice1 ), (void**)&dxgiDevice ) );
				ThrowIfFailed<Exception>( d2dFactory->CreateDevice( dxgiDevice, &d2dDevice ) );

				_d2dFactory = d2dFactory;
				_d2dDevice = d2dDevice;
				_d3dDevice = d3dDevice;
				_d3dContext = d3dContext;
				_dxgiDevice = dxgiDevice;
			}
		};
	}
}