#pragma once

#include "Bitmap.hpp"
#include "Brush.hpp"

namespace Kodo
{
	namespace Graphics
	{
		ref class Context;

		/// <summary>
		/// Paints an area with a solid color.
		/// </summary>
		public ref class BitmapBrush : Brush
		{
			GRAPHICS_OBJECT( BitmapBrush, ID2D1BitmapBrush1 );

		public:

			/*property Kodo::Graphics::Bitmap^ Bitmap
			{
				Kodo::Graphics::Bitmap^ get();
			}*/

			property ExtendMode ExtendModeX
			{
				ExtendMode get();
				void set( ExtendMode value );
			}

			property ExtendMode ExtendModeY
			{
				ExtendMode get();
				void set( ExtendMode value );
			}

			property BitmapInterpolationMode InterpolationMode
			{
				BitmapInterpolationMode get();
				void set( BitmapInterpolationMode value );
			}

			BitmapBrush( Context^ context, Kodo::Graphics::Bitmap^ bitmap );
		};
	}
}
