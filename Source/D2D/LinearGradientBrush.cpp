#include "LinearGradientBrush.hpp"

#include "Context.hpp"

namespace Kodo
{
	namespace Graphics
	{
		GradientStopCollection::GradientStopCollection( Context^ context, array<GradientStop>^ stops )
		{
			ID2D1GradientStopCollection *collection = nullptr;

			pin_ptr<GradientStop> pinnedStops = &stops[ 0 ];

			ThrowIfFailed<GraphicsException>(
				context->_d2dContext->CreateGradientStopCollection( reinterpret_cast<D2D1_GRADIENT_STOP*>( pinnedStops ), stops->Length, &collection )
				);

			Assing( collection );
		}

		GradientStopCollection::GradientStopCollection( Context^ context, array<GradientStop>^ stops, Gamma gamma, Kodo::Graphics::ExtendMode extendMode )
		{
			ID2D1GradientStopCollection *collection = nullptr;

			pin_ptr<GradientStop> pinnedStops = &stops[ 0 ];

			ThrowIfFailed<GraphicsException>( 
				context->_d2dContext->CreateGradientStopCollection( reinterpret_cast<D2D1_GRADIENT_STOP*>( pinnedStops ), stops->Length, static_cast<D2D1_GAMMA>( gamma ), static_cast<D2D1_EXTEND_MODE>( extendMode ), &collection )
				);

			Assing( collection );
		}

		array<GradientStop>^ GradientStopCollection::GetStops()
		{
			auto count = Native->GetGradientStopCount();
			auto results = gcnew array<GradientStop>( count );
			pin_ptr<GradientStop> pinnedResults = &results[ 0 ];

			Native->GetGradientStops( reinterpret_cast<D2D1_GRADIENT_STOP*>( pinnedResults ), count );

			return results;
		}

		LinearGradientBrush::LinearGradientBrush( Context^ context, GradientStopCollection^ gradientStops, LinearGradientBrushProperties linearGradientBrushProperties )
		{
			ID2D1LinearGradientBrush *brush = nullptr;

			ThrowIfFailed<GraphicsException>(
				context->_d2dContext->CreateLinearGradientBrush( reinterpret_cast<D2D1_LINEAR_GRADIENT_BRUSH_PROPERTIES*>( &linearGradientBrushProperties ), nullptr, gradientStops->Native, &brush )
				);

			Assing( brush );
		}

		LinearGradientBrush::LinearGradientBrush( Context^ context, GradientStopCollection^ gradientStops, LinearGradientBrushProperties linearGradientBrushProperties, BrushProperties properties )
		{
			ID2D1LinearGradientBrush *brush = nullptr;

			ThrowIfFailed<GraphicsException>(
				context->_d2dContext->CreateLinearGradientBrush( reinterpret_cast<D2D1_LINEAR_GRADIENT_BRUSH_PROPERTIES*>( &linearGradientBrushProperties ), reinterpret_cast<D2D1_BRUSH_PROPERTIES*>( &properties ), gradientStops->Native, &brush )
				);

			Assing( brush );
		}

		Point LinearGradientBrush::Start::get()
		{
			D2D1_POINT_2F result = Native->GetStartPoint();

			return Point( result.x, result.y );
		}

		void LinearGradientBrush::Start::set( Point value )
		{
			D2D1_POINT_2F point = D2D1::Point2F( value.X, value.Y );

			Native->SetStartPoint( point );
		}

		Point LinearGradientBrush::End::get()
		{
			D2D1_POINT_2F result = Native->GetEndPoint();

			return Point( result.x, result.y );
		}

		void LinearGradientBrush::End::set( Point value )
		{
			D2D1_POINT_2F point = D2D1::Point2F( value.X, value.Y );

			Native->SetEndPoint( point );
		}

		GradientStopCollection^ LinearGradientBrush::GradientStops::get()
		{
			ID2D1GradientStopCollection *stops = nullptr;

			Native->GetGradientStopCollection( &stops );

			return gcnew GradientStopCollection( stops );
		}
	}
}