#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		/// <summary> Describes a geometric path that does not contain quadratic bezier curves or arcs. </summary>
		public ref class SimplifiedGeometrySink : GraphicsObject
		{
			GRAPHICS_OBJECT( SimplifiedGeometrySink, ID2D1SimplifiedGeometrySink );

		protected:

			SimplifiedGeometrySink() {}

		public:
			/// <summary> Creates a sequence of cubic Bezier curves and adds them to the geometry sink. </summary>
			/// <param name="beziers">
			/// An array of Bezier segments that describes the Bezier curves to create. 
			/// A curve is drawn from the geometry sink's current point (the end point of the last segment drawn or the location specified by BeginFigure) to the end point of the first Bezier segment in the array. 
			/// If the array contains additional Bezier segments, each subsequent Bezier segment uses the end point of the preceding Bezier segment as its start point.
			/// </param>
			void AddBeziers( array<BezierSegment>^ beziers )
			{
				pin_ptr<BezierSegment> pinnedBeziers = &beziers[ 0 ];

				Native->AddBeziers( reinterpret_cast<D2D1_BEZIER_SEGMENT*>( pinnedBeziers ), beziers->Length );
			}
			/// <summary> Creates a sequence of lines using the specified points and adds them to the geometry sink. </summary>
			/// <param name="points">
			/// An array of one or more points that describe the lines to draw. 
			/// A line is drawn from the geometry sink's current point (the end point of the last segment drawn or the location specified by BeginFigure) to the first point in the array. 
			/// If the array contains additional points, a line is drawn from the first point to the second point in the array, from the second point to the third point, and so on.
			/// </param>
			void AddLines( array<Point>^ points )
			{
				pin_ptr<Point> pinnedPoints = &points[ 0 ];

				Native->AddLines( reinterpret_cast<D2D1_POINT_2F*>( pinnedPoints ), points->Length );
			}
			/// <summary> Starts a new figure at the specified point. </summary>
			/// <param name="startPoint">The point at which to begin the new figure.</param>
			/// <param name="style">Whether the new figure should be hollow or filled.</param>
			void BeginFigure( Point startPoint, FigureBegin style )
			{
				D2D1_POINT_2F point = D2D1::Point2F( startPoint.X, startPoint.Y );

				Native->BeginFigure( point, static_cast<D2D1_FIGURE_BEGIN>( style ) );
			}
			/// <summary> Closes the geometry sink, indicates whether it is in an error state, and resets the sink's error state. </summary>
			void Close()
			{
				ThrowIfFailed<GraphicsException>( Native->Close() );
			}
			/// <summary> Ends the current figure. </summary>
			/// <param name="style">A value that indicates whether the current figure is closed. If the figure is closed, a line is drawn between the current point and the start point specified by BeginFigure.</param>
			void EndFigure( FigureEnd style )
			{
				Native->EndFigure( static_cast<D2D1_FIGURE_END>( style ) );
			}
			/// <summary> Specifies the method used to determine which points are inside the geometry described by this geometry sink and which points are outside.
			/// </summary>
			/// <param name="fillMode">The method used to determine whether a given point is part of the geometry.</param>
			void SetFillMode( FillMode fillMode )
			{
				Native->SetFillMode( static_cast<D2D1_FILL_MODE>( fillMode ) );
			}
			/// <summary> Specifies stroke and join options to be applied to new segments added to the geometry sink. </summary>
			/// <param name="vertexFlags">Stroke and join options to be applied to new segments added to the geometry sink.</param>
			void SetSegmentFlags( PathSegment vertexFlags )
			{
				Native->SetSegmentFlags( static_cast<D2D1_PATH_SEGMENT>( vertexFlags ) );
			}
		};
	}
}