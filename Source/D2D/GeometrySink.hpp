#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

#include "SimplifiedGeometrySink.hpp"

namespace Kodo
{
	namespace Graphics
	{
		/// <summary> Describes a geometric path that can contain lines, arcs, cubic Bezier curves, and quadratic Bezier curves. </summary>
		public ref class GeometrySink : SimplifiedGeometrySink
		{
			GRAPHICS_OBJECT( GeometrySink, ID2D1GeometrySink );

		public:

			/// <summary> Creates a single arc and adds it to the path geometry. </summary>
			/// <param name="arc">The arc segment to add to the figure.</param>
			void AddArc( ArcSegment arc )
			{
				Native->AddArc( reinterpret_cast<D2D1_ARC_SEGMENT*>( &arc ) );
			}
			/// <summary> Creates a cubic Bezier curve between the current point and the specified end point and adds it to the geometry sink. </summary>
			/// <param name="bezier">A structure that describes the control points and end point of the Bezier curve to add.</param>
			void AddBezier( BezierSegment bezier )
			{
				Native->AddBezier( reinterpret_cast<D2D1_BEZIER_SEGMENT*>( &bezier ) );
			}
			/// <summary> Creates a line segment between the current point and the specified end point and adds it to the geometry sink. </summary>
			/// <param name="point">The end point of the line to draw.</param>
			void AddLine( Point point )
			{
				D2D1_POINT_2F p = D2D1::Point2F( point.X, point.Y );
				Native->AddLine( p );
			}
			/// <summary> Creates a quadratic Bezier curve between the current point and the specified end point and adds it to the geometry sink. </summary>
			/// <param name="bezier">A structure that describes the control point and the end point of the quadratic Bezier curve to add.</param>
			void AddQuadraticBezier( QuadraticBezierSegment bezier )
			{
				Native->AddQuadraticBezier( reinterpret_cast<D2D1_QUADRATIC_BEZIER_SEGMENT*>( &bezier ) );
			}
			/// <summary> Adds a sequence of quadratic Bezier segments as an array in a single call. </summary>
			/// <param name="beziers">An array of a sequence of quadratic Bezier segments.</param>
			void AddQuadraticBeziers( array<QuadraticBezierSegment>^ beziers )
			{
				pin_ptr<QuadraticBezierSegment> pinnedBeziers = &beziers[ 0 ];
				Native->AddQuadraticBeziers( reinterpret_cast<D2D1_QUADRATIC_BEZIER_SEGMENT*>( pinnedBeziers ), beziers->Length );
			}
		};
	}
}