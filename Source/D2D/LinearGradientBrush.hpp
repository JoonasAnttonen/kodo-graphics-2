#pragma once

#include "Common.hpp"
#include "..\Color.hpp"

#include "Brush.hpp"

namespace Kodo
{
	namespace Graphics
	{
		using namespace System;

		ref class Context;

		/// <summary> Contains the position and color of a gradient stop. </summary>
		public value class GradientStop
		{
		public:

			/// <summary> A value that indicates the relative position of the gradient stop in the brush. This value must be in the [0.0f, 1.0f] range if the gradient stop is to be seen explicitly. </summary>
			property Single Position;
			/// <summary> The color of the gradient stop. </summary>
			property Kodo::Graphics::Color Color;

			GradientStop( Single pos, Kodo::Graphics::Color color )
			{
				Position = pos;
				Color = color;
			}
		};

		/// <summary> Represents an collection of GradientStop objects for linear and radial gradient brushes. </summary>
		public ref class GradientStopCollection : GraphicsObject
		{
			GRAPHICS_OBJECT( GradientStopCollection, ID2D1GradientStopCollection );

		public:

			/// <summary> Indicates the gamma space in which the gradient stops are interpolated. </summary>
			property Gamma InterpolationGamma
			{
				Gamma get() { return static_cast<Gamma>( Native->GetColorInterpolationGamma() ); }
			}

			/// <summary> Indicates the behavior of the gradient outside the normalized gradient range. </summary>
			property Kodo::Graphics::ExtendMode ExtendMode
			{
				Kodo::Graphics::ExtendMode get()
				{
					return static_cast<Kodo::Graphics::ExtendMode>( Native->GetExtendMode() );
				}
			}

			/// <summary> Create a new instance of <see cref="GradientStopCollection"/> </summary>
			/// <param name="context">The render target from which to create this brush.</param>
			GradientStopCollection( Context^ context, array<GradientStop>^ stops );
			/// <summary> Create a new instance of <see cref="GradientStopCollection"/> </summary>
			/// <param name="context">The render target from which to create this brush.</param>
			GradientStopCollection( Context^ context, array<GradientStop>^ stops, Gamma gamma, Kodo::Graphics::ExtendMode extendMode );

			/// <summary> Copies the gradient stops from the collection into an array of GradientStop structures. </summary>
			array<GradientStop>^ GetStops();
		};

		/// <summary> Contains the starting point and endpoint of the gradient axis for an LinearGradientBrush. </summary>
		public value class LinearGradientBrushProperties
		{
		public:

			/// <summary> The starting coordinates of the linear gradient. </summary>
			property Point StartPoint;
			/// <summary> The ending coordinates of the linear gradient. </summary>
			property Point EndPoint;
		};

		/// <summary> Paints an area with a linear gradient. </summary>
		public ref class LinearGradientBrush : Brush
		{
			GRAPHICS_OBJECT( LinearGradientBrush, ID2D1LinearGradientBrush );

		public:

			/// <summary> Get or set the starting coordinates of the linear gradient. </summary>
			property Point Start
			{
				Point get();
				void set( Point value );
			}
			/// <summary> Get or set he ending coordinates of the linear gradient. </summary>
			property Point End
			{
				Point get();
				void set( Point value );
			}
			/// <summary> Get the <see cref="GradientStopCollection"/> associated with this linear gradient brush. </summary>
			property GradientStopCollection^ GradientStops
			{
				GradientStopCollection^ get();
			}

			/// <summary> Create a new instance of <see cref="LinearGradientBrush"/> </summary>
			/// <param name="context">The render target from which to create this brush.</param>
			/// <param name="gradientStops"></param>
			/// <param name="linearGradientBrushProperties"></param>
			LinearGradientBrush( Context^ context, GradientStopCollection^ gradientStops, LinearGradientBrushProperties linearGradientBrushProperties );
			/// <summary> Create a new instance of <see cref="LinearGradientBrush"/> </summary>
			/// <param name="context">The render target from which to create this brush.</param>
			/// <param name="gradientStops"></param>
			/// <param name="linearGradientBrushProperties"></param>
			/// <param name="properties"></param>
			LinearGradientBrush( Context^ context, GradientStopCollection^ gradientStops, LinearGradientBrushProperties linearGradientBrushProperties, BrushProperties properties );
		};
	}
}