#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

#include "Geometry.hpp"

namespace Kodo
{
	namespace Graphics
	{
		ref class GeometrySink;

		public ref class PathGeometry : Geometry
		{
			GRAPHICS_OBJECT( PathGeometry, ID2D1PathGeometry );

		public:
			/// <summary> The number of segments in the path geometry. </summary>
			property Int32 SegmentCount
			{
				Int32 get()
				{
					UINT32 count;
					Native->GetSegmentCount( &count );
					return count;
				}
			}
			/// <summary> The number of figures in the path geometry. </summary>
			property Int32 FigureCount
			{
				Int32 get()
				{
					UINT32 count;
					Native->GetFigureCount( &count );
					return count;
				}
			}
			/// <summary> Create an empty PathGeometry. </summary>
			PathGeometry();
			/// <summary> Retrieves the geometry sink that is used to populate the path geometry with figures and segments. </summary>
			GeometrySink^ Open();
			/// <summary> </summary>
			/// <param name="geometrySink">Copies the contents of the path geometry to the specified GeometrySink.</param>
			void Stream( GeometrySink^ geometrySink );
		};
	}
}