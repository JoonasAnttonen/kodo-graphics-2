#include "BitmapBrush.hpp"

#include "Context.hpp"
//#include "Bitmap.hpp"

namespace Kodo
{
	namespace Graphics
	{
		/*Kodo::Graphics::Bitmap^ BitmapBrush::Bitmap::get()
		{
			ID2D1Bitmap *bitmap;
			Native->GetBitmap( &bitmap );
			return gcnew Kodo::Graphics::Bitmap( bitmap );
		}*/

		ExtendMode BitmapBrush::ExtendModeX::get()
		{
			return (ExtendMode)Native->GetExtendModeX();
		}
		void BitmapBrush::ExtendModeX::set( ExtendMode value )
		{
			Native->SetExtendModeX( (D2D1_EXTEND_MODE)value );
		}

		ExtendMode BitmapBrush::ExtendModeY::get()
		{
			return (ExtendMode)Native->GetExtendModeY();
		}
		void BitmapBrush::ExtendModeY::set( ExtendMode value )
		{
			Native->SetExtendModeY( (D2D1_EXTEND_MODE)value );
		}

		BitmapInterpolationMode BitmapBrush::InterpolationMode::get()
		{
			return (BitmapInterpolationMode)Native->GetInterpolationMode();
		}
		void BitmapBrush::InterpolationMode::set( BitmapInterpolationMode value )
		{
			Native->SetInterpolationMode( (D2D1_BITMAP_INTERPOLATION_MODE)value );
		}

		BitmapBrush::BitmapBrush( Context^ context, Kodo::Graphics::Bitmap^ bitmap )
		{
			ID2D1BitmapBrush *brush = nullptr;

			ThrowIfFailed<GraphicsException>(
				context->_d2dContext->CreateBitmapBrush( bitmap->Native, &brush )
				);

			Assing( brush );
		}
	}
}
