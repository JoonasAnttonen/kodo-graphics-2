#pragma once

namespace Kodo
{
	namespace Graphics
	{
		using namespace System;
		using namespace System::Runtime::InteropServices;

		// Size
		//______________________________________________________________________________________

		[ StructLayout( LayoutKind::Sequential ) ]
		public value struct Size
		{
		private:

			Single width;
			Single height;

		public:

			property Single Width { Single get() { return width; } void set( Single value ) { width = value; } }
			property Single Height { Single get() { return height; } void set( Single value ) { height = value; } }

			Size( Single w, Single h ) : width( w ), height( h ) { }

			virtual Boolean Equals( Size other )
			{
				return ( *this == other );
			}

			Boolean Equals( Object^ obj ) override
			{
				if ( obj != nullptr )
					return ( (Size)obj ) == *this;

				return false;
			}

			Int32 GetHashCode() override
			{
				return (Int32)( width + height );
			}

			String^ ToString() override
			{
				return String::Format( System::Globalization::CultureInfo::InvariantCulture, "{0},{1}", width, height );
			}

			static Boolean operator !=( Size left, Size right ) { return !( left == right ); }
			static Boolean operator ==( Size left, Size right )
			{
				return ( left.width == right.width && left.height == right.height );
			}
		};

		// Point
		//______________________________________________________________________________________

		[ StructLayout( LayoutKind::Sequential ) ]
		public value struct Point
		{
		private:

			Single x;
			Single y;

		public:

			property Single X { Single get() { return x; } void set( Single value ) { x = value; } }
			property Single Y { Single get() { return y; } void set( Single value ) { y = value; } }

			Point( Single x, Single y ) : x( x ), y( y ) { }

			virtual Boolean Equals( Point other )
			{
				return ( *this == other );
			}

			Boolean Equals( Object^ obj ) override
			{
				if ( obj != nullptr )
					return ( (Point)obj ) == *this;

				return false;
			}

			Int32 GetHashCode() override
			{
				return (Int32)( x + y );
			}

			String^ ToString() override
			{
				return String::Format( System::Globalization::CultureInfo::InvariantCulture, "{0},{1}", x, y );
			}

			static Boolean operator !=( Point left, Point right ) { return !( left == right ); }
			static Boolean operator ==( Point left, Point right )
			{
				return ( left.x == right.x && left.y == right.y );
			}
		};

		/// <summary>
		/// Describes a rectangular area with four single precision floating point coordinates.
		/// </summary>
		[ StructLayout( LayoutKind::Sequential ) ]
		public value struct Rectangle : IEquatable<Rectangle>
		{
		private:

			Single left;
			Single top;
			Single right;
			Single bottom;

		public:

			property Point TopLeft { Point get() { return Point( left, top ); } }
			property Point TopRight { Point get() { return Point( right, top ); } }
			property Point BottomLeft { Point get() { return Point( left, bottom ); } }
			property Point BottomRight { Point get() { return Point( right, bottom ); } }

			property Single Left { Single get() { return left; } void set( Single value ) { left = value; } }
			property Single Top { Single get() { return top; } void set( Single value ) { top = value; } }
			property Single Right { Single get() { return right; } void set( Single value ) { right = value; } }
			property Single Bottom { Single get() { return bottom; } void set( Single value ) { bottom = value; } }

			/// <summary>
			/// Width of the rectangle.
			/// </summary>
			property Single Width { Single get() { return ( right - left ); } }
			/// <summary>
			/// Height of the rectangle.
			/// </summary>
			property Single Height { Single get() { return ( bottom - top ); } }

			/// <summary>
			/// Location (upper-left corner) of the rectangle.
			/// </summary>
			property Point Location { Point get() { return Point( left, top ); } }

			/// <summary>
			/// Dimensions (size and width) of the rectangle.
			/// </summary>
			property Size Dimensions { Size get() { return Size( Width, Height ); } }

			/// <summary>
			/// Create a new <see cref="Kodo::Graphics::Rectangle"/> located at (0,0) with the specified dimensions.
			/// </summary>
			/// <param name="dim"><see cref="Size"/> that describes the dimensions.</param>
			Rectangle( Size dim ) : Rectangle( 0, 0, dim.Width, dim.Height ) { }
			/// <summary>
			/// Create a new <see cref="Kodo::Graphics::Rectangle"/> with the specified location and dimensions.
			/// </summary>
			/// <param name="loc"><see cref="Point"/> that describes the location.</param>
			/// <param name="dim"><see cref="Size"/> that describes the dimensions.</param>
			Rectangle( Point loc, Size dim ) : Rectangle( loc.X, loc.Y, loc.X + dim.Width, loc.Y + dim.Height ) { }
			/// <summary>
			/// Create a new <see cref="Kodo::Graphics::Rectangle"/> with the specified boundary coordinates.
			/// </summary>
			/// <param name="l">The left side.</param>
			/// <param name="t">The top side.</param>
			/// <param name="r">The right side.</param>
			/// <param name="b">The bottom side.</param>
			Rectangle( Double l, Double t, Double r, Double b ) : Rectangle( (Single)l, (Single)t, (Single)r, (Single)b ) { }
			/// <summary>
			/// Create a new <see cref="Kodo::Graphics::Rectangle"/> with the specified boundary coordinates.
			/// </summary>
			/// <param name="l">The left side.</param>
			/// <param name="t">The top side.</param>
			/// <param name="r">The right side.</param>
			/// <param name="b">The bottom side.</param>
			Rectangle( Single l, Single t, Single r, Single b )
			{
				left = l;
				top = t;
				right = r;
				bottom = b;
			}

			/// <summary>
			/// Indicates whether or not the rectangle contains the specified point.
			/// </summary>
			/// <param name="point">The <see cref="Point"/>.</param>
			Boolean Contains( Point point )
			{
				return ( ( Left <= point.X ) && ( Top <= point.Y ) && ( Right >= point.X ) && ( Bottom >= point.Y ) );
			}

			Rectangle Adjust( Single xAdjust, Single yAdjust )
			{
				return Rectangle( left, top, right + xAdjust, bottom + yAdjust );
			}

			Rectangle Scale( Single xScale, Single yScale )
			{
				return Rectangle( left, top, ( left + ( Width * xScale ) ), ( top + ( Height * yScale ) ) );
			}

			Rectangle Move( Single xTranslation, Single yTranslation )
			{
				return Rectangle( left + xTranslation, top + yTranslation, right + xTranslation, bottom + yTranslation );
			}

			Rectangle CenterTo( Rectangle area )
			{
				return Move( ( area.Left - left ) + ( ( area.Width - Width ) / 2 ), ( area.Top - top ) + ( ( area.Height - Height ) / 2 ) );
			}

			static Rectangle FromXYWH( Single x, Single y, Single w, Single h )
			{
				return Rectangle( x, y, x + w, y + h );
			}

			static Rectangle FromLTRB( Single l, Single t, Single r, Single b )
			{
				return Rectangle( l, t, r, b );
			}

			virtual Boolean Equals( Rectangle other )
			{
				return ( *this == other );
			}

			Boolean Equals( Object^ obj ) override
			{
				if ( obj != nullptr )
					return ( (Rectangle)obj ) == *this;

				return false;
			}

			Int32 GetHashCode() override
			{
				return (Int32)( left + top + right + bottom );
			}

			String^ ToString() override
			{
				return String::Format( System::Globalization::CultureInfo::InvariantCulture, "{0},{1},{2},{3}", left, top, right, bottom );
			}

			static Boolean operator !=( Rectangle left, Rectangle right ) { return !( left == right ); }
			static Boolean operator ==( Rectangle left, Rectangle right )
			{
				return ( left.left == right.left && left.top == right.top && left.right == right.right && left.bottom == right.bottom );
			}
		};
	}
}