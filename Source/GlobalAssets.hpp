#pragma once

namespace Kodo
{
	namespace Graphics
	{
		ref class FactoryD2D;
		ref class FactoryDW;

		namespace Imaging
		{
			ref class WicFactory;
		}

		public ref class GlobalAssets abstract sealed
		{
		public:

			static GlobalAssets();

			static FactoryD2D^ Factory2D;
			static FactoryDW^ FactoryDw;
			static Imaging::WicFactory^ FactoryWic;

			static void Create();
		};
	}
}