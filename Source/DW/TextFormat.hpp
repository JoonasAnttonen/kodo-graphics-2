#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		using namespace System;

		ref class InlineObject;

		/// <summary> Specifies the trimming option for text overflowing the layout box. </summary>
		public value class Trimming
		{
		public:

			/// <summary> Text granularity of which trimming applies. </summary>
			property TrimmingGranularity Granularity;

			/// <summary> Character code used as the delimiter signaling the beginning of the portion of text to be preserved, most useful for path ellipsis, where the delimiter would be a slash. </summary>
			property Int32 Delimiter;

			/// <summary> How many occurrences of the delimiter to step back. </summary>
			property Int32 DelimiterCount;

			Trimming( TrimmingGranularity granularity, Int32 delimiter, Int32 delimiterCount )
			{
				Granularity = granularity;
				Delimiter = delimiter;
				DelimiterCount = delimiterCount;
			}
		};

		public ref class TextFormat : public GraphicsObject
		{
			GRAPHICS_OBJECT( TextFormat, IDWriteTextFormat );

		private protected:

			TextFormat() { }

		public:

			InlineObject^ EllipsisTrimSign();

			TextFormat( String^ familyName, FontWeight weight, FontStyle style, FontStretch stretch, Single fontSize, String^ localeName );

			property FlowDirection FlowDirection
			{
				Kodo::Graphics::FlowDirection get();
				void set( Kodo::Graphics::FlowDirection value );
			}

			property String ^FontFamilyName
			{
				String ^get();
			}

			property Single FontSize
			{
				Single get();
			}

			property Kodo::Graphics::FontStretch FontStretch
			{
				Kodo::Graphics::FontStretch get();
			}

			property Kodo::Graphics::FontStyle FontStyle
			{
				Kodo::Graphics::FontStyle get();
			}

			property Kodo::Graphics::FontWeight FontWeight
			{
				Kodo::Graphics::FontWeight get();
			}

			property Single IncrementalTabStop
			{
				Single get();
				void set( Single value );
			}

			void GetLineSpacing( [ Out ] LineSpacingMethod %method, [ Out ] Single %lineSpacing, [ Out ] Single %baseline );
			void SetLineSpacing( LineSpacingMethod method, Single lineSpacing, Single baseline );

			property String ^LocaleName
			{
				String ^get();
			}

			property ParagraphAlignment ParagraphAlignment
			{
				Kodo::Graphics::ParagraphAlignment get();
				void set( Kodo::Graphics::ParagraphAlignment value );
			}

			property Kodo::Graphics::ReadingDirection ReadingDirection
			{
				Kodo::Graphics::ReadingDirection get();
				void set( Kodo::Graphics::ReadingDirection value );
			}

			property TextAlignment TextAlignment
			{
				Kodo::Graphics::TextAlignment get();
				void set( Kodo::Graphics::TextAlignment value );
			}

			Trimming GetTrimming();
			Trimming GetTrimming( [ Out ] InlineObject ^%trimmingSign );
			void SetTrimming( Trimming trimming, InlineObject^ trimmingSign );

			property Kodo::Graphics::WordWrapping WordWrapping
			{
				Kodo::Graphics::WordWrapping get();
				void set( Kodo::Graphics::WordWrapping value );
			}
		};
	}
}