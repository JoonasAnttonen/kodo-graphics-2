#pragma once

#include "..\Graphics.hpp"
#include "..\Common.hpp"
#include "Common.hpp"

#include "TextFormat.hpp"

namespace Kodo
{
	namespace Graphics
	{
		using namespace System::Runtime::InteropServices;

		/// <summary> 
		/// Represents a range of text. 
		/// </summary>
		[ StructLayout( LayoutKind::Sequential ) ]
		public value struct TextRange
		{
		private:
			UINT32 _startPosition;
			UINT32 _length;

		public:

			/// <summary>
			/// The starting index of the range.
			/// </summary>
			property Int32 Start
			{
				Int32 get() { return _startPosition; }
				void set( Int32 value ) { _startPosition = value; }
			}

			/// <summary>
			/// THe length of the range.
			/// </summary>
			property Int32 Length
			{
				Int32 get() { return _length; }
				void set( Int32 value ) { _length = value; }
			}

			/// <summary>
			/// Create a new <see cref="Kodo::Graphics::TextRange"/>.
			/// </summary>
			/// <param name="start">The starting index of the range.</param>
			/// <param name="length">The length of the range.</param>
			TextRange( Int32 start, Int32 length )
			{
				_startPosition = start;
				_length = length;
			}
		};

		/// <summary> 
		/// Represents a block of text after it has been fully analyzed and formatted. 
		/// </summary>
		public ref class TextLayout : TextFormat
		{
			GRAPHICS_OBJECT( TextLayout, IDWriteTextLayout );

		public:

			/// <summary>
			/// Create a new <see cref="Kodo::Graphics::TextLayout"/>.
			/// </summary>
			/// <param name="text">The text of the layout.</param>
			/// <param name="textFormat">The <see cref="TextFormat"/> of the layout.</param>
			/// <param name="maxSize">The maximum size of the layout.</param>
			TextLayout( String^ text, TextFormat^ textFormat, Size maxSize ) : TextLayout( text, textFormat, maxSize.Width, maxSize.Height ) { }
			/// <summary>
			/// Create a new <see cref="Kodo::Graphics::TextLayout"/>.
			/// </summary>
			/// <param name="text">The text of the layout.</param>
			/// <param name="textFormat">The <see cref="TextFormat"/> of the layout.</param>
			/// <param name="maxWidth">The maximum width of the layout.</param>
			/// <param name="maxHeight">The maximum height of the layout.</param>
			TextLayout( String^ text, TextFormat^ textFormat, Single maxWidth, Single maxHeight );

			property array<Kodo::Graphics::ClusterMetrics>^ ClusterMetrics
			{
				array<Kodo::Graphics::ClusterMetrics>^ get();
			}
			property Single MinWidth
			{
				Single get()
				{
					FLOAT minWidth;
					ThrowIfFailed<GraphicsException>( GetNative()->DetermineMinWidth( &minWidth ) );
					return minWidth;
				}
			}

			/// <summary>
			/// Get the maximum width and height of this layout.
			/// </summary>
			Kodo::Graphics::Size GetMaximumSize()
			{
				return Kodo::Graphics::Size( GetNative()->GetMaxWidth(), GetNative()->GetMaxHeight() );
			}

			/// <summary>
			/// Set the maximum width and height of this layout.
			/// </summary>
			/// <param name="value">The <see cref="Kodo::Graphics::Size"/>.</param>
			void SetMaximumSize( Kodo::Graphics::Size value )
			{
				ThrowIfFailed<GraphicsException>( GetNative()->SetMaxWidth( value.Width ) );
				ThrowIfFailed<GraphicsException>( GetNative()->SetMaxHeight( value.Height ) );
			}

			//ClientDrawingEffect^ GetDrawingEffect( Int32 position );
			//ClientDrawingEffect^ GetDrawingEffect( Int32 position, [ Out ]TextRange% textRange );

			String^ GetFontFamilyName( Int32 position );
			String^ GetFontFamilyName( Int32 position, [ Out ]TextRange% textRange );

			Single GetFontSize( Int32 position );
			Single GetFontSize( Int32 position, [ Out ]TextRange% textRange );

			Kodo::Graphics::FontStretch GetFontStretch( Int32 position );
			Kodo::Graphics::FontStretch GetFontStretch( Int32 position, [ Out ]TextRange% textRange );

			Kodo::Graphics::FontStyle GetFontStyle( Int32 position );
			Kodo::Graphics::FontStyle GetFontStyle( Int32 position, [ Out ]TextRange% textRange );

			Kodo::Graphics::FontWeight GetFontWeight( Int32 position );
			Kodo::Graphics::FontWeight GetFontWeight( Int32 position, [ Out ]TextRange% textRange );

			//InlineObject^ GetInlineObject( Int32 position );
			//InlineObject^ GetInlineObject( Int32 position, [ Out ]TextRange% textRange );

			property array<Kodo::Graphics::LineMetrics>^ LineMetrics
			{
				array<Kodo::Graphics::LineMetrics>^ get();
			}

			System::Globalization::CultureInfo^ TextLayout::GetCulture( Int32 position );
			System::Globalization::CultureInfo^ TextLayout::GetCulture( Int32 position, [ Out ]TextRange% textRange );

			property TextMetrics Metrics
			{
				TextMetrics get()
				{
					TextMetrics value;
					GetNative()->GetMetrics( (DWRITE_TEXT_METRICS*)&value );
					return value;
				}
			}
			property Kodo::Graphics::OverhangMetrics OverhangMetrics
			{
				Kodo::Graphics::OverhangMetrics get()
				{
					Kodo::Graphics::OverhangMetrics value;
					ThrowIfFailed<GraphicsException>( GetNative()->GetOverhangMetrics( (DWRITE_OVERHANG_METRICS *)&value ) );
					return value;
				}
			}

			Boolean GetStrikethrough( Int32 position );
			Boolean GetStrikethrough( Int32 position, [ Out ]TextRange% textRange );

			Boolean GetUnderline( Int32 position );
			Boolean GetUnderline( Int32 position, [ Out ]TextRange% textRange );

			HitTestMetrics HitTestPoint( Single x, Single y, [ Out ]Boolean% isTrailingHit, [ Out ]Boolean% isInside );
			HitTestMetrics HitTestTextPosition( Int32 textPosition, Boolean isTrailingHit, [ Out ]Single% pointX, [ Out ]Single% pointY );
			array<HitTestMetrics>^ HitTestTextRange( Int32 textPosition, Int32 textLength, Single originX, Single originY );

			void SetDrawingEffect( GraphicsObject^ effect, TextRange range );

			void SetFontFamilyName( String^ fontFamilyName, TextRange textRange );
			void SetFontSize( Single fontSize, TextRange textRange );
			void SetFontStretch( Kodo::Graphics::FontStretch fontStretch, TextRange textRange );
			void SetFontStyle( Kodo::Graphics::FontStyle fontStyle, TextRange textRange );
			void SetFontWeight( Kodo::Graphics::FontWeight fontWeight, TextRange textRange );
			void SetCulture( System::Globalization::CultureInfo^ cultureInfo, TextRange textRange );
			void SetStrikethrough( Boolean hasStrikethrough, TextRange textRange );
			void SetUnderline( Boolean hasUnderline, TextRange textRange );
		internal:

			IDWriteTextLayout* GetNative()
			{
				return (IDWriteTextLayout*)TextFormat::Native;
			}
		};
	}
}