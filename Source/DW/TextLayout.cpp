#include "TextLayout.hpp"

#include "FactoryDW.hpp"
#include "..\GlobalAssets.hpp"

namespace Kodo
{
	namespace Graphics
	{
		TextLayout::TextLayout( String^ text, TextFormat^ textFormat, Single maxWidth, Single maxHeight )
		{
			pin_ptr<const wchar_t> pinnedText = PtrToStringChars( text );

			IDWriteTextLayout *layout = nullptr;

			ThrowIfFailed<GraphicsException>(
				GlobalAssets::FactoryDw->Native->CreateTextLayout( pinnedText, text->Length,
																   textFormat->Native, maxWidth, maxHeight, &layout ) );

			Assing( layout );
		}

		array<Kodo::Graphics::ClusterMetrics>^ TextLayout::ClusterMetrics::get()
		{
			UINT32 count;
			HRESULT hr = Native->GetClusterMetrics( nullptr, 0, &count );
			if ( FAILED( hr ) && hr != E_NOT_SUFFICIENT_BUFFER )
				Marshal::ThrowExceptionForHR( hr );
			array<Kodo::Graphics::ClusterMetrics>^ metrics = gcnew array<Kodo::Graphics::ClusterMetrics>( count );
			pin_ptr<Kodo::Graphics::ClusterMetrics> pMetrics = &metrics[ 0 ];
			ThrowIfFailed<GraphicsException>( Native->GetClusterMetrics( (DWRITE_CLUSTER_METRICS *)pMetrics, count, &count ) );
			return metrics;
		}

		String^ TextLayout::GetFontFamilyName( Int32 position )
		{
			IDWriteTextLayout* native = Native;
			UINT32 nameLength;
			ThrowIfFailed<GraphicsException>( native->GetFontFamilyNameLength( position, &nameLength, nullptr ) );
			wchar_t* name = new wchar_t[ nameLength + 1 ];
			try
			{
				ThrowIfFailed<GraphicsException>( native->GetFontFamilyName( position, name, nameLength, nullptr ) );
				return gcnew String( name );
			}
			finally
			{
				delete name;
			}
		}

		String^ TextLayout::GetFontFamilyName( Int32 position, [ Out ]TextRange% textRange )
		{
			IDWriteTextLayout* native = Native;
			UINT32 nameLength;
			ThrowIfFailed<GraphicsException>( native->GetFontFamilyNameLength( position, &nameLength, nullptr ) );
			wchar_t* name = new wchar_t[ nameLength + 1 ];
			try
			{
				pin_ptr<TextRange> pTextRange = &textRange;
				ThrowIfFailed<GraphicsException>( native->GetFontFamilyName( position, name, nameLength, (DWRITE_TEXT_RANGE *)pTextRange ) );
				return gcnew String( name );
			}
			finally
			{
				delete name;
			}
		}

		Single TextLayout::GetFontSize( Int32 position )
		{
			FLOAT fontSize;
			ThrowIfFailed<GraphicsException>( Native->GetFontSize( position, &fontSize, nullptr ) );
			return fontSize;
		}

		Single TextLayout::GetFontSize( Int32 position, [ Out ]TextRange% textRange )
		{
			FLOAT fontSize;
			pin_ptr<TextRange> pTextRange = &textRange;
			ThrowIfFailed<GraphicsException>( Native->GetFontSize( position, &fontSize, (DWRITE_TEXT_RANGE *)pTextRange ) );
			return fontSize;
		}

		FontStretch TextLayout::GetFontStretch( Int32 position )
		{
			DWRITE_FONT_STRETCH value;
			ThrowIfFailed<GraphicsException>( Native->GetFontStretch( position, &value, nullptr ) );
			return ( Kodo::Graphics::FontStretch )value;
		}

		FontStretch TextLayout::GetFontStretch( Int32 position, [ Out ]TextRange% textRange )
		{
			DWRITE_FONT_STRETCH value;
			pin_ptr<TextRange> pTextRange = &textRange;
			ThrowIfFailed<GraphicsException>( Native->GetFontStretch( position, &value, (DWRITE_TEXT_RANGE *)pTextRange ) );
			return ( Kodo::Graphics::FontStretch )value;
		}

		FontStyle TextLayout::GetFontStyle( Int32 position )
		{
			DWRITE_FONT_STYLE value;
			ThrowIfFailed<GraphicsException>( Native->GetFontStyle( position, &value, nullptr ) );
			return ( Kodo::Graphics::FontStyle )value;
		}

		FontStyle TextLayout::GetFontStyle( Int32 position, [ Out ]TextRange% textRange )
		{
			DWRITE_FONT_STYLE value;
			pin_ptr<TextRange> pTextRange = &textRange;
			ThrowIfFailed<GraphicsException>( Native->GetFontStyle( position, &value, (DWRITE_TEXT_RANGE *)pTextRange ) );
			return ( Kodo::Graphics::FontStyle )value;
		}

		FontWeight TextLayout::GetFontWeight( Int32 position )
		{
			DWRITE_FONT_WEIGHT value;
			ThrowIfFailed<GraphicsException>( Native->GetFontWeight( position, &value, nullptr ) );
			return ( Kodo::Graphics::FontWeight )value;
		}

		FontWeight TextLayout::GetFontWeight( Int32 position, [ Out ]TextRange% textRange )
		{
			DWRITE_FONT_WEIGHT value;
			pin_ptr<TextRange> pTextRange = &textRange;
			ThrowIfFailed<GraphicsException>( Native->GetFontWeight( position, &value, (DWRITE_TEXT_RANGE *)pTextRange ) );
			return ( Kodo::Graphics::FontWeight )value;
		}

		array<Kodo::Graphics::LineMetrics>^ TextLayout::LineMetrics::get()
		{
			UINT32 count;
			HRESULT hr = Native->GetLineMetrics( nullptr, 0, &count );
			if ( FAILED( hr ) && hr != E_NOT_SUFFICIENT_BUFFER )
				Marshal::ThrowExceptionForHR( hr );
			array<Kodo::Graphics::LineMetrics>^ metrics = gcnew array<Kodo::Graphics::LineMetrics>( count );
			pin_ptr<Kodo::Graphics::LineMetrics> pMetrics = &metrics[ 0 ];
			ThrowIfFailed<GraphicsException>( Native->GetLineMetrics( (DWRITE_LINE_METRICS *)pMetrics, count, &count ) );
			return metrics;
		}

		HitTestMetrics TextLayout::HitTestPoint( Single x, Single y, [ Out ]Boolean% isTrailingHit, [ Out ]Boolean% isInside )
		{
			HitTestMetrics metrics;
			BOOL b1, b2;
			ThrowIfFailed<GraphicsException>( Native->HitTestPoint( x, y, &b1, &b2, (DWRITE_HIT_TEST_METRICS*)&metrics ) );
			isTrailingHit = b1 != 0;
			isInside = b2 != 0;
			return metrics;
		}

		HitTestMetrics TextLayout::HitTestTextPosition( Int32 textPosition, Boolean isTrailingHit, [ Out ]Single% pointX, [ Out ]Single% pointY )
		{
			HitTestMetrics metrics;
			FLOAT x, y;
			ThrowIfFailed<GraphicsException>(
				Native->HitTestTextPosition(
					textPosition,
					isTrailingHit,
					&x,
					&y,
					(DWRITE_HIT_TEST_METRICS*)&metrics ) );
			pointX = x;
			pointY = y;
			return metrics;
		}

		array<HitTestMetrics>^ TextLayout::HitTestTextRange( Int32 textPosition, Int32 textLength, Single originX, Single originY )
		{
			UINT32 count;
			HRESULT hr = Native->HitTestTextRange( textPosition, textLength, originX, originY, nullptr, 0, &count );
			if ( hr != E_NOT_SUFFICIENT_BUFFER )
				ThrowIfFailed<GraphicsException>( hr );

			array<HitTestMetrics>^ metrics = gcnew array<HitTestMetrics>( count );
			pin_ptr<HitTestMetrics> pMetrics = &metrics[ 0 ];

			ThrowIfFailed<GraphicsException>(
				Native->HitTestTextRange(
					textPosition,
					textLength,
					originX,
					originY,
					(DWRITE_HIT_TEST_METRICS *)pMetrics,
					count,
					&count ) );

			return metrics;
		}

		System::Globalization::CultureInfo^ TextLayout::GetCulture( Int32 position )
		{
			UINT32 len;
			ThrowIfFailed<GraphicsException>( Native->GetLocaleNameLength( position, &len ) );
			wchar_t* name = new wchar_t[ len + 1 ];
			try
			{
				ThrowIfFailed<GraphicsException>( Native->GetLocaleName( position, name, len, nullptr ) );
				return gcnew System::Globalization::CultureInfo( gcnew String( name ) );
			}
			finally
			{
				delete[] name;
			}
		}

		System::Globalization::CultureInfo^ TextLayout::GetCulture( Int32 position, [ Out ]TextRange% textRange )
		{
			UINT32 len;
			ThrowIfFailed<GraphicsException>( Native->GetLocaleNameLength( position, &len ) );
			wchar_t* name = new wchar_t[ len + 1 ];
			try
			{
				pin_ptr<TextRange> pTextRange = &textRange;
				ThrowIfFailed<GraphicsException>( Native->GetLocaleName( position, name, len, (DWRITE_TEXT_RANGE *)pTextRange ) );
				return gcnew System::Globalization::CultureInfo( gcnew String( name ) );
			}
			finally
			{
				delete[] name;
			}
		}

		Boolean TextLayout::GetStrikethrough( Int32 position )
		{
			BOOL value;
			ThrowIfFailed<GraphicsException>( Native->GetStrikethrough( position, &value, nullptr ) );
			return value != 0;
		}

		Boolean TextLayout::GetStrikethrough( Int32 position, [ Out ]TextRange% textRange )
		{
			BOOL value;
			pin_ptr<TextRange> pTextRange = &textRange;
			ThrowIfFailed<GraphicsException>( Native->GetStrikethrough( position, &value, (DWRITE_TEXT_RANGE *)pTextRange ) );
			return value != 0;
		}

		Boolean TextLayout::GetUnderline( Int32 position )
		{
			BOOL value;
			ThrowIfFailed<GraphicsException>( Native->GetUnderline( position, &value, nullptr ) );
			return value != 0;
		}

		Boolean TextLayout::GetUnderline( Int32 position, [ Out ]TextRange% textRange )
		{
			BOOL value;
			pin_ptr<TextRange> pTextRange = &textRange;
			ThrowIfFailed<GraphicsException>( Native->GetUnderline( position, &value, (DWRITE_TEXT_RANGE *)pTextRange ) );
			return value != 0;
		}

		void TextLayout::SetDrawingEffect( GraphicsObject^ effect, TextRange range )
		{
			ThrowIfFailed<GraphicsException>( Native->SetDrawingEffect( effect->_native, *(DWRITE_TEXT_RANGE *)&range ) );
		}

		void TextLayout::SetFontFamilyName( String^ fontFamilyName, TextRange textRange )
		{
			pin_ptr<const wchar_t> name = PtrToStringChars( fontFamilyName );
			ThrowIfFailed<GraphicsException>( Native->SetFontFamilyName( name, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}

		void TextLayout::SetFontSize( Single fontSize, TextRange textRange )
		{
			ThrowIfFailed<GraphicsException>( Native->SetFontSize( fontSize, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}

		void TextLayout::SetFontStretch( Kodo::Graphics::FontStretch fontStretch, TextRange textRange )
		{
			ThrowIfFailed<GraphicsException>( Native->SetFontStretch( (DWRITE_FONT_STRETCH)fontStretch, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}

		void TextLayout::SetFontStyle( Kodo::Graphics::FontStyle fontStyle, TextRange textRange )
		{
			ThrowIfFailed<GraphicsException>( Native->SetFontStyle( (DWRITE_FONT_STYLE)fontStyle, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}

		void TextLayout::SetFontWeight( Kodo::Graphics::FontWeight fontWeight, TextRange textRange )
		{
			ThrowIfFailed<GraphicsException>( Native->SetFontWeight( (DWRITE_FONT_WEIGHT)fontWeight, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}

		void TextLayout::SetCulture( System::Globalization::CultureInfo^ cultureInfo, TextRange textRange )
		{
			pin_ptr<const wchar_t> name = PtrToStringChars( cultureInfo->Name );
			ThrowIfFailed<GraphicsException>( Native->SetLocaleName( name, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}

		void TextLayout::SetStrikethrough( Boolean hasStrikethrough, TextRange textRange )
		{
			ThrowIfFailed<GraphicsException>( Native->SetStrikethrough( hasStrikethrough, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}

		void TextLayout::SetUnderline( Boolean hasUnderline, TextRange textRange )
		{
			ThrowIfFailed<GraphicsException>( Native->SetUnderline( hasUnderline, *(DWRITE_TEXT_RANGE *)&textRange ) );
		}
	}
}