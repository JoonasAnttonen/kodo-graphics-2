#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		using namespace System;
		using namespace System::Runtime::InteropServices;

		//ref class InlineObject;
		//ref class TextFormat;
		//ref class TextLayout;

		public ref class FactoryDW : public GraphicsObject
		{
			GRAPHICS_OBJECT( FactoryDW, IDWriteFactory2 );

		public:

			FactoryDW()
			{
				IDWriteFactory2 *factory = nullptr;

				ThrowIfFailed<GraphicsException>( DWriteCreateFactory( DWRITE_FACTORY_TYPE_SHARED, __uuidof( IDWriteFactory2 ), reinterpret_cast<IUnknown**>( &factory ) ) );

				Assing( factory );
			}

			//InlineObject^ CreateEllipsisTrimmingSign( TextFormat^ textFormat );
		};
	}
}
