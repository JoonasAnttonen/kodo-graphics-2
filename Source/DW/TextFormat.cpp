#include "TextFormat.hpp"

#include "FactoryDW.hpp"
#include "..\GlobalAssets.hpp"
#include "InlineObject.hpp"

namespace Kodo
{
	namespace Graphics
	{
		using namespace System;

		InlineObject^ TextFormat::EllipsisTrimSign()
		{
			IDWriteInlineObject* obj = nullptr;

			ThrowIfFailed<GraphicsException>( GlobalAssets::FactoryDw->Native->CreateEllipsisTrimmingSign( Native, &obj ) );

			return gcnew InlineObject( obj );
		}

		TextFormat::TextFormat( String^ familyName, Kodo::Graphics::FontWeight weight, Kodo::Graphics::FontStyle style, Kodo::Graphics::FontStretch stretch, Single fontSize, String^ localeName )
		{
			pin_ptr<const wchar_t> pinnedText = PtrToStringChars( localeName );
			pin_ptr<const wchar_t> pinnedName = PtrToStringChars( familyName );

			IDWriteTextFormat *format = nullptr;

			ThrowIfFailed<GraphicsException>( GlobalAssets::FactoryDw->Native->CreateTextFormat(
				pinnedName,
				nullptr,
				static_cast<DWRITE_FONT_WEIGHT>( weight ),
				static_cast<DWRITE_FONT_STYLE>( style ),
				static_cast<DWRITE_FONT_STRETCH>( stretch ),
				fontSize,
				pinnedText,
				&format )
				);

			Assing( format );
		}

		Kodo::Graphics::FlowDirection TextFormat::FlowDirection::get()
		{
			return static_cast<Kodo::Graphics::FlowDirection>( Native->GetFlowDirection() );
		}

		void TextFormat::FlowDirection::set( Kodo::Graphics::FlowDirection value )
		{
			Native->SetFlowDirection( static_cast<DWRITE_FLOW_DIRECTION>( value ) );
		}

		String ^TextFormat::FontFamilyName::get()
		{
			UINT32 count = Native->GetFontFamilyNameLength();
			std::vector<WCHAR> name;
			name.resize( count + 1 );

			if ( FAILED( Native->GetFontFamilyName( &name[ 0 ], static_cast<UINT32>( name.size() ) ) ) )
				return String::Empty;

			return gcnew String( &name[ 0 ] );
		}

		Single TextFormat::FontSize::get()
		{
			return Native->GetFontSize();
		}

		Kodo::Graphics::FontStretch TextFormat::FontStretch::get()
		{
			return static_cast<Kodo::Graphics::FontStretch>( Native->GetFontStretch() );
		}

		Kodo::Graphics::FontStyle TextFormat::FontStyle::get()
		{
			return static_cast<Kodo::Graphics::FontStyle>( Native->GetFontStyle() );
		}

		Kodo::Graphics::FontWeight TextFormat::FontWeight::get()
		{
			return static_cast<Kodo::Graphics::FontWeight>( Native->GetFontWeight() );
		}

		Single TextFormat::IncrementalTabStop::get()
		{
			return Native->GetIncrementalTabStop();
		}

		void TextFormat::IncrementalTabStop::set( Single value )
		{
			Native->SetIncrementalTabStop( value );
		}

		void TextFormat::GetLineSpacing( [ Out ] LineSpacingMethod %method, [ Out ] Single %lineSpacing, [ Out ] Single %baseline )
		{
			DWRITE_LINE_SPACING_METHOD nativeMethod;
			Single nativeLineSpacing, nativeBaseline;

			ThrowIfFailed<GraphicsException>( Native->GetLineSpacing( &nativeMethod, &nativeLineSpacing, &nativeBaseline ) );

			method = static_cast<LineSpacingMethod>( nativeMethod );
			lineSpacing = nativeLineSpacing;
			baseline = nativeBaseline;
		}

		void TextFormat::SetLineSpacing( LineSpacingMethod method, Single lineSpacing, Single baseline )
		{
			ThrowIfFailed<GraphicsException>( Native->SetLineSpacing( static_cast<DWRITE_LINE_SPACING_METHOD>( method ), lineSpacing, baseline ) );
		}

		String ^TextFormat::LocaleName::get()
		{
			UINT32 count = Native->GetLocaleNameLength();
			std::vector<WCHAR> name;
			name.resize( count + 1 );

			if ( FAILED( Native->GetLocaleName( &name[ 0 ], static_cast<UINT32>( name.size() ) ) ) )
				return String::Empty;

			return gcnew String( &name[ 0 ] );
		}

		Kodo::Graphics::ParagraphAlignment TextFormat::ParagraphAlignment::get()
		{
			return static_cast<Kodo::Graphics::ParagraphAlignment>( Native->GetParagraphAlignment() );
		}

		void TextFormat::ParagraphAlignment::set( Kodo::Graphics::ParagraphAlignment value )
		{
			Native->SetParagraphAlignment( static_cast<DWRITE_PARAGRAPH_ALIGNMENT>( value ) );
		}

		Kodo::Graphics::ReadingDirection TextFormat::ReadingDirection::get()
		{
			return static_cast<Kodo::Graphics::ReadingDirection>( Native->GetReadingDirection() );
		}

		void TextFormat::ReadingDirection::set( Kodo::Graphics::ReadingDirection value )
		{
			ThrowIfFailed<GraphicsException>( Native->SetReadingDirection( static_cast<DWRITE_READING_DIRECTION>( value ) ) );
		}

		Kodo::Graphics::TextAlignment TextFormat::TextAlignment::get()
		{
			return static_cast<Kodo::Graphics::TextAlignment>( Native->GetTextAlignment() );
		}

		void TextFormat::TextAlignment::set( Kodo::Graphics::TextAlignment value )
		{
			Native->SetTextAlignment( static_cast<DWRITE_TEXT_ALIGNMENT>( value ) );
		}

		static Trimming GetTrimmingInternal( IDWriteTextFormat *format, IDWriteInlineObject **obj )
		{
			DWRITE_TRIMMING trimming;

			if ( FAILED( format->GetTrimming( &trimming, obj ) ) )
			{
				DWRITE_TRIMMING zero = { DWRITE_TRIMMING_GRANULARITY( 0 ) };
				trimming = zero;
			}

			return Trimming( static_cast<TrimmingGranularity>( trimming.granularity ), trimming.delimiter, trimming.delimiterCount );
		}

		Trimming TextFormat::GetTrimming()
		{
			return GetTrimmingInternal( Native, 0 );
		}

		Trimming TextFormat::GetTrimming( [ Out ] InlineObject ^%trimmingSign )
		{
			IDWriteInlineObject *obj = 0;
			Trimming result = GetTrimmingInternal( Native, &obj );

			if ( obj )
			{
				trimmingSign = gcnew InlineObject( obj );
			}
			return result;
		}

		void TextFormat::SetTrimming( Trimming trimming, InlineObject^ trimmingSign )
		{
			DWRITE_TRIMMING nativeTrimming;
			nativeTrimming.delimiter = trimming.Delimiter;
			nativeTrimming.delimiterCount = trimming.DelimiterCount;
			nativeTrimming.granularity = (DWRITE_TRIMMING_GRANULARITY)trimming.Granularity;

			Native->SetTrimming( &nativeTrimming, trimmingSign->Native );
		}

		Kodo::Graphics::WordWrapping TextFormat::WordWrapping::get()
		{
			return static_cast<Kodo::Graphics::WordWrapping>( Native->GetWordWrapping() );
		}

		void TextFormat::WordWrapping::set( Kodo::Graphics::WordWrapping value )
		{
			ThrowIfFailed<GraphicsException>( Native->SetWordWrapping( static_cast<DWRITE_WORD_WRAPPING>( value ) ) );
		}
	}
}
