#pragma once

#include "..\Graphics.hpp"
#include "Common.hpp"

namespace Kodo
{
	namespace Graphics
	{
		public ref class InlineObject : public GraphicsObject
		{
			GRAPHICS_OBJECT( InlineObject, IDWriteInlineObject );
		};
	}
}
